SET FOREIGN_KEY_CHECKS=0;

SET AUTOCOMMIT = 0;
START TRANSACTION;

--
-- Estructura de tabla para la tabla `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `names` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `readed` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_index` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `country`
--

INSERT INTO `country` (`id`, `country`, `country_index`, `code`) VALUES
(1, 'Afganistán', 'AF', '93'),
(2, 'Islas Gland', 'AX', NULL),
(3, 'Albania', 'AL', '355'),
(4, 'Alemania', 'DE', '49'),
(5, 'Andorra', 'AD', '376'),
(6, 'Angola', 'AO', '244'),
(7, 'Anguilla', 'AI', NULL),
(8, 'Antártida', 'AQ', NULL),
(9, 'Antigua y Barbuda', 'AG', '1-268'),
(10, 'Antillas Holandesas', 'AN', NULL),
(11, 'Arabia Saudí', 'SA', '966'),
(12, 'Argelia', 'DZ', '213'),
(13, 'Argentina', 'AR', '54'),
(14, 'Armenia', 'AM', '374'),
(15, 'Aruba', 'AW', '297'),
(16, 'Australia', 'AU', '61'),
(17, 'Austria', 'AT', '43'),
(18, 'Azerbaiyán', 'AZ', NULL),
(19, 'Bahamas', 'BS', '1-242'),
(20, 'Bahréin', 'BH', '973'),
(21, 'Bangladesh', 'BD', '880'),
(22, 'Barbados', 'BB', '1-246'),
(23, 'Bielorrusia', 'BY', '375'),
(24, 'Bélgica', 'BE', '32'),
(25, 'Belice', 'BZ', '501'),
(26, 'Benin', 'BJ', '229'),
(27, 'Bermudas', 'BM', '1-441'),
(28, 'Bhután', 'BT', NULL),
(29, 'Bolivia', 'BO', NULL),
(30, 'Bosnia y Herzegovina', 'BA', NULL),
(31, 'Botsuana', 'BW', NULL),
(32, 'Isla Bouvet', 'BV', NULL),
(33, 'Brasil', 'BR', NULL),
(34, 'Brunéi', 'BN', NULL),
(35, 'Bulgaria', 'BG', NULL),
(36, 'Burkina Faso', 'BF', NULL),
(37, 'Burundi', 'BI', NULL),
(38, 'Cabo Verde', 'CV', NULL),
(39, 'Islas Caimán', 'KY', NULL),
(40, 'Camboya', 'KH', NULL),
(41, 'Camerún', 'CM', NULL),
(42, 'Canadá', 'CA', NULL),
(43, 'República Centroafricana', 'CF', NULL),
(44, 'Chad', 'TD', NULL),
(45, 'República Checa', 'CZ', NULL),
(46, 'Chile', 'CL', NULL),
(47, 'China', 'CN', NULL),
(48, 'Chipre', 'CY', NULL),
(49, 'Isla de Navidad', 'CX', NULL),
(50, 'Ciudad del Vaticano', 'VA', NULL),
(51, 'Islas Cocos', 'CC', NULL),
(52, 'Colombia', 'CO', NULL),
(53, 'Comoras', 'KM', NULL),
(54, 'República Democrática del Congo', 'CD', NULL),
(55, 'Congo', 'CG', NULL),
(56, 'Islas Cook', 'CK', NULL),
(57, 'Corea del Norte', 'KP', NULL),
(58, 'Corea del Sur', 'KR', NULL),
(59, 'Costa de Marfil', 'CI', NULL),
(60, 'Costa Rica', 'CR', NULL),
(61, 'Croacia', 'HR', NULL),
(62, 'Cuba', 'CU', NULL),
(63, 'Dinamarca', 'DK', NULL),
(64, 'Dominica', 'DM', NULL),
(65, 'República Dominicana', 'DO', NULL),
(66, 'Ecuador', 'EC', NULL),
(67, 'Egipto', 'EG', NULL),
(68, 'El Salvador', 'SV', NULL),
(69, 'Emiratos Árabes Unidos', 'AE', NULL),
(70, 'Eritrea', 'ER', NULL),
(71, 'Eslovaquia', 'SK', NULL),
(72, 'Eslovenia', 'SI', NULL),
(73, 'España', 'ES', NULL),
(74, 'Islas ultramarinas de Estados Unidos', 'UM', NULL),
(75, 'Estados Unidos', 'US', NULL),
(76, 'Estonia', 'EE', NULL),
(77, 'Etiopía', 'ET', NULL),
(78, 'Islas Feroe', 'FO', NULL),
(79, 'Filipinas', 'PH', NULL),
(80, 'Finlandia', 'FI', NULL),
(81, 'Fiyi', 'FJ', NULL),
(82, 'Francia', 'FR', NULL),
(83, 'Gabón', 'GA', NULL),
(84, 'Gambia', 'GM', NULL),
(85, 'Georgia', 'GE', NULL),
(86, 'Islas Georgias del Sur y Sandwich del Sur', 'GS', NULL),
(87, 'Ghana', 'GH', NULL),
(88, 'Gibraltar', 'GI', NULL),
(89, 'Granada', 'GD', NULL),
(90, 'Grecia', 'GR', NULL),
(91, 'Groenlandia', 'GL', NULL),
(92, 'Guadalupe', 'GP', NULL),
(93, 'Guam', 'GU', NULL),
(94, 'Guatemala', 'GT', NULL),
(95, 'Guayana Francesa', 'GF', NULL),
(96, 'Guinea', 'GN', NULL),
(97, 'Guinea Ecuatorial', 'GQ', NULL),
(98, 'Guinea-Bissau', 'GW', NULL),
(99, 'Guyana', 'GY', NULL),
(100, 'Haití', 'HT', NULL),
(101, 'Islas Heard y McDonald', 'HM', NULL),
(102, 'Honduras', 'HN', NULL),
(103, 'Hong Kong', 'HK', NULL),
(104, 'Hungría', 'HU', NULL),
(105, 'India', 'IN', NULL),
(106, 'Indonesia', 'ID', NULL),
(107, 'Irán', 'IR', NULL),
(108, 'Iraq', 'IQ', NULL),
(109, 'Irlanda', 'IE', NULL),
(110, 'Islandia', 'IS', NULL),
(111, 'Israel', 'IL', NULL),
(112, 'Italia', 'IT', NULL),
(113, 'Jamaica', 'JM', NULL),
(114, 'Japón', 'JP', NULL),
(115, 'Jordania', 'JO', NULL),
(116, 'Kazajstán', 'KZ', NULL),
(117, 'Kenia', 'KE', NULL),
(118, 'Kirguistán', 'KG', NULL),
(119, 'Kiribati', 'KI', NULL),
(120, 'Kuwait', 'KW', NULL),
(121, 'Laos', 'LA', NULL),
(122, 'Lesotho', 'LS', NULL),
(123, 'Letonia', 'LV', NULL),
(124, 'Líbano', 'LB', NULL),
(125, 'Liberia', 'LR', NULL),
(126, 'Libia', 'LY', NULL),
(127, 'Liechtenstein', 'LI', NULL),
(128, 'Lituania', 'LT', NULL),
(129, 'Luxemburgo', 'LU', NULL),
(130, 'Macao', 'MO', NULL),
(131, 'ARY Macedonia', 'MK', NULL),
(132, 'Madagascar', 'MG', NULL),
(133, 'Malasia', 'MY', NULL),
(134, 'Malawi', 'MW', NULL),
(135, 'Maldivas', 'MV', NULL),
(136, 'Malí', 'ML', NULL),
(137, 'Malta', 'MT', NULL),
(138, 'Islas Malvinas', 'FK', NULL),
(139, 'Islas Marianas del Norte', 'MP', NULL),
(140, 'Marruecos', 'MA', NULL),
(141, 'Islas Marshall', 'MH', NULL),
(142, 'Martinica', 'MQ', NULL),
(143, 'Mauricio', 'MU', NULL),
(144, 'Mauritania', 'MR', NULL),
(145, 'Mayotte', 'YT', NULL),
(146, 'México', 'MX', NULL),
(147, 'Micronesia', 'FM', NULL),
(148, 'Moldavia', 'MD', NULL),
(149, 'Mónaco', 'MC', NULL),
(150, 'Mongolia', 'MN', NULL),
(151, 'Montserrat', 'MS', NULL),
(152, 'Mozambique', 'MZ', NULL),
(153, 'Myanmar', 'MM', NULL),
(154, 'Namibia', 'NA', NULL),
(155, 'Nauru', 'NR', NULL),
(156, 'Nepal', 'NP', NULL),
(157, 'Nicaragua', 'NI', NULL),
(158, 'Níger', 'NE', NULL),
(159, 'Nigeria', 'NG', NULL),
(160, 'Niue', 'NU', NULL),
(161, 'Isla Norfolk', 'NF', NULL),
(162, 'Noruega', 'NO', NULL),
(163, 'Nueva Caledonia', 'NC', NULL),
(164, 'Nueva Zelanda', 'NZ', NULL),
(165, 'Omán', 'OM', NULL),
(166, 'Países Bajos', 'NL', NULL),
(167, 'Pakistán', 'PK', NULL),
(168, 'Palau', 'PW', NULL),
(169, 'Palestina', 'PS', NULL),
(170, 'Panamá', 'PA', NULL),
(171, 'Papúa Nueva Guinea', 'PG', NULL),
(172, 'Paraguay', 'PY', NULL),
(173, 'Perú', 'PE', NULL),
(174, 'Islas Pitcairn', 'PN', NULL),
(175, 'Polinesia Francesa', 'PF', NULL),
(176, 'Polonia', 'PL', NULL),
(177, 'Portugal', 'PT', NULL),
(178, 'Puerto Rico', 'PR', NULL),
(179, 'Qatar', 'QA', NULL),
(180, 'Reino Unido', 'GB', NULL),
(181, 'Reunión', 'RE', NULL),
(182, 'Ruanda', 'RW', NULL),
(183, 'Rumania', 'RO', NULL),
(184, 'Rusia', 'RU', NULL),
(185, 'Sahara Occidental', 'EH', NULL),
(186, 'Islas Salomón', 'SB', NULL),
(187, 'Samoa', 'WS', NULL),
(188, 'Samoa Americana', 'AS', NULL),
(189, 'San Cristóbal y Nevis', 'KN', NULL),
(190, 'San Marino', 'SM', NULL),
(191, 'San Pedro y Miquelón', 'PM', NULL),
(192, 'San Vicente y las Granadinas', 'VC', NULL),
(193, 'Santa Helena', 'SH', NULL),
(194, 'Santa Lucía', 'LC', NULL),
(195, 'Santo Tomé y Príncipe', 'ST', NULL),
(196, 'Senegal', 'SN', NULL),
(197, 'Serbia y Montenegro', 'CS', NULL),
(198, 'Seychelles', 'SC', NULL),
(199, 'Sierra Leona', 'SL', NULL),
(200, 'Singapur', 'SG', NULL),
(201, 'Siria', 'SY', NULL),
(202, 'Somalia', 'SO', NULL),
(203, 'Sri Lanka', 'LK', NULL),
(204, 'Suazilandia', 'SZ', NULL),
(205, 'Sudáfrica', 'ZA', NULL),
(206, 'Sudán', 'SD', NULL),
(207, 'Suecia', 'SE', NULL),
(208, 'Suiza', 'CH', NULL),
(209, 'Surinam', 'SR', NULL),
(210, 'Svalbard y Jan Mayen', 'SJ', NULL),
(211, 'Tailandia', 'TH', NULL),
(212, 'Taiwán', 'TW', NULL),
(213, 'Tanzania', 'TZ', NULL),
(214, 'Tayikistán', 'TJ', NULL),
(215, 'Territorio Británico del Océano Índico', 'IO', NULL),
(216, 'Territorios Australes Franceses', 'TF', NULL),
(217, 'Timor Oriental', 'TL', NULL),
(218, 'Togo', 'TG', NULL),
(219, 'Tokelau', 'TK', NULL),
(220, 'Tonga', 'TO', NULL),
(221, 'Trinidad y Tobago', 'TT', NULL),
(222, 'Túnez', 'TN', NULL),
(223, 'Islas Turcas y Caicos', 'TC', NULL),
(224, 'Turkmenistán', 'TM', NULL),
(225, 'Turquía', 'TR', NULL),
(226, 'Tuvalu', 'TV', NULL),
(227, 'Ucrania', 'UA', NULL),
(228, 'Uganda', 'UG', NULL),
(229, 'Uruguay', 'UY', NULL),
(230, 'Uzbekistán', 'UZ', NULL),
(231, 'Vanuatu', 'VU', NULL),
(232, 'Venezuela', 'VE', '58'),
(233, 'Vietnam', 'VN', NULL),
(234, 'Islas Vírgenes Británicas', 'VG', NULL),
(235, 'Islas Vírgenes de los Estados Unidos', 'VI', NULL),
(236, 'Wallis y Futuna', 'WF', NULL),
(237, 'Yemen', 'YE', NULL),
(238, 'Yibuti', 'DJ', NULL),
(239, 'Zambia', 'ZM', NULL),
(240, 'Zimbabue', 'ZW', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `information`
--

CREATE TABLE `information` (
  `id` int(11) NOT NULL,
  `history` longtext COLLATE utf8mb4_unicode_ci,
  `team` longtext COLLATE utf8mb4_unicode_ci,
  `services` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `information`
--

INSERT INTO `information` (`id`, `history`, `team`, `services`) VALUES
(1, '<p>DEVA Psicología y Sexología&nbsp;</p><p>En los primeros años (1982) recibía el nombre de “Gabinete de Crecimiento Personal”. Sus actividades iban en la línea de la Psicología Clínica y Educativa. A partir de 1984 se van introduciendo otras actividades, que priorizan un cambio de nombre. La Sexología empieza a tomar cuerpo aquí. Así nace INTESEX. (Investigación, Terapia Psicológica y Sexología). Durante todos esos años el Centro está enclavado en el barrio de Ayora.&nbsp;<br><br>En el 2006 se traslada a Benimaclet, donde nos encontramos desde entonces en la calle Francisco Martínez, 1-6º-18ª, pasando a ser <strong>Deva: Centro de Psicología y Sexología (Intesex)</strong>, y estamos pendientes de abrir otro local, en breve.</p><figure class=\"image\"><img src=\"http:\\/\\/devapsicolegs.com/public/images/information/CK/ck5ded7079e1b63351985ded7079e1e1d.jpeg\"></figure>', '<p>Apostamos por un servicio holístico, donde las personas sean atendidas según sus necesidades, por lo que somos un equipo multidisciplinar en continuo reciclaje formativo para poder ofrecer el mejor servicio.</p>', '<p>Disponemos de 2 áreas de intervención principales Psicología y Sexología que a su vez se dividen en otras áreas. Utilizamos diferentes métodos y técnicas según el caso de cada persona.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_image` tinytext COLLATE utf8mb4_unicode_ci,
  `banner_image` tinytext COLLATE utf8mb4_unicode_ci,
  `video` text COLLATE utf8mb4_unicode_ci,
  `code` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headline` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `draft` tinyint(1) NOT NULL,
  `rating` decimal(10,1) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `content`, `cover_image`, `banner_image`, `video`, `code`, `headline`, `active`, `draft`, `rating`, `create_at`, `update_at`) VALUES
(1, 1, 'Lorem ipsum', '<p>In ligula magna, commodo ac aliquet at, facilisis in lectus. Phasellus rutrum porta lectus, id consequat odio laoreet ut. Mauris vehicula magna sit amet enim cursus, eu fermentum magna faucibus. Etiam consequat in justo ac varius. Ut ultrices eros quis turpis imperdiet accumsan. Aenean bibendum massa et gravida tempus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent vitae bibendum orci, id convallis purus. In tincidunt dapibus quam, at ultrices urna. Nulla non facilisis velit. Proin id arcu luctus dolor posuere hendrerit vel eu nisi. Donec volutpat est egestas pulvinar pharetra. Morbi libero augue, viverra quis sagittis vitae, posuere vel purus. Integer ac nisl euismod, lacinia dolor in, eleifend dolor.</p><p>Phasellus viverra auctor est ut pulvinar. Suspendisse fermentum feugiat blandit. Duis fringilla, ex in accumsan aliquam, ligula sem egestas ligula, sed pretium ligula leo eget diam. Aenean sit amet maximus enim. Nullam rhoncus fringilla dui quis euismod. Sed non convallis enim. Phasellus vehicula nisi id risus vestibulum ullamcorper. Nullam consectetur nunc eu lacus gravida sagittis. Proin bibendum varius dui sed commodo. Mauris sed ipsum justo. In lacus quam, commodo id tincidunt sit amet, viverra nec sem.</p>', 'ADM001_cover_64245dfb6f8b04f01.jpeg', 'banner_848205dfb6fb8aae14.jpeg', '', '12_2019', 0, 1, 0, NULL, '2019-12-19 08:40:24', '2019-12-19 08:40:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_categories`
--

CREATE TABLE `posts_categories` (
  `post_id` int(11) NOT NULL,
  `postcategory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `posts_categories`
--

INSERT INTO `posts_categories` (`post_id`, `postcategory_id`) VALUES
(1, 2),
(1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_category`
--

CREATE TABLE `posts_category` (
  `id` int(11) NOT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `canonical_name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cover_image` tinytext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `posts_category`
--

INSERT INTO `posts_category` (`id`, `name`, `canonical_name`, `description`, `cover_image`) VALUES
(1, 'Desde la Psicología', 'desde-la-psicologia', '', NULL),
(2, 'Sexología y diversidad', 'sexologia-y-diversidad', '', NULL),
(3, 'Actualidad y sociedad', 'actualidad-y-sociedad', '', NULL),
(4, 'Actividades', 'actividades', '', NULL),
(5, 'Artículos', 'articulos', '', NULL),
(6, 'Galería', 'galeria', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_subcategories`
--

CREATE TABLE `posts_subcategories` (
  `post_id` int(11) NOT NULL,
  `postsubcategory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_subcategory`
--

CREATE TABLE `posts_subcategory` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `canonical_name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_tags`
--

CREATE TABLE `posts_tags` (
  `post_id` int(11) NOT NULL,
  `tags_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `posts_tags`
--

INSERT INTO `posts_tags` (`post_id`, `tags_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post_comments`
--

CREATE TABLE `post_comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `names` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `post_comments`
--

INSERT INTO `post_comments` (`id`, `post_id`, `names`, `email`, `comment`, `active`, `create_at`, `update_at`) VALUES
(1, 1, 'ALI', 'ali@ali.com', 'Comentario', 1, '2019-12-19 08:50:40', '2019-12-19 08:50:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post_gallery`
--

CREATE TABLE `post_gallery` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `image_name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `post_gallery`
--

INSERT INTO `post_gallery` (`id`, `post_id`, `image_name`) VALUES
(1, 1, 'g_135dfb6fad37d49.jpeg'),
(2, 1, 'g_644435dfb6fad540ac.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post_seo`
--

CREATE TABLE `post_seo` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `author` tinytext COLLATE utf8mb4_unicode_ci,
  `url` tinytext COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `post_seo`
--

INSERT INTO `post_seo` (`id`, `post_id`, `title`, `author`, `url`, `keywords`, `description`) VALUES
(1, 1, 'Lorem ipsum', 'ALI GUEVARA', 'lorem-ipsum', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `role`, `role_name`) VALUES
(1, 'ROLE_ADMIN', 'ADMINISTRADOR WEB'),
(2, 'ROLE_MODERATOR', 'MODERADOR'),
(3, 'ROLE_USER', 'USUARIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` tinytext COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seo_section`
--

CREATE TABLE `seo_section` (
  `id` int(11) NOT NULL,
  `section` tinytext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id`, `name`, `description`, `create_at`, `update_at`) VALUES
(1, '1 PSICOLOGÍA', '', '2019-12-08 18:22:34', '2019-12-08 18:22:34'),
(2, '2 SEXOLOGÍA', '', '2019-12-08 18:22:50', '2019-12-08 18:22:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `image` tinytext COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `url` tinytext COLLATE utf8mb4_unicode_ci,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `image`, `content`, `url`, `create_at`, `update_at`) VALUES
(3, 'slider_881755dcc642e83d69.jpeg', '<h1 class=\"h1 light as\">Hay situaciones en las que, necesitamos ser escuchados</h1>\r\n<h3>y recibir ayuda profesional.</h3>', 'http://devapsicolegs.com', '2019-11-13 16:14:38', '2019-11-13 16:14:55'),
(4, 'slider_619935dcc651625a28.jpeg', '<h1 class=\"h1 light as\">Psicoterapia de Adultos, Niñ@s y Adolescentes</h1>\r\n<h3>Psicología Educativa y Logopedia </h3>', '', '2019-11-13 16:18:30', '2019-12-26 07:41:41'),
(5, 'slider_932995dcc653c6a1e5.jpeg', '<h1 class=\"h1 light as\"> Sexología: Terapia Sexual y/o de pareja</h1>\r\n<h3>Asesoramiento y Educación Sexual.</h3>', '', '2019-11-13 16:19:08', '2019-11-13 16:19:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_services`
--

CREATE TABLE `sub_services` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sub_services`
--

INSERT INTO `sub_services` (`id`, `service_id`, `name`, `description`, `create_at`, `update_at`) VALUES
(1, 1, 'PSICOTERAPIA DE ADULTOS', '<p>Contemplamos la Psicoterapia en la línea de la Psicología Humanista, preferentemente \"Gestalt\", en conjunción con un enfoque Psico-dinámico, dependiendo del profesional que intervenga, y lo que el caso en sí, requiera.<br><br>Aquí encontraremos tres áreas de intervención:</p><ul><li>Psicoterapia Individual: Las personas que acuden a consulta presentan distinta sintomatología. Ésta puede ir aislada o acompañada de otros síntomas o problemas que le impiden desarrollar su vida adecuadamente, en algún nivel de ésta, o en todos.</li><li>Psicoterapia de Grupo: Puede resultar de gran ayuda en el proceso terapéutico, aunque es importante la Psicoterapia Individual, a priori o paralelamente.</li><li>Psicoterapia de Familia: Ofrece un nuevo contexto en el que pueden comunicarse, expresar asuntos pendientes o no expresados, negociar la forma de relacionarse, como de convivir, etc.</li></ul>', '2019-12-08 18:23:34', '2019-12-08 18:23:34'),
(2, 1, 'PSICOTERAPIA INFANTO-JUVENIL', '<p>Se tratará al o la infante o adolescente, como un ser individual, que necesita una atención personalizada y propia, sin perder de vista, sus familiares más cercanos, a l@s que se les solicitará que acudan a consulta, en algunas ocasiones y que colaboren y participen, si fuera necesaria ésta.</p>', '2019-12-08 18:24:06', '2019-12-08 18:24:06'),
(3, 1, 'PSICOLOGÍA EDUCATIVA Y LOGOPEDIA ', '<p>Aquí podemos ver las diferentes áreas de intervención:</p><ul><li>Dificultades de aprendizaje: Dislexia, disgrafía y disortografía.</li><li>Déficit en comprensión, producción y uso del lenguaje.</li><li>Déficit de Atención e Hiperactividad.</li><li>Bajo rendimiento o fracaso escolar.</li><li>Técnicas de estudio.</li><li>Orientación y apoyo a las familias.</li></ul>', '2019-12-08 18:24:27', '2019-12-08 18:24:27'),
(4, 2, 'SEXOLOGÍA CLÍNICA', '<p>Terapia Sexual: Suelen ser problemas o dificultades comunes, y no graves, pero pueden presentarse de mayor gravedad, de esto dependerá las características de la intervención. Puede ser individual o en pareja, aunque es conveniente acudir en pareja, si ésta existe. Eyaculación precoz. Problemas de erección. Falta de deseo. Vaginismo. Conflictos de orientación sexual…</p><p>-Terapia de pareja: Ésta estará focalizada en la pareja, principalmente, y la intervención se efectuará sobre estos sujetos sexuados y la relación que mantienen. Celos. Problemas de relación y en la vivencia sexual. Intervención y acompañamiento en el proceso de separación</p>', '2019-12-08 18:24:53', '2019-12-08 18:24:53'),
(5, 2, 'ASESORAMIENTO SEXOLÓGICO ', '<p>Información, Asesoramiento u Orientación Sexual y resolver problemas de poca gravedad, tanto a nivel individual como en pequeños grupos.</p>', '2019-12-08 18:25:12', '2019-12-08 18:25:12'),
(6, 2, 'EDUCACIÓN SEXUAL ', '<p>Informar y formar, es el punto de partida. Enfatizará en la resolución de pequeños problemas, dudas, aclaraciones, en especial, sobre los distintos aspectos de la vivencia sexual y sus expresiones, en grupo.&nbsp;</p>', '2019-12-08 18:25:37', '2019-12-08 18:25:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'Artículos'),
(2, 'Sexología');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `profile_image` tinytext COLLATE utf8mb4_unicode_ci,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `phone` tinytext COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `team`
--

INSERT INTO `team` (`id`, `name`, `description`, `profile_image`, `create_at`, `update_at`, `phone`, `experience`) VALUES
(1, 'RAQUEL VALERO OLTRA', 'Psicología Clínica y Sexología Directora y fundadora del Centro', 'team_824645ded71f534f80.jpeg', '2019-12-08 17:58:13', '2019-12-26 08:50:50', '666474813', '<ul><li>Colegiada CV-01040. Especialista en Psicología Clínica y Sexología. DEA (Diploma de Estudios Avanzados).</li><li>Diploma Post-universitario en Mediación. Formación en distintos enfoques terapéuticos, resaltando: Gestalt, Psicoanálisis y Sistémica.</li><li>Fundadora de INTESEX (Investigación, Terapia Psicológica y Sexología) y de DEVA:: centro de Psicología y Sexología.</li><li>Miembro de la Mesa Permanente de Salud y Responsable de la Comisión de Sexología y Planificación Familiar del Colegio Oficial de Psicología de la Comunidad Valenciana.</li><li>Ha trabajado en el Gabinete Psicopedagógico del Ayuntamiento de Burriana (1983 a 1988) y como Sexóloga en el Centro de Planificación Familiar de Burriana y en el Hospital Dr. Peset de Valencia (1987 a 1993).</li><li>Asesora para la Consellería de Sanidad en materia de Planificación Familiar (1989). Directora de dos promociones de Máster de Sexología (1982 a 2000), avalados por el COP-CV.</li></ul>'),
(2, 'GLORIA SEMPERE FIGUÉREZ', 'Psicóloga con Habilitación Sanitaria. Especialista en Psicoterapia Infanto-Juvenil. ', 'team_957895ded721e9a3e7.jpeg', '2019-12-08 17:58:54', '2019-12-08 18:15:06', '697660173', '<ul><li>Especialista en Psicoterapia Infanto-Juvenil.</li><li>Miembro de la Comisión de Sexología y Planificación Familiar del COPCV.</li><li>Formación práctica de Postgrado en Mindfulness.</li><li>Prácticas voluntarias en Centro de Enfermedad Mental.</li><li>Formación para Profesionales que Intervienen en el ámbito de la Infancia y la Adolescencia de la Comunidad Valenciana</li><li>Coterapeuta en Grupos de Crecimiento Personal</li><li>Perteneciente a grupo de Crecimiento Personal</li><li>Tutorizada en el área de Psicología Clínica y Salud mediante el programa Galatea Mentoring del Colegio Oficial de Psicología de la Comunitat Valenciana</li></ul>'),
(3, 'JULIA TORTAJADA SANFELIU', 'Doble Grado en Psicología y Logopeda', 'team_31445ded72b92f492.jpeg', '2019-12-08 18:01:29', '2019-12-08 18:16:54', '', '<ul><li>Logopeda</li><li>Técnico de Selección de Personal de Recursos Humanos.</li><li>Formación en Técnicas Gestálticas aplicadas en la Infancia y la Adolescencia.</li><li>Perteneciente a Grupo de Crecimiento Personal.</li><li>Psicóloga voluntaria en Acción Social UCV y Coordinadora Voluntaria en Proyecte Somriure (Cáritas) .</li><li>Miembro de la Comisión de Sexología y Planificación Familiar del COPCV.</li></ul>'),
(4, 'ANTONIO NORTES PASTOR', 'Grado en Psicología', 'team_412475ded72daa8cfc.jpeg', '2019-12-08 18:02:02', '2019-12-08 18:17:31', '', '<ul><li>Máster en Sexología: Educación Sexual y Asesoramiento Sexológico.</li><li>Prácticas en Intervención Comunitarias.</li><li>Colaborador en CAVAS (Centro de Asistencia a Víctimas de Agresión Sexual): Puntos Violetas y charlas de prevención de Violencia Machista a niños con riesgo de exclusión social.</li><li>Miembro de la Comisión de Sexología y Planificación Familiar del COPCV.</li></ul>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `session_token` text COLLATE utf8mb4_unicode_ci,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `status_id`, `username`, `email`, `password`, `username_canonical`, `email_canonical`, `profile_image`, `code`, `last_login`, `password_requested_at`, `session_token`, `create_at`, `update_at`) VALUES
(1, 1, 2, 'MACROALI', 'macroali@gmail.com', '$2y$12$sfYKFZlzfp53s5VQyKhPW.R5W/RjFBP8PSDqycq8.6nclmvUCtkpe', 'MACROALI', 'macroali@gmail.com', NULL, 'ADM001', '2019-12-26 08:47:45', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzczNjQ0NjUsImV4cCI6MTU3NzQ1MDg2NSwiYXVkIjoiMjlhZmYxNTU2Mzg3MzEzNGZmNmQ0NDY2M2Y5NzY4ZWVlZWJlZjdjMyIsImRhdGEiOnsidXNlcl9pZCI6MSwidXNlcm5hbWUiOiJNQUNST0FMSSJ9fQ.XlIH4S0BMbf6tmYGYZARNXuDmx87afPWGCFxoj4XYlc', '2019-07-16 00:00:00', '2019-12-26 08:47:45'),
(2, 1, 2, 'ADMIN', 'admin@admin.com', '$2y$12$f216xPJvZsDC6zdlNQQmheUJOHKGXdMItn5wMjoYEUYytSlAGZjcS', 'ADMIN', 'admin@admin.com', NULL, '2a6H5H', '2019-12-26 07:41:09', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzczNjA0NjksImV4cCI6MTU3NzQ0Njg2OSwiYXVkIjoiZmU2MjQ5ODY3OTlmNzVlYTRlOWM5Nzc4NDEzYTgyZWU4MzZhOWZlZCIsImRhdGEiOnsidXNlcl9pZCI6MiwidXNlcm5hbWUiOiJBRE1JTiJ9fQ.pm9bB-V0z40t6nmZldV_mPbYe0h1tLdaRTmIL6aF-9Y', '2019-10-27 20:00:35', '2019-12-26 07:41:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_information`
--

CREATE TABLE `user_information` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date DEFAULT NULL,
  `city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` text COLLATE utf8mb4_unicode_ci,
  `address_2` text COLLATE utf8mb4_unicode_ci,
  `phone_1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `signature` text COLLATE utf8mb4_unicode_ci,
  `dni` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_information`
--

INSERT INTO `user_information` (`id`, `user_id`, `country_id`, `name`, `last_name`, `birthdate`, `city`, `address_1`, `address_2`, `phone_1`, `phone_2`, `note`, `signature`, `dni`, `postal_code`, `region`) VALUES
(2, 1, 1, 'ALI', 'GUEVARA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 73, 'NOMBRE', 'APELLIDO', '2011-11-11', '', '', '', '', '', '', '', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_logs`
--

CREATE TABLE `user_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_access` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_proxy` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_company` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_logs`
--

INSERT INTO `user_logs` (`id`, `user_id`, `username`, `action`, `ip_access`, `ip_proxy`, `ip_company`, `create_at`) VALUES
(1, 1, 'MACROALI', 'Inicio de sesión', '190.142.131.120', NULL, NULL, '2019-10-27 19:59:12'),
(2, 2, 'ADMIN', 'Inicio de sesión', '190.142.131.120', NULL, NULL, '2019-10-27 20:02:06'),
(3, 2, 'ADMIN', 'Inicio de sesión', '190.142.131.120', NULL, NULL, '2019-10-27 20:03:44'),
(4, 2, 'ADMIN', 'Inicio de sesión', '90.169.74.171', NULL, NULL, '2019-10-28 04:47:56'),
(5, 1, 'MACROALI', 'Inicio de sesión', '190.216.229.17', NULL, NULL, '2019-11-13 16:03:56'),
(6, 1, 'MACROALI', 'Creando imagen de carrusel. ID: 1', '190.216.229.17', NULL, NULL, '2019-11-13 16:12:03'),
(7, 1, 'MACROALI', 'Creando imagen de carrusel. ID: 2', '190.216.229.17', NULL, NULL, '2019-11-13 16:12:46'),
(8, 1, 'MACROALI', 'Creando imagen de carrusel. ID: 3', '190.216.229.17', NULL, NULL, '2019-11-13 16:14:38'),
(9, 1, 'MACROALI', 'Editando imagen de carrusel. ID: 3', '190.216.229.17', NULL, NULL, '2019-11-13 16:14:55'),
(10, 1, 'MACROALI', 'Creando imagen de carrusel. ID: 4', '190.216.229.17', NULL, NULL, '2019-11-13 16:18:30'),
(11, 1, 'MACROALI', 'Creando imagen de carrusel. ID: 5', '190.216.229.17', NULL, NULL, '2019-11-13 16:19:08'),
(12, 1, 'MACROALI', 'Inicio de sesión', '190.216.229.17', NULL, NULL, '2019-11-28 08:56:48'),
(13, 2, 'ADMIN', 'Inicio de sesión', '190.216.229.17', NULL, NULL, '2019-11-28 09:00:52'),
(14, 2, 'ADMIN', 'Inicio de sesión', '90.169.74.171', NULL, NULL, '2019-11-28 09:31:38'),
(15, 1, 'MACROALI', 'Inicio de sesión', '190.206.214.190', NULL, NULL, '2019-11-28 10:21:42'),
(16, 2, 'ADMIN', 'Inicio de sesión', '81.172.13.119', NULL, NULL, '2019-11-28 12:34:03'),
(17, 2, 'ADMIN', 'Inicio de sesión', '81.172.13.119', NULL, NULL, '2019-11-28 12:52:36'),
(18, 2, 'ADMIN', 'Inicio de sesión', '81.172.13.119', NULL, NULL, '2019-12-02 11:52:05'),
(19, 2, 'ADMIN', 'Inicio de sesión', '81.172.13.119', NULL, NULL, '2019-12-03 09:01:05'),
(20, 2, 'ADMIN', 'Inicio de sesión', '81.172.13.119', NULL, NULL, '2019-12-07 12:29:01'),
(21, 1, 'MACROALI', 'Inicio de sesión', '181.208.32.32', NULL, NULL, '2019-12-08 17:50:53'),
(22, 1, 'MACROALI', 'Editando Informacion / Historia', '181.208.32.32', NULL, NULL, '2019-12-08 17:51:58'),
(23, 1, 'MACROALI', 'Editando Informacion / Equipo', '181.208.32.32', NULL, NULL, '2019-12-08 17:58:13'),
(24, 1, 'MACROALI', 'Editando Informacion / Equipo', '181.208.32.32', NULL, NULL, '2019-12-08 17:58:54'),
(25, 1, 'MACROALI', 'Editando Informacion / Equipo', '181.208.32.32', NULL, NULL, '2019-12-08 18:01:29'),
(26, 1, 'MACROALI', 'Editando Informacion / Equipo', '181.208.32.32', NULL, NULL, '2019-12-08 18:02:02'),
(27, 1, 'MACROALI', 'Editando miembro. ID:1 - nombre: RAQUEL VALERO OLTRA', '181.208.32.32', NULL, NULL, '2019-12-08 18:06:00'),
(28, 1, 'MACROALI', 'Editando miembro. ID:2 - nombre: GLORIA SEMPERE FIGUÉREZ', '181.208.32.32', NULL, NULL, '2019-12-08 18:15:06'),
(29, 1, 'MACROALI', 'Editando miembro. ID:3 - nombre: JULIA TORTAJADA SANFELIU', '181.208.32.32', NULL, NULL, '2019-12-08 18:16:54'),
(30, 1, 'MACROALI', 'Editando miembro. ID:4 - nombre: ANTONIO NORTES PASTOR', '181.208.32.32', NULL, NULL, '2019-12-08 18:17:31'),
(31, 1, 'MACROALI', 'Editando Informacion / Servicios', '181.208.32.32', NULL, NULL, '2019-12-08 18:22:34'),
(32, 1, 'MACROALI', 'Editando Informacion / Servicios', '181.208.32.32', NULL, NULL, '2019-12-08 18:22:50'),
(33, 1, 'MACROALI', 'Agregando sub-servicio. ID:1 - nombre: Psicoterapia de adultos', '181.208.32.32', NULL, NULL, '2019-12-08 18:23:34'),
(34, 1, 'MACROALI', 'Agregando sub-servicio. ID:2 - nombre: Psicoterapia Infanto-juvenil', '181.208.32.32', NULL, NULL, '2019-12-08 18:24:06'),
(35, 1, 'MACROALI', 'Agregando sub-servicio. ID:3 - nombre: Psicología Educativa y Logopedia ', '181.208.32.32', NULL, NULL, '2019-12-08 18:24:27'),
(36, 1, 'MACROALI', 'Agregando sub-servicio. ID:4 - nombre: Sexología Clínica', '181.208.32.32', NULL, NULL, '2019-12-08 18:24:53'),
(37, 1, 'MACROALI', 'Agregando sub-servicio. ID:5 - nombre: Asesoramiento Sexológico ', '181.208.32.32', NULL, NULL, '2019-12-08 18:25:12'),
(38, 1, 'MACROALI', 'Agregando sub-servicio. ID:6 - nombre: Educación Sexual ', '181.208.32.32', NULL, NULL, '2019-12-08 18:25:37'),
(39, 2, 'ADMIN', 'Inicio de sesión', '81.172.13.119', NULL, NULL, '2019-12-09 06:30:32'),
(40, 1, 'MACROALI', 'Inicio de sesión', '190.216.229.17', NULL, NULL, '2019-12-19 08:35:42'),
(41, 1, 'MACROALI', 'Creando categoría. ID: 1, Nombre: Desde la Psicología', '190.216.229.17', NULL, NULL, '2019-12-19 08:36:14'),
(42, 1, 'MACROALI', 'Creando categoría. ID: 2, Nombre: Sexología y diversidad', '190.216.229.17', NULL, NULL, '2019-12-19 08:36:29'),
(43, 1, 'MACROALI', 'Creando categoría. ID: 3, Nombre: Actualidad y sociedad', '190.216.229.17', NULL, NULL, '2019-12-19 08:36:39'),
(44, 1, 'MACROALI', 'Creando categoría. ID: 4, Nombre: Actividades', '190.216.229.17', NULL, NULL, '2019-12-19 08:36:46'),
(45, 1, 'MACROALI', 'Creando categoría. ID: 5, Nombre: Artículos', '190.216.229.17', NULL, NULL, '2019-12-19 08:36:55'),
(46, 1, 'MACROALI', 'Creando categoría. ID: 6, Nombre: Galería', '190.216.229.17', NULL, NULL, '2019-12-19 08:37:09'),
(47, 1, 'MACROALI', 'Creando etiqueta. ID: 1, Nombre: Artículos', '190.216.229.17', NULL, NULL, '2019-12-19 08:37:40'),
(48, 1, 'MACROALI', 'Creando etiqueta. ID: 2, Nombre: Sexología', '190.216.229.17', NULL, NULL, '2019-12-19 08:38:03'),
(49, 1, 'MACROALI', 'Creando publicación. ID: 1, Nombre: Lorem ipsum', '190.216.229.17', NULL, NULL, '2019-12-19 08:40:24'),
(50, 1, 'MACROALI', 'Editando publicación. ID: 1, Nombre: Lorem ipsum', '190.216.229.17', NULL, NULL, '2019-12-19 08:40:33'),
(51, 2, 'ADMIN', 'Inicio de sesión', '90.169.74.171', NULL, NULL, '2019-12-26 07:41:09'),
(52, 2, 'ADMIN', 'Editando imagen de carrusel. ID: 4', '90.169.74.171', NULL, NULL, '2019-12-26 07:41:41'),
(53, 2, 'ADMIN', 'Editando miembro. ID:1 - nombre: RAQUEL VALERO OLTRA', '90.169.74.171', NULL, NULL, '2019-12-26 08:35:44'),
(54, 2, 'ADMIN', 'Editando miembro. ID:1 - nombre: RAQUEL VALERO OLTRA', '90.169.74.171', NULL, NULL, '2019-12-26 08:36:13'),
(55, 1, 'MACROALI', 'Inicio de sesión', '190.206.214.190', NULL, NULL, '2019-12-26 08:47:45'),
(56, 1, 'MACROALI', 'Editando miembro. ID:1 - nombre: RAQUEL VALERO OLTRA', '190.206.214.190', NULL, NULL, '2019-12-26 08:50:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_status`
--

CREATE TABLE `user_status` (
  `id` int(11) NOT NULL,
  `description` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_status`
--

INSERT INTO `user_status` (`id`, `description`) VALUES
(1, 'No verificada'),
(2, 'Verificada'),
(3, 'Suspendida'),
(4, 'De baja');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '1: registro',
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_885DBAFAA76ED395` (`user_id`);

--
-- Indices de la tabla `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD PRIMARY KEY (`post_id`,`postcategory_id`),
  ADD KEY `IDX_A8C3AA464B89032C` (`post_id`),
  ADD KEY `IDX_A8C3AA465275645B` (`postcategory_id`);

--
-- Indices de la tabla `posts_category`
--
ALTER TABLE `posts_category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `posts_subcategories`
--
ALTER TABLE `posts_subcategories`
  ADD PRIMARY KEY (`post_id`,`postsubcategory_id`),
  ADD KEY `IDX_83A346FF4B89032C` (`post_id`),
  ADD KEY `IDX_83A346FFEDE7006A` (`postsubcategory_id`);

--
-- Indices de la tabla `posts_subcategory`
--
ALTER TABLE `posts_subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D198996B12469DE2` (`category_id`);

--
-- Indices de la tabla `posts_tags`
--
ALTER TABLE `posts_tags`
  ADD PRIMARY KEY (`post_id`,`tags_id`),
  ADD KEY `IDX_D5ECAD9F4B89032C` (`post_id`),
  ADD KEY `IDX_D5ECAD9F8D7B4FB4` (`tags_id`);

--
-- Indices de la tabla `post_comments`
--
ALTER TABLE `post_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E0731F8B4B89032C` (`post_id`);

--
-- Indices de la tabla `post_gallery`
--
ALTER TABLE `post_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7AC3CF094B89032C` (`post_id`);

--
-- Indices de la tabla `post_seo`
--
ALTER TABLE `post_seo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_353661434B89032C` (`post_id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6C71EC30D823E37A` (`section_id`);

--
-- Indices de la tabla `seo_section`
--
ALTER TABLE `seo_section`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sub_services`
--
ALTER TABLE `sub_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C99D0F30ED5CA9E6` (`service_id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E9F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`),
  ADD KEY `IDX_1483A5E9D60322AC` (`role_id`),
  ADD KEY `IDX_1483A5E96BF700BD` (`status_id`);

--
-- Indices de la tabla `user_information`
--
ALTER TABLE `user_information`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8062D116A76ED395` (`user_id`),
  ADD KEY `IDX_8062D116F92F3E70` (`country_id`);

--
-- Indices de la tabla `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8A0E8A95A76ED395` (`user_id`);

--
-- Indices de la tabla `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT de la tabla `information`
--
ALTER TABLE `information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `posts_category`
--
ALTER TABLE `posts_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `posts_subcategory`
--
ALTER TABLE `posts_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `post_comments`
--
ALTER TABLE `post_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `post_gallery`
--
ALTER TABLE `post_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `post_seo`
--
ALTER TABLE `post_seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `seo_section`
--
ALTER TABLE `seo_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `sub_services`
--
ALTER TABLE `sub_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `user_information`
--
ALTER TABLE `user_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT de la tabla `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `FK_885DBAFAA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD CONSTRAINT `FK_A8C3AA465275645B` FOREIGN KEY (`postcategory_id`) REFERENCES `posts_category` (`id`),
  ADD CONSTRAINT `FK_A8C3AA464B89032C` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Filtros para la tabla `posts_subcategories`
--
ALTER TABLE `posts_subcategories`
  ADD CONSTRAINT `FK_83A346FFEDE7006A` FOREIGN KEY (`postsubcategory_id`) REFERENCES `posts_subcategory` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_83A346FF4B89032C` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `posts_subcategory`
--
ALTER TABLE `posts_subcategory`
  ADD CONSTRAINT `FK_D198996B12469DE2` FOREIGN KEY (`category_id`) REFERENCES `posts_category` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `posts_tags`
--
ALTER TABLE `posts_tags`
  ADD CONSTRAINT `FK_D5ECAD9F8D7B4FB4` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D5ECAD9F4B89032C` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `post_comments`
--
ALTER TABLE `post_comments`
  ADD CONSTRAINT `FK_E0731F8B4B89032C` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `post_gallery`
--
ALTER TABLE `post_gallery`
  ADD CONSTRAINT `FK_7AC3CF094B89032C` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `post_seo`
--
ALTER TABLE `post_seo`
  ADD CONSTRAINT `FK_353661434B89032C` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `seo`
--
ALTER TABLE `seo`
  ADD CONSTRAINT `FK_6C71EC30D823E37A` FOREIGN KEY (`section_id`) REFERENCES `seo_section` (`id`);

--
-- Filtros para la tabla `sub_services`
--
ALTER TABLE `sub_services`
  ADD CONSTRAINT `FK_C99D0F30ED5CA9E6` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_1483A5E96BF700BD` FOREIGN KEY (`status_id`) REFERENCES `user_status` (`id`),
  ADD CONSTRAINT `FK_1483A5E9D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Filtros para la tabla `user_information`
--
ALTER TABLE `user_information`
  ADD CONSTRAINT `FK_8062D116F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  ADD CONSTRAINT `FK_8062D116A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_logs`
--
ALTER TABLE `user_logs`
  ADD CONSTRAINT `FK_8A0E8A95A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

COMMIT;
SET FOREIGN_KEY_CHECKS=1;
