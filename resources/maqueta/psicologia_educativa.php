<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">PSICOLOGÍA</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Psicoterapia infanto-juvenil</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-md-push-8 col-lg-3 col-lg-push-9">
						<div class="mobileSearch large">
							Search
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<ul class="categoryList normall">
								<li><a href="psicologia.php">Psicología Clínica</a></li>
								<li><a href="sicoterapia-infanto-juvenil.php">Psicoterapia Infanto-juvenil</a></li>
								<li class="activeCat"><a href="#l">Psicología Educativa</a></li>
							</ul>
							
														<div class="openingHours">
								<h6 class="h6 as">Cita Previa:</h6>
								<ul class="normall">
									<li><span>Raquel Valero</span> <span>666 474 813 </span> <div class="clear"></div></li>
									<li><span>Gloria Sempere</span> <span>697 660 173</span> <div class="clear"></div></li>
								</ul>
							</div>
							
							<hr>

							<div class="openingHours">
								<h6 class="h6 as">Horario</h6>
								<ul class="normall">
									<li><span>De lunes a viernes</span> <span>Mañanas y tardes</span> <div class="clear"></div></li>
									<li><span>Sábado mañana</span> <span>Con cita previa</span> <div class="clear"></div></li>
								</ul>
							</div>

						</aside>
					</div>
					<div class="col-sm-12 col-md-8 col-md-pull-4 col-lg-9 col-lg-pull-3">
						<div class="mainServicesContent">
							
							
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<div class="row">
									<div class="col-sm-7">
										<div class="imgWrapper">
											<img src="img/psicologia1.jpg" alt="">
										</div>
										<div class="emptySpace-xs20"></div>
									</div>
									<div class="col-sm-5">
										<div class="imgWrapper">
											<img src="img/psicologia2.jpg" alt="">
										</div>
									</div>
								</div>
								
								<div class="emptySpace50 emptySpace-xs30"></div>
								
								<div class="blogContent">
									<div class="simple-article normall">
										<h5>PSICOLOGÍA EDUCATIVA</h5>
										<p>
Durante la infancia y la adolescencia, el ámbito escolar es una de las áreas donde pasan gran parte de su crecimiento y evolución. Pasan largos periodos dentro del sistema educativo y este ámbito pasa a ser de gran importancia para su desarrollo y posterior evolución. Aquí será donde empiezan sus relaciones sociales con iguales y será de gran relevancia que se muevan con seguridad y confianza, tanto a nivel relacional como en el área del aprendizaje. La falta de medios y de profesionales dentro de la población educativa, obliga a padres y profesores a solicitar una evaluación y tratamiento externos para el mejor desarrollo y evolución de estos infantes o adolescentes. Pueden ser problemas puntuales en el desarrollo, en la relación con sus iguales u otros, o por posibles problemas o dificultades en el aprendizaje. 
<br><br>
Las dificultades específicas del aprendizaje son la primera causa de bajo rendimiento académico y fracaso escolar. 
<br><br>
En Logopedia, partimos de una premisa inexcusable, y es que l@s niñ@s tienen características, debilidades y fortalezas que la terapeuta conocerá para hacer más productiva la intervención. De esta manera, la terapia se transforma en una actividad dinámica donde la logopeda conoce las particularidades de la persona para adaptar las actividades/pautas a cada paciente en concreto.
<br><br>
En definitiva, el objetivo de la terapia logopédica es mejorar las capacidades de aprendizaje y producción del lenguaje cuyo fin es ganar en calidad de vida.

									</p>
									
										<p>	
											<h6>Áreas de intervención en este ámbito:</h6>
											<ul>
												<li>Dificultades de aprendizaje: Dislexia, disgrafía y disortografía.</li>
												<li>Déficit en comprensión, producción y uso del lenguaje.</li>
												<li>Déficit de Atención e Hiperactividad. </li>
												<li>Déficit de Atención e Hiperactividad.</li>
												<li>Bajo rendimiento o fracaso escolar.</li>
												<li>Técnicas de estudio.</li>
												<li>Orientación y apoyo a las familias.</li>
												
											<ul>
											<hr>
										</p><br><br>
										
									</div>
								</div>
										
							</div>
							<!-- 	Blog1-END 	-->
							
						</div>
					</div>
				</div>
			</div>
		</div>
		

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
