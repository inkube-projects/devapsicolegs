<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body class="homepage2">
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
	 
   <div class="headerClearFix headerfix2"></div>
      	
		<!-- 	Main banner swiper-START 	-->
		<div class="swiperMainWrapper mainSwiperbanner index2slider">
			<!-- 	Swiper slider buttons-START 	-->
			<div class="swipert-black-button swiper-button-prev"></div>
			<div class="swipert-black-button swiper-button-next"></div>
			<!-- 	Swiper slider buttons-END 	-->
			<div class="swiper-container" data-auto-height="1" data-effect="fadde" data-speed="600" data-autoplay="5000" data-loop="1">
				<div class="swiper-wrapper">
					
					
					
					<!-- 	Slide1-START 	-->
					<div class="homepage5 swiper-slide mainBanner"> 
						<div class="sliderBg" style="background-image: url('<?=URL?>/img/slider01.jpg')"></div>
						<div class="container">
							<div class="row">
								
								<div class="col-xs-12 col-md-12">
								
									<div class="cell-view">
									<div class="rightone">
										<div class="bannerTitle">
											<h1 class="h1 light as">Hay situaciones en las que, necesitamos ser escuchados
 </h1>
											<h3>y recibir ayuda profesional.</h3>
											
										</div>
										<div class="bannerBtnWrapper">
											<a href="#" class="button btnSize1 ">+ información</a>
										</div>
									</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<!-- 	Slide1-END 	--> 
					
					<!-- 	Slide2-START 	-->
					<div class="homepage5 swiper-slide mainBanner"> 
						<div class="sliderBg" style="background-image: url('<?=URL?>/img/slider02.jpg')"></div>
						<div class="container">
							<div class="row">
							
							  <div class="col-xs-12 col-md-12">
								
									<div class="cell-view">
									<div class="rightone">
										<div class="bannerTitle">
											<h1 class="h1 light as">Psicoterapia de Adultos, Niñ@s y Adolescentes</h1>
											<h3>Psicología Educativa y logopedia. </h3>
											
										</div>
										<div class="bannerBtnWrapper">
											<a href="#" class="button btnSize1 ">+ información</a>
										</div>
									</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<!-- 	Slide2-END 	-->
					
					<!-- 	Slide3-START 	-->
					<div class="homepage5 swiper-slide mainBanner "> 
						<div class="sliderBg" style="background-image: url('<?=URL?>/img/slider03.jpg')"></div>
						<div class="container">
							<div class="row">
							
								
								<div class="col-xs-12 col-md-12">
								
									<div class="cell-view">
									<div class="rightone">
										<div class="bannerTitle">
											<h1 class="h1 light as"> Sexología: Terapia Sexual y/o de pareja</h1>
											<h3>Asesoramiento y Educación Sexual.</h3>
											
										</div>
										<div class="bannerBtnWrapper">
											<a href="#" class="button btnSize1 ">+ información</a>
										</div>
									</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<!-- 	Slide3-END 	-->
					
				</div>
			</div>

		</div>
		<!-- 	Main banner swiper-END 	-->
	 
	 
	 <div class="strip strip5">
			<div class="container">
			    <div class="row">
				     <div class="col-md-5">
				         <div class="single-content">
				             <div class="icon">
				                 <i class="flaticon1-smartphone-call"></i>
				             </div>
				             <div class="text">
				                 <h4>Cita Previa</h4>
				                 <p>Raquel Valero: 666 474 813<br>Gloria Sempere: 697 660 173</p>
				             </div>
							 <div class="line"></div>

				         </div>
				     </div>
					 
				     <div class="col-md-4">
				         <div class="single-content">
				             <div class="icon">
				                <i class="flaticon1-envelope"></i>
				             </div>
				             <div class="text">
				                <h4>Mail</h4>
				                <p>hola@psicologiaysexologia.es</p>
				             </div>
							  <div class="line"></div>
			             </div>
				     </div>

				     <div class="col-md-3">
				        <div class="single-content">
				             <div class="icon">
				                <i class="flaticon2-placeholder"></i>
				             </div>
				             <div class="text">
				                 <h4>Ubicación</h4>
				                 <p>C/ Francisco Martínez, 1-6º-18ª <br>46020 - Benimaclet (Valencia)</p>
				             </div>
				        </div>
				    </div>
				</div>
		    </div>
		</div>
		

	<div class="welcome_consulting">
		   <div class="no-bottom">
			<div class="container">
				<div class="row">
		           <div class="col-md-5">
					<img alt="" src="<?=URL?>/img/raquel.png" class="img-responsive">
					</div>
		
		            <div class="col-md-7">
					    <div class="contentTitle normall css-padding-top120">
							<h2>DEVA <span>Psicología y Sexología</span></h2>
							
							<h4>Deva: Centro de  Psicología y Sexología (INTESEX)</h4>
						
							<p>Mi vida ha sido un continuo trasiego de acá para allá. Me llevaron a Santiago (Chile), siendo muy pequeña y me trajeron de vuelta a mi ciudad natal (Valencia), de adolescente (mis padres, desde luego). Fui una niña que, siempre, tuvo claro que quería estudiar de mayor. Mi pretensión era hacer dos carreras, que no tenían nada en común: Químicas y Psicología. 
							<br><br>
							Finalmente, sólo estudié Psicología, y nunca, me he arrepentido. Me encanta mi trabajo. Hay momentos duros es cierto, pero es tan satisfactorio ver cómo la gente evoluciona y aunque, el mérito, siempre es, principalmente, de ell@s, es muy agradable sentir, que aunque sea un poco, hay algo que has puesto tú ahí, y se ha logrado…
<br><br>
Me he formado en casi todos los enfoques terapéuticos, aunque trabajo, preferentemente, en Gestalt, con una escucha más analítica, en su trasfondo. A la vez, fui a Madrid a Incisex, donde me formé, primero, en Educación Sexual, y posteriormente, realicé el Curso Superior de Sexología, que más tarde, la Universidad de Alcalá de Henares me convalidó como Máster. En el 2013-14, realicé un posgrado universitario en Mediación, aunque esa área la tengo en un segundo lugar.
<br><br>
Hace 25 años me vine a vivir a Benimaclet, donde vivo feliz. En el 2006 trasladé mi consulta a este encantador lugar, donde junto a mis compañer@s, quiero seguir trabajando y además, viviendo.
</p>
					    </div>
						
						<h4>Raquel Valero Oltra <em>(Especialista en Psicología Clínica y Sexología)</em></h4>
						<img alt="" src="<?=URL?>/img/psign.png" class="img-responsive">                                                                                                        
					</div>
		 
			    </div>
		    </div>
		</div>
		</div>
		

			<div class="about3">
		   <div class="contentPadding">
			<div class="container">
				<div class="row">
		           
		
		            <div class="col-md-6">
							<div class="contentTitle normall">
							<h2 class="h2 font-28">¿Que servicios ofrecemos?</h2>
							<h3>Disponemos de 2 áreas de interveción principales Psicología  y Sexología que a su vez se dividen en otras áreas. </h3>
							
							<p>Utilizamos diferentes métodos y ténicas según el caso de cada persona, puede conocer nuestros servicios pulsando más info.</p>
							</div>
						
						<div class="listing">
						<div class="col-md-6 col-sm-6">
							<p></p><h4> PISOCOLOGÍA</h4><p></p>
						 <ul>
						 <li>Psicoterapia de Adultos</li>
						 <li>Psicoterapia Infanto-juvenil</li>
						 <li>Psicología Educativa</li>
						 </ul>
						</div>
						
						<div class="col-md-6 col-sm-6">
							<p></p><h4>SEXOLOGÍA</h4><p></p>
						<ul>
						 <li>Sexología Clínica</li>
						 <li>Counseling o Asesoramiento</li>
						 <li>Educación Sexual</li>
						 </ul>
						</div>
						</div>
						
						<div class="col-md-12 col-sm-12">
      						<a href="#" class="button btnStyle3">+ información</a>
						</div>
					
					</div>
		
			    </div>
		    </div>
		</div>
		</div>


		
    	
		
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
				    <div class="col-xs-12">
				    	<div class="contentTitle normall">
							<h3 class="h3 as font-28">Últimos artículos del <span>Blog</span></h3>
						</div>
				    </div>
					<!-- 	News1-START 	-->
					<div class="col-xs-12 col-sm-4">
						<div class="tumbWrapper style2">
							<div class="imgWrapper">
								<img src="<?=URL?>/img/tumb-img2.jpg" alt="">
								<div class="timeBlock">01/09/2019</div>
							</div>
							<h6 class="h6 as"><a href="#">We give complete value chain of oil and  processing &amp; transportation. </a></h6>
							<div class="tumbContent small">
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
							</div>
							<a class="readMore" href="#">+ info</a>
						</div>
						<div class="emptySpace-xs30"></div>
					</div>
					<!-- 	News1-END 	-->
					
					<!-- 	News2-START 	-->
					<div class="col-xs-12 col-sm-4">
						<div class="tumbWrapper style2">
							<div class="imgWrapper">
								<img src="<?=URL?>/img/news-image.jpg" alt="">
								<div class="timeBlock">01/09/2019</div>
							</div>
							<h6 class="h6 as"><a href="#">We give complete value chain of oil and  processing &amp; transportation. </a></h6>
							<div class="tumbContent small">
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
							</div>
							<a class="readMore" href="#">+ info</a>
						</div>
						<div class="emptySpace-xs30"></div>
					</div>
					<!-- 	News2-END 	-->
					
					<!-- 	News3-START 	-->
					<div class="col-xs-12 col-sm-4">
						<div class="tumbWrapper style2">
							<div class="imgWrapper">
								<img src="<?=URL?>/img/news-image2.jpg" alt="">
								<div class="timeBlock">01/09/2019</div>
							</div>
							<h6 class="h6 as"><a href="#">We give complete value chain of oil and  processing &amp; transportation. </a></h6>
							<div class="tumbContent small">
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusa nt ium dolor emque laudantium totam.</p>
							</div>
							<a class="readMore" href="#">+ info</a>
						</div>
					</div>
					<!-- 	News3-END 	-->
				</div>
			</div>
		</div>

	 <?php include("includes/maps.php"); ?>
	 <?php include("includes/footer.php"); ?>
		
    </div>
    <!--END-->

     <?php include("includes/js.php"); ?>

    <link rel="stylesheet" href="owlcarousel/assets/owl.carousel.min.css">

    <script src="owlcarousel/owl.carousel.js"></script>
	
	    <script>
            $(document).on('ready', function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 3
                  },
                  1000: {
                    items: 6
                  }
                }
              })
            })
          </script>
</body>
</html>
