<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">SEXOLOGÍA</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Counseling</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-md-push-8 col-lg-3 col-lg-push-9">
						<div class="mobileSearch large">
							Search
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<ul class="categoryList normall">
								<li ><a href="sexologia.php">Introducción</a></li>
								<li><a href="educacion_sexual.php">Educación Sexual</a></li>
								<li class="activeCat"><a href="#">Consueling</a></li>
								<li><a href="sexologia_clinica.php">Sexología Clínica</a></li>
							
							</ul>
							
														<div class="openingHours">
								<h6 class="h6 as">Cita Previa:</h6>
								<ul class="normall">
									<li><span>Raquel Valero</span> <span>666 474 813 </span> <div class="clear"></div></li>
									<li><span>Gloria Sempere</span> <span>697 660 173</span> <div class="clear"></div></li>
								</ul>
							</div>
							
							<hr>

							<div class="openingHours">
								<h6 class="h6 as">Horario</h6>
								<ul class="normall">
									<li><span>De lunes a viernes</span> <span>Mañanas y tardes</span> <div class="clear"></div></li>
									<li><span>Sábado mañana</span> <span>Con cita previa</span> <div class="clear"></div></li>
								</ul>
							</div>

						</aside>
					</div>
					<div class="col-sm-12 col-md-8 col-md-pull-4 col-lg-9 col-lg-pull-3">
						<div class="mainServicesContent">
							
							
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<div class="row">
									<div class="col-sm-7">
										<div class="imgWrapper">
											<img src="img/stress-img.jpg" alt="">
										</div>
										<div class="emptySpace-xs20"></div>
									</div>
									<div class="col-sm-5">
										<div class="imgWrapper">
											<img src="img/stress-img2.jpg" alt="">
										</div>
									</div>
								</div>
								
								<div class="emptySpace50 emptySpace-xs30"></div>
								
								<div class="blogContent">
									<div class="simple-article normall">
										<h5>COUNSELING</h5>
										
										<h6 class="h6 as">Sex Counseling o Asesoramiento Sexual</h6>
										<p>
<strong>Objetivo: </strong>Información, formación, aclaración, orientación y resolución de problemas comunes y de poca gravedad, dudas, dificultades, desinformación.<br><br>
											
<strong>Formato: </strong>su intervención se presenta en pareja o individualmente, aunque también se puede realizar con un grupo pequeño.<br><br>
											
Se plantea una forma de intervención basada en la <strong>compresión y en el cultivo</strong>. Y puesto que se centra en el sujeto y su biografía, no existen recetas, en sí. Las soluciones se alcanzan a través de un método reflexivo a partir del sujeto, y su capacidad de darse cuenta y aprender de su experiencia y vivencias, en una palabra de su “ser y estar en el mundo”.<br><br>
											
Se pone el énfasis: en la forma en la que los sujetos se configuran como <strong>sexuad@s </strong>y en cómo <strong>se relacionan,</strong> como tales. Veremos que es una concepción más global, que estudia la <strong>"realidad sexual"</strong> desde una perspectiva biográfica y relacional. Así pues el objeto de estudio es el sujeto sexuado, tanto en sí mismo como en relación con otr@ u otr@s. Es decir, no se va a centrar la atención en el problema ni en el síntoma, sino en el <strong>"ser sexuad@ y sus vivencias".</strong><br><br>
											
Se plantea la cuestión actitudinal del <strong>Asesor/a Sexológico,</strong> que pasa de tener una actitud normativa (anteriormente, utilizada) a tener una actitud de cultivo y comprensión.<br><br>
											
Se trataría, pues, de una forma de intervención sexológica que permitiría trabajar desde las <strong>capacidades</strong> de la propio <strong>persona,</strong> profundizando en sus fuentes de riqueza, y no tanto para curar patologías como para <strong>cultivar sus posibilidades,</strong> siguiendo el axioma de Havelock Ellis, que Efigenio  Amezúa menciona con frecuencia en sus obras: "en Sexología hay más fenómenos cultivables que trastornos curables".<br><br>
											
Utilizamos el término <strong>“sexológico”</strong> por parecernos más correcto, pero debido que <strong>"sexual"</strong> se emplea más frecuentemente, a pesar de que emerja de <strong>"sexo"</strong> y no de<strong> "sexología",</strong> como sería lo adecuado, haremos uso de ambos indistintamente.
										</p>
											
											<hr>
											
									</div>
								</div>

										
							</div>
							<!-- 	Blog1-END 	-->
							
						</div>
					</div>
				</div>
			</div>
		</div>
		

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
