<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->

     	<div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">EL CENTRO</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Nota Legal</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		<!-- 	About our-START 	-->
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-9 col-md-offset-1 center-block">
						<div class="aboutOur">
			
							<h2>DEVA <span>Psicología y Sexlogía</span></h2><br><br>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
							</p>
							</div>
							
														<span>OBJETO:</span>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
								La presente página Web ha sido diseñada para dar a conocer los productos ofertados por Raquel Valero Oltra con CIF22604867F, con domicilio social en C/Francisco Martinez, 1-18ª. 46020 (Benimaclet) Valencia.
<br><br>
							</p>
							</div>
							
														<span>PROPIEDAD INTELECTUAL E INDUSTRIAL:</span>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
								Los derechos de propiedad intelectual de la página www.psicologiaysexologia.es, su código fuente, diseño, estructuras de navegación y los distintos elementos en ella contenidos son titularidad de Raquel Valero Oltra, a quien corresponde el ejercicio exclusivo de los derechos de explotación de los mismos en cualquier forma y, en especial, los derechos de reproducción, distribución, comunicación pública y transformación, de acuerdo con la legislación española y de la unión europea aplicable.<br><br>
							</p>
							</div>
							
														<span>CONTENIDOS:</span>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
								Se facilita a través de este Web información acerca de productos destinados a conocimiento público que en todo caso se sujetarán a los términos y condiciones expresamente detallados en cada momento y que son accesibles desde esta página Web, los cuales se sujetarán a las distintas disposiciones legales de aplicación.<br><br>
							</p>
							</div>
							
														<span>RESPONSABILIDAD:</span>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
								Tanto el acceso a ésta página Web, como el uso que pueda hacerse de la información y contenidos incluidos en la misma, será de la exclusiva responsabilidad de quien lo realice. Las condiciones de acceso a este Web estarán supeditadas a la legalidad vigente y los principios de la buena fe y uso lícito por parte del usuario de la misma, quedando prohibido con carácter general cualquier tipo de actuación en perjuicio de Raquel Valero Oltra Se considerará terminantemente prohibido el uso de la presente página Web con fines ilegales o no autorizados.<br><br>
							</p>
							</div>
							
														<span>SERVICIOS:</span>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
								Raquel Valero Oltra se reserva el derecho de suspender el acceso a su página Web, sin previo aviso, de forma discrecional y temporal, por razones técnicas o de cualquier otra índole, pudiendo asimismo modificar unilateralmente tanto las condiciones de acceso, como la totalidad o parte de los contenidos en ella incluidos.<br><br>
							</p>
							</div>
							
														<span>PROTECCION DE DATOS:</span>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
								De conformidad con la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), los datos suministrados por el Usuario quedarán incorporados en ficheros automatizados, los cuales serán procesados exclusivamente para la finalidad descrita, según se especifique en la leyenda adjunta a cada formulario. Los datos que se recogerán a través de los formularios correspondientes sólo contendrán los campos imprescindibles para poder prestar el servicio o información requerida por el Usuario. Los datos de carácter personal serán tratados con el grado de protección adecuado, según el Real Decreto 994/1999 de 11 de junio, tomándose las medidas de seguridad necesarias para evitar su alteración, pérdida, tratamiento o acceso no autorizado por parte de terceros que lo puedan utilizar para finalidades distintas para las que han sido solicitados al Usuario. Como único responsable del fichero, Raquel Valero Oltra se compromete al cumplimiento de su obligación de secreto de los datos de carácter personal y de su deber de guardarlos, y adoptará las medidas necesarias para evitar su alteración, pérdida, tratamiento o acceso no autorizado, habida cuenta en todo momento del estado de la tecnología. Finalmente, el Usuario podrá ejercer sus derechos de oposición, acceso, rectificación y cancelación en cumplimiento de lo establecido en la LOPD, o bien para cualquier consulta o comentario personal a este respecto, sin que en ningún caso este servicio suponga contraprestación alguna.<br><br>
								
								Asimismo, en cumplimiento del artículo 21 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, Raquel Valero Oltra no enviará por correo electrónico comunicaciones publicitarias que no hayan sido autorizadas por los usuarios. Cuando Raquel Valero Oltra disponga del consentimiento expreso del destinatario en recibir comunicación comercial mediante correo electrónico, la palabra "publicidad" aparecerá en el inicio del mensaje.<br><br>
								
							</p>
							</div>
							
														<span>GENERALES:</span>
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
							<p>
								Para toda cuestión litigiosa o que incumba a la Página Web de www.psicologiaysexologia.es, será de aplicación la legislación española, siendo competentes para la resolución de todos los conflictos derivados o relacionados con el uso de esta página Web, los Juzgados y Tribunales del domicilio del usuario. El acceso a la página Web de www.psicologiaysexologia.es implica la aceptación de todas las condiciones anteriormente expresadas.<br><br>
							</p>
							</div>
							
						</div>
						<div class="emptySpace-xs30"></div>
					</div>

				</div>
			</div>
		</div>
		<!-- 	About our-END 	-->

		
    <?php include("includes/maps.php"); ?>
	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->


     <?php include("includes/js.php"); ?>

</body>
</html>
