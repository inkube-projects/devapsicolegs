<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">PSICOLOGÍA</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Psicoterapia infanto-juvenil</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-md-push-8 col-lg-3 col-lg-push-9">
						<div class="mobileSearch large">
							Search
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<ul class="categoryList normall">
								<li><a href="psicologia.php">Psicología Clínica</a></li>
								<li class="activeCat"><a href="#">Psicoterapia Infanto-juvenil</a></li>
								<li><a href="psicologia_educativa.php">Psicología Educativa</a></li>
							</ul>
							
														<div class="openingHours">
								<h6 class="h6 as">Cita Previa:</h6>
								<ul class="normall">
									<li><span>Raquel Valero</span> <span>666 474 813 </span> <div class="clear"></div></li>
									<li><span>Gloria Sempere</span> <span>697 660 173</span> <div class="clear"></div></li>
								</ul>
							</div>
							
							<hr>

							<div class="openingHours">
								<h6 class="h6 as">Horario</h6>
								<ul class="normall">
									<li><span>De lunes a viernes</span> <span>Mañanas y tardes</span> <div class="clear"></div></li>
									<li><span>Sábado mañana</span> <span>Con cita previa</span> <div class="clear"></div></li>
								</ul>
							</div>

						</aside>
					</div>
					<div class="col-sm-12 col-md-8 col-md-pull-4 col-lg-9 col-lg-pull-3">
						<div class="mainServicesContent">
							
							
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<div class="row">
									<div class="col-sm-7">
										<div class="imgWrapper">
											<img src="img/psicologia1.jpg" alt="">
										</div>
										<div class="emptySpace-xs20"></div>
									</div>
									<div class="col-sm-5">
										<div class="imgWrapper">
											<img src="img/psicologia2.jpg" alt="">
										</div>
									</div>
								</div>
								
								<div class="emptySpace50 emptySpace-xs30"></div>
								
								<div class="blogContent">
									<div class="simple-article normall">
										<h5>PSICOTERAPIA INFANTO-JUVENIL</h5>
										<p>
											La psicoterapia infanto-juvenil requiere una atención centrada en el proceso evolutivo en el que se encuentra el/la niño/a o adolescente, ya que se encuentran en pleno desarrollo físico, psicológico, social…ete. La psicoterapia a estas edades requiere de la utilización de diversas técnicas, tanto expresivas como cognitivas, adaptadas al estadio evolutivo del o de la infante o adolescente. Siguiendo modelos Humanistas, en general, donde nos encontraremos con diferentes enfoques, especialmente el Gestáltico, nos centramos en el momento presente, sin perder de vista todo lo acontecido, anteriormente, y en todas las circunstancias habidas o probables. Se tratará al o la infante o adolescente, como un ser individual, que necesita una atención personalizada y propia, sin perder de vista, sus familiares más cercanos, a l@s que se les solicitará que acudan a consulta, en algunas ocasiones y que colaboren y participen, si fuera necesaria ésta.
											
										
										</p>
										<img src="img/stress-img3.jpg" alt="" class="float_right">
										<p>
											Cada vez, con bases más científicas y seriamente avaladas por estudios o investigaciones podemos constatar de la importancia de los primeros años de vida, en la vida futura de ést@s y en las consecuencias que puede acarrear problemas no resueltos o expresados en la infancia o la adolescencia. Como refleja la frase ‘Cuidar a un niño evitará un adulto enfermo’, trabajamos desde esta premisa.<br><br>
											
Un desequilibrio o malestar emocional durante la infancia o la adolescencia puede tener unas consecuencias importantes a largo plazo, por lo que es de suma importancia que se preste atención a esta sintomatología. Podemos observar cambios en la conducta, en el estado de ánimo, que pueden estar asociados al proceso evolutivo en el que se encuentre o a  situaciones contextuales y no normativas como el nacimiento de un hermano, separación de los padres o alguna pérdida importante, y un largo ete., que puede afectar de modo subjetivo a cada ser.<br><br>
											
En el proceso terapéutico tanto en la infancia como la adolescencia, es necesario que se contemple todo el entorno, que se plantee desde <strong>“una visión holística”</strong>, donde los padres, profesores o resto de familiares o personas que les circundan, tendrán una importancia considerable en éste.  A veces, sólo se precisará de un asesoramiento a los padres o personas <strong>al cargo de éstos,</strong> ya que los problemas que presentan, pueden ser síntomas debidos a la etapa evolutiva o a cuestiones puntuales, que surgen del propio sistema familiar y/o educativo. En otras ocasiones, el proceso terapéutico se centrará, preferentemente, <strong>en la psicoterapia familiar,</strong> donde la intervención irá encaminada a ejercer cambios que permitan una buena convivencia y relación,  contemplar el lugar que tiene cada miembro en ésta,  situar la problemática  y reestablecer el equilibrio y el bienestar.<br><br>
											
Pero en ocasiones el problema puede requerir otro planteamiento, debiéndose crear un espacio, donde el/la niño/a se sienta seguro/a para poderse expresar libremente, a través de diferentes técnicas y estrategias. Ahí está la función del o de la psicoterapeuta como adulto externo a él/ella y a su entorno que le proporcionará confianza y seguridad que necesita para avanzar en su camino. A través del dibujo, del juego, del movimiento, de la música, la creatividad…ete., el/la niño/a o adolescente tiene la oportunidad de expresar lo que necesite o le surja espontáneamente.<br><br>

										</p>
									
									</div>
								</div>
										
							</div>
							<!-- 	Blog1-END 	-->
							
						</div>
					</div>
				</div>
			</div>
		</div>
		

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
