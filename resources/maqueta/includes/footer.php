        <!-- Footer-START -->
        <footer>
			<div class="container">
				<!-- 	Footer top info-START 	-->
				<div class="row">
					<div class="col-xs-12 col-sm-5 col-md-3">
						<!-- 	Block1-START 	-->
						<div class="footerBlock small">
							<!-- 	Logo-START 	-->
            				<a href="index.html" class="logo">
            					<img src="img/logo.png" alt="">
							</a>
        					<!-- 	Logo-END 	-->
         					<div class="simple-article">
         						<p>Hay situaciones en las que, necesitamos ser escuchados y recibir ayuda profesional.</p>
         					</div>
         					<a class="readMore" href="#">+ info</a>
         					<!-- 	Social-START 	-->
							<div class="socialWrapper light">
								<a href="https://twitter.com/intesex" target="_blank"><i class="fa fa-twitter"></i></a>
								<a href="https://www.linkedin.com/in/raquel-valero-oltra-23827657" target="_blank"><i class="fa fa-linkedin-square"></i></a>
								<a href="https://www.facebook.com/psicologiaintesex/" target="_blank"><i class="fa fa-facebook"></i></a>
								<a href="https://www.instagram.com/devapsicologiavlc/" target="_blank"><i class="fa fa-instagram"></i></a>

							</div>
							<!-- 	Social-END 	-->
							<div class="emptySpace-md30"></div>
						</div>
						<!-- 	Block1-END 	-->
					</div>
					<div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-0">
						<!-- 	Block2-END 	-->
						<div class="footerBlock normall">
							<div class="footerTitle">
								<p>Secciones</p>
							</div>
							<div class="simple-article style2">
								<ul>
									<li><a href="#">El centro</a></li>
									<li><a href="#">Blog</a></li>
									<li><a href="cookies.php">Cookies</a></li>
									<li><a href="legal.php">RGPD</a></li>
								</ul>
							</div>
							<div class="emptySpace-md30"></div>
						</div>
						<!-- 	Block2-END 	-->
					</div>
					<div class="col-xs-12 col-sm-5 col-md-3">
						<!-- 	Block3-END 	-->
						<div class="footerBlock normall">
							<div class="footerTitle">
								<p>Servicios</p>
							</div>
							<div class="simple-article style2">
								<ul>
									<li><a href="#">Psicología</a></li>
									<li><a href="#">Sexología</a></li>
									<li><a href="#">Contacto/Pedir cita</a></li>
								</ul>
							</div>
						</div>
						<!-- 	Block3-END 	-->
					</div>
					
					<div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-0">
						<!-- 	Block4-END 	-->
						<div class="footerBlock normall">
							<div class="footerTitle">
								<p>DEVA Psicología y Sexología</p>
							</div>
							<div class="locationBlock">
								<img src="img/location-icon.png" alt="">
								<div class="locationContent">
									<p>Ubicación</p>
									<span>C/ Francisco Martínez, 1-6º-18ª, 46020 - Benimaclet (Valencia)</span>
								</div>
							</div>
							<div class="footerContants">
								<i class="fa fa-phone"></i>
								<a href="#">666 474 813 - 697 660 173</a>
							</div>
							<div class="footerContants">
								<i class="fa fa-envelope-o"></i>
								<a href="mailto:hola@psicologiaysexologia.es">hola@psicologiaysexologia.es</a>
							</div>
						</div>
						<!-- 	Block4-END 	-->
					</div>
					
					<div class="col-xs-12" style="font-size: 14px; color: blueviolet;"><a href="#" target="_blank">Existe la posibilidad, dependiendo de ciertas circunstancias, de ser <strong>atendido por Skype</strong> (pago por anticipado)</a></div>
				</div>
				<!-- 	Footer top info-END 	-->
				
				<div class="emptySpace30"></div>
				
				<!-- 	Footer bottom info-START 	-->
				<div class="row">
					<div class="bottomInfo small">
						<div class="col-xs-12 col-sm-8">
						<div class="copy">
								<p>Copyright © Deva Psicología y Sexología 2019. All rights reserved.</p>
							</div>
						</div>
						<!--<div class="col-xs-12 col-sm-4">
							<div class="created">
								<a href="#">Created by: <span>Inkube</span></a>
							</div>
						</div>-->
						<div class="clear"></div>
					</div>
				</div>
				<!-- 	Footer bottom info-END 	-->
			</div>
        </footer>
        <!-- Footer-END -->