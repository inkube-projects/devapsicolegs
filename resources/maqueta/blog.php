<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">BLOG</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Blog</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-3">
						<div class="mobileSearch large">
							Buscar
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<div class="searchWrapper">
								<input class="simple-input" type="text" placeholder="Buscar">
								<button class="searchBtn" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
							<!-- 	Category-START 	-->
							<div class="recentTitle">
								<h5 class="h5 as">Categorias</h5>
							</div>
							<ul class="categoriesList normall">
								<li><a href="#">Categoria 1</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 3</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 4</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 5</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 6</a> <i class="fa fa-angle-right"></i></li>
							</ul>
							
							<!-- 	Category-END 	-->
							
							<!-- 	Recent news-START-->
							<div class="recentTitle">
								<h5 class="h5 as">Más recientes</h5>
							</div>
							
							<!-- 	News1-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor </a>
									<span>28/08/2019</span>
								</div>
							</div>
							<!-- 	News1-END 	-->
							
							<!-- 	News2-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor</a>
									<span>28/08/2019</span>
								</div>
							</div>
							<!-- 	News2-END 	-->
							
							<!-- 	News3-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor</a>
									<span>28/08/2019</span>
								</div>
							</div>
							<!-- 	News3-END 	-->
							
							<!-- 	News4-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor</a>
									<span>28/08/2019</span>
								</div>
							</div>
							<!-- 	News4-END 	-->
							
							<!-- 	Recent news-START-->
							<div class="emptySpace30 emptySpace-xs10"></div>
							<!-- 	Tags-START 	-->
							<div class="recentTitle">
								<h5 class="h5 as">TAGS/ETIQUETAS</h5>
							</div>
							<a href="blog.html#" class="tags small">Psicología</a>
							<a href="blog.html#" class="tags small">Depresion</a>
							<a href="blog.html#" class="tags small active">Stress</a>
							<a href="blog.html#" class="tags small">problemas infantiles</a>
							<a href="blog.html#" class="tags small">Anorexia</a>
							<a href="blog.html#" class="tags small">terapia de pareja</a>
							<!-- 	Tags-END 	-->
							<div class="emptySpace-xs30"></div>
						</aside>
					</div>
					<div class="col-sm-12 col-md-9">
						<div class="mainBlogContent">
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<a href="blog_detalle.php" class="imgWrapper blogThumbnail">
									<img src="img/blog-img.jpg" alt="">
									<span class="timeBlock">18/08/2019</span>
								</a>
								<div class="blogInfo">
									<p><i class="fa fa-user"></i> Raquel Valero</p>
									<p><i class="fa fa-tag"></i> Nombre de la categoría</p>
									<p><i class="fa fa-comments-o"></i> comentarios: <span>5</span></p>
								</div>
								<div class="blogContent">
									<h5 class="h5 as"><a href="blog_detalle.php">Título del post de prueba, lorem ipsut dolor texto de prueba para las imprentas</a></h5>
									<div class="simple-article normall">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book....</p>
									</div>
								</div>
								<a href="blog_detalle.php" class="button">Leer más</a>
							</div>
							<!-- 	Blog1-END 	-->
							<div class="emptySpace80 emptySpace-xs30"></div>

							<!-- 	Blog2-START 	-->
							<div class="blogWrapper">
								<a href="blog_detalle.php" class="imgWrapper blogThumbnail">
									<img src="img/blog-img2.jpg" alt="">
									<span class="timeBlock">18/08/2019</span>
								</a>
								<div class="blogInfo">
									<p><i class="fa fa-user"></i>Gloria Sempere</p>
									<p><i class="fa fa-tag"></i> Nombre de la categoría</p>
									<p><i class="fa fa-comments-o"></i> comentarios: <span>5</span></p>
								</div>
								<div class="blogContent">
									<h5 class="h5 as"><a href="blog_detalle.php">Título del post de prueba, lorem ipsut dolor texto de prueba para las imprentas</a></h5>
									<div class="simple-article normall">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book....</p>
									</div>
								</div>
								<a href="blog_detalle.php" class="button">Leer más</a>
							</div>
							<!-- 	Blog2-END 	-->
							<div class="emptySpace80 emptySpace-xs30"></div>

							<!-- 	Blog3-START 	-->
							<div class="blogWrapper">
								<a href="blog_detalle.php" class="imgWrapper blogThumbnail">
									<img src="img/blog-img3.jpg" alt="">
									<span class="timeBlock">18/08/2019</span>
								</a>
								<div class="blogInfo">
									<p><i class="fa fa-user"></i> Raquel Valero</p>
									<p><i class="fa fa-tag"></i> Nombre de la categoría</p>
									<p><i class="fa fa-comments-o"></i> comentarios: <span>5</span></p>
								</div>
								<div class="blogContent">
									<h5 class="h5 as"><a href="blog_detalle.php">Título del post de prueba, lorem ipsut dolor texto de prueba para las imprentas</a></h5>
									<div class="simple-article normall">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book....</p>
									</div>
								</div>
								<a href="blog_detalle.php" class="button">Leer más</a>
							</div>
							<!-- 	Blog3-END 	-->
							<div class="emptySpace80 emptySpace-xs30"></div>

							<!-- 	Blog4-START 	-->
							<div class="blogWrapper">
								<a href="blog_detalle.php" class="imgWrapper blogThumbnail">
									<img src="img/blog-img4.jpg" alt="">
									<span class="timeBlock">18/08/2019</span>
								</a>
								<div class="blogInfo">
									<p><i class="fa fa-user"></i> Gloria Sempere</p>
									<p><i class="fa fa-tag"></i> Nombre de la categoría</p>
									<p><i class="fa fa-comments-o"></i> comentarios: <span>5</span></p>
								</div>
								<div class="blogContent">
									<h5 class="h5 as"><a href="blog_detalle.php">Título del post de prueba, lorem ipsut dolor texto de prueba para las imprentas</a></h5>
									<div class="simple-article normall">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book....</p>
									</div>
								</div>
								<a href="blog_detalle.php" class="button">Leer más</a>
							</div>
							<!-- 	Blog4-END 	-->
							<div class="emptySpace80 emptySpace-xs30"></div>

							<!-- 	Blog6-START 	-->
							<div class="blogWrapper">
								<a href="blog_detalle.php" class="imgWrapper blogThumbnail">
									<img src="img/blog-img5.jpg" alt="">
									<span class="timeBlock">18/08/2019</span>
								</a>
								<div class="blogInfo">
									<p><i class="fa fa-user"></i> Gloria Sempere</p>
									<p><i class="fa fa-tag"></i> Nombre de la categoría</p>
									<p><i class="fa fa-comments-o"></i> comentarios: <span>5</span></p>
								</div>
								<div class="blogContent">
									<h5 class="h5 as"><a href="blog_detalle.php">Título del post de prueba, lorem ipsut dolor texto de prueba para las imprentas</a></h5>
									<div class="simple-article normall">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book....</p>
									</div>
								</div>
								<a href="blog_detalle.php" class="button">Leer más</a>
							</div>
							<!-- 	Blog6-END 	-->

							<div class="emptySpace90 emptySpace-xs30"></div>

							<!-- 	Page pagitaion-START 	-->
							<div class="paginationWrapper large">
								<a class="pagiPrev" href="blog.html#"><i class="fa fa-angle-left"></i></a>
								<div class="nubmerPagination">
									<a class="numberPagi activePagi" href="blog.html#">1</a>
									<a class="numberPagi" href="blog.html#">2</a>
								</div>
								<a class="pagiNext" href="blog.html#"><i class="fa fa-angle-right"></i></a>
							</div>
							<!-- 	Page pagitaion-END 	-->
						</div>
					</div>
				</div>
			</div>
		</div>

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
