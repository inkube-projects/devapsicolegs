<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">SEXOLOGÍA</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Educación Sexual</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-md-push-8 col-lg-3 col-lg-push-9">
						<div class="mobileSearch large">
							Search
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<ul class="categoryList normall">
								<li><a href="sexologia.php">Introducción</a></li>
								<li class="activeCat"><a href="#">Educación Sexual</a></li>
								<li><a href="consueling.php">Consueling</a></li>
								<li><a href="sexologia_clinica.php">Sexología Clínica</a></li>
							
							</ul>
							
														<div class="openingHours">
								<h6 class="h6 as">Cita Previa:</h6>
								<ul class="normall">
									<li><span>Raquel Valero</span> <span>666 474 813 </span> <div class="clear"></div></li>
									<li><span>Gloria Sempere</span> <span>697 660 173</span> <div class="clear"></div></li>
								</ul>
							</div>
							
							<hr>

							<div class="openingHours">
								<h6 class="h6 as">Horario</h6>
								<ul class="normall">
									<li><span>De lunes a viernes</span> <span>Mañanas y tardes</span> <div class="clear"></div></li>
									<li><span>Sábado mañana</span> <span>Con cita previa</span> <div class="clear"></div></li>
								</ul>
							</div>

						</aside>
					</div>
					<div class="col-sm-12 col-md-8 col-md-pull-4 col-lg-9 col-lg-pull-3">
						<div class="mainServicesContent">
							
							
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<div class="row">
									<div class="col-sm-7">
										<div class="imgWrapper">
											<img src="img/stress-img.jpg" alt="">
										</div>
										<div class="emptySpace-xs20"></div>
									</div>
									<div class="col-sm-5">
										<div class="imgWrapper">
											<img src="img/stress-img2.jpg" alt="">
										</div>
									</div>
								</div>
								
								<div class="emptySpace50 emptySpace-xs30"></div>
								
								<div class="blogContent">
									<div class="simple-article normall">
										<h5>EDUCACIÓN SEXUAL</h5>
										
										<h6 class="h6 as">Sex Education o Educación sexual:</h6>
										<p>
<strong>Objetivo: </strong>informar y formar. Enfatizará en la resolución de pequeños problemas, dudas, aclaraciones, en especial, sobre los distintos aspectos de la vivencia sexual y sus expresiones. 
<strong>Formato:</strong> puede llegar a un amplio espectro de la población, por medio de conferencias, charlas-debates, talleres, etc. Se realizará, principalmente, en grupo.
<br><br>
En el conjunto de la trilogía que, anteriormente, hemos presentado, podemos decir, en la actualidad, que la <strong>Educación Sexual </strong>representa la base y el comienzo: el punto de partida. Aquella intervención que a priori, evitaría el recurrir a las otras formas de la práctica sexológica como son: el <strong>asesoramiento</strong> o la <strong>terapia sexual. </strong>
 <br><br>
Es sabido que muchos de los problemas que, a veces, traen las personas a las consultas, son el resultado de una <strong>precaria educación sexual; </strong>por no hablar de la prevalencia en nuestra sociedad (a pesar de estar ya en siglo XXI), de una serie de <strong>prejuicios y mitos,</strong> que siguen problematizando la vivencia sexual de los sujetos. 
 <br><br>
El modelo utilizado por nuestro centro preferentemente es el de <strong>“Actitud de Cultivo”.</strong> En él será prioritario, no sólo el contenido, sino también las actitudes del sexólog@, la forma de ver y de vivir el hecho de ser sexuad@, con sus implicaciones, no como algo prohibido, ni siquiera permitido, sino como un hecho y una dimensión de la persona humana, que vale la pena suscitar, promocionar y, en definitiva, <strong>"cultivar".</strong> Y esto será extensible a todas las intervenciones de esta área.
 <br><br>
Y ahí, en el hecho de <strong>ser sexuad@, </strong>radicará toda la potencialidad del ser humano en cuanto a la realidad de ser hombre o mujer; es decir<strong> "ser sexuad@" </strong>indiferentemente de su género, orientación de su deseo sexual u otra peculiaridad a tener en cuenta, con todas las implicaciones, que consigo, esto conlleva. 
 <br><br>
Desde esta <strong>"actitud de cultivo",</strong> la sexualidad podrá ser reconocida desde una perspectiva muy diferente, lejana a la que promovían las actitudes prohibitivas o permisivas. La <strong>"sexualidad" </strong>en vías de cultivo, podrá, en principio, llevarnos a una calidad de vida más humanizada, quizá más satisfactoria, en consecuencia, a ser vivida o vivenciada según los deseos o necesidades de cada ser.
<br><br>
									   </p>
											
											<hr>
											
									</div>
								</div>

										
							</div>
							<!-- 	Blog1-END 	-->
							
						</div>
					</div>
				</div>
			</div>
		</div>
		

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
