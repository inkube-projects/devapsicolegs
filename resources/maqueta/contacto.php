<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">CONTACTO</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Contacto</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->

			<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="contentTitle normall">
							<h2 class="h2 as">Contacta con Deva Psicología</h2>
							<p>Puedes hacernos una consulta o solicitar una cita previa, rellenado el siguiente formulario o contactando con nosotras por teléfono o email.</p>
							<hr>
							
						</div>
						<div class="emptySpace50 emptySpace-xs30"></div>
					</div>
					<div class="col-sm-6 col-md-8">
						
													<h4>1º Cita</h4>
							<p>Primera cita concertada gratuita en formato de entrevista, duración de 15 min, aproximadamente.</p><br><br>
						
						<form action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
							<input class="simple-input" id="name" name="name" type="text" value="" placeholder="Nombre y Apellidos" />
							<div class="emptySpace20"></div>
							<input class="simple-input" id="email" name="email" type="email" value="" placeholder="Email" />
							<div class="emptySpace20"></div>
							<input class="simple-input" id="subject" name="subject" type="text" value="" placeholder="Teléfono" />
							<div class="emptySpace20"></div>
																<div class="sumoWrapper">
										<select name="form" class="SelectBox">
											<option selected disabled>Motivo</option>
											<option>Ampliar información</option>
											<option>Pedir cita</option>
											<option>Otro</option>
										</select>
									</div>
							<textarea class="simple-input" id="message" name="message" placeholder="Describe el motivo de tu consulta"></textarea>
							<div class="emptySpace50 emptySpace-xs30"></div>
							<button type="submit" class="button">Enviar</button>
						</form>
						<div class="emptySpace-xs30"></div>
						<!--<div id="success">
                            <p>Your text message sent successfully!</p>
                        </div>
                        <div id="error">
                            <p>Sorry! Message not sent. Something went wrong!!</p>
                        </div>-->
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="contactDetails normall">
							<!-- 	Contacts1-START 	-->
							<div class="contactAddres">
								<div class="imgWrapper">
									<img src="img/location-icon-white.png" alt="">
								</div>
								<div class="simple-article light">
									<p>DEVA PSICOLOGÍA:</p>
									<p>C/ Francisco Martínez, 1-6º-18ª, 46020 - Benimaclet (Valencia)</p>
									<p><strong>Próximamente, dispondremos de un segundo centro.</strong></p>
								</div>
							</div>
							<!-- 	Contacts1-END 	-->
							
							<!-- 	Contacts2-START 	-->
							<div class="contactAddres">
								<div class="imgWrapper">
									<i class="fa fa-envelope-o"></i>
								</div>
								<a href="mailto:hola@psicologiaysexologia.es">hola@psicologiaysexologia.es</a>
							</div>
							<!-- 	Contacts2-END 	-->
							
							<!-- 	Contacts3-START 	-->
							<div class="contactAddres large">
								<div class="imgWrapper">
									<i class="fa fa-phone"></i>
								</div>
								<a href="tel:666474813">Raquel Valero<br>666 474 813 </a>
							</div>
							<!-- 	Contacts3-END 	-->
							
														<!-- 	Contacts3-START 	-->
							<div class="contactAddres large">
								<div class="imgWrapper">
									<i class="fa fa-phone"></i>
								</div>
								<a href="tel:697660173">Gloria Sempere<br>697 660 173 </a>
							</div>
							<!-- 	Contacts3-END 	-->
							
							<div class="contactAddres large">
								<div class="imgWrapper">
									<i class="fa fa-clock-o"></i>
								</div>
							<a href=" "><span>Horario:</span><br></a>
							<span style="font-size: 12px; line-height: 16px; color: #fff;">De lunes a viernes (mañanas y tardes).<br>
							Sábado mañana, con cita previa.<br>
								Algunas actividades pueden realizarse durante todo un sábado.</span>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
       	<div class="emptySpace20"></div>
    
    <?php include("includes/maps.php"); ?>
	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
