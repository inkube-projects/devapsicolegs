<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">SEXOLOGÍA</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Introducción</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-md-push-8 col-lg-3 col-lg-push-9">
						<div class="mobileSearch large">
							Search
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<ul class="categoryList normall">
								<li class="activeCat"><a href="#">Introducción</a></li>
								<li><a href="educacion_sexual.php">Educación Sexual</a></li>
								<li><a href="consueling.php">Consueling</a></li>
								<li><a href="sexologia_clinica.php">Sexología Clínica</a></li>
							
							</ul>
							
														<div class="openingHours">
								<h6 class="h6 as">Cita Previa:</h6>
								<ul class="normall">
									<li><span>Raquel Valero</span> <span>666 474 813 </span> <div class="clear"></div></li>
									<li><span>Gloria Sempere</span> <span>697 660 173</span> <div class="clear"></div></li>
								</ul>
							</div>
							
							<hr>

							<div class="openingHours">
								<h6 class="h6 as">Horario</h6>
								<ul class="normall">
									<li><span>De lunes a viernes</span> <span>Mañanas y tardes</span> <div class="clear"></div></li>
									<li><span>Sábado mañana</span> <span>Con cita previa</span> <div class="clear"></div></li>
								</ul>
							</div>

						</aside>
					</div>
					<div class="col-sm-12 col-md-8 col-md-pull-4 col-lg-9 col-lg-pull-3">
						<div class="mainServicesContent">
							
							
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<div class="row">
									<div class="col-sm-7">
										<div class="imgWrapper">
											<img src="img/stress-img.jpg" alt="">
										</div>
										<div class="emptySpace-xs20"></div>
									</div>
									<div class="col-sm-5">
										<div class="imgWrapper">
											<img src="img/stress-img2.jpg" alt="">
										</div>
									</div>
								</div>
								
								<div class="emptySpace50 emptySpace-xs30"></div>
								
								<div class="blogContent">
									<div class="simple-article normall">
										<h5>INTRODUCCIÓN</h5>
										<p>La Sexología tiene como objeto de estudio: la <strong>"Dimensión Sexual Humana"</strong> o el <strong>"Hecho Sexual Humano" </strong>-el hecho por el cual el sujeto humano se hace, se vive, se expresa, se relaciona.......etc. como "sujeto sexuado". Por lo tanto, su intervención se dará en esos ámbitos.<br><br>
											
Actualmente, en la Práctica Sexológica se considera en la base a la <strong>educación,</strong> a continuación el <strong>asesoramiento</strong>, y finalmente la <strong>terapia,</strong> como último recurso de intervención donde las anteriores no han podido solucionar o atajar, aquello que se presentaba como problema. En los apartados correspondientes a cada concepto, daremos una pequeña información sobre cada uno.<br><br>
Ahora, entraremos en un tema de gran relieve en esta ciencia. La terminología.<br><br>
Y cómo no, vamos a empezar por un término de mucho uso, podía decirse que hasta de abuso, por la población en general. Vamos a hablar del concepto: <strong>"sexo".</strong><br><br>
											
<strong>¿De qué estamos hablando cuando hablamos de "sexo"?</strong> Generalmente, no sabemos de qué estamos hablando. <br><br>
											
Vulgarmente empleamos el término <strong>"sexo" </strong>para decir muchas cosas, pero, en el fondo, como antes se ha mencionado, no sabemos a qué nos estamos refiriendo, ni nos aclaramos sobre qué queremos decir. </p>
				
							
										<h6>Por ejemplo:</h6>
										<ul>
											<li>"Sexo seguro": entonces haremos relación a tener relaciones sexuales, en este caso "genitales", con precaución"</li><br>
											<li>"Mi sexo": estaremos hablando de los genitales.</li><br>
											<li>"Sexo": mujer". Y así un largo etc.</li><br>
										<ul>
											<p>Vamos a aclarar esto, un poco.</p>
																		<p>
										
										<strong>La Sexología </strong>es la ciencia que tiene como objeto de estudio al: <strong>"hecho sexual humano",</strong> hecho por el cual el sujeto humano se hace (sexuación), se vive (sexualidad) y se expresa (erótica) como "sujeto sexuado".<br><br>
										
<strong>La sexuación</strong> es el proceso por el cual el ser humano, pasando por una serie de relevos en su desarrollo, tanto a nivel cromosómico, genital, neurológico, somático, psicológico, sociológico .y un largo etc., se convierte en "ser sexuadx" a través de toda su existencia.<br><br>
										
El<strong> "sexo"</strong> no es una función, sino una dimensión que impregna la existencia de todo individuo. El "sexo" será el resultado de esa serie de elementos diferenciales, que nos van configurando como "seres sexuad@s", en una amplia gama de posibilidades, en la base de una intersexualidad constituyente, en todos sus modos, matices y peculiaridades.<br><br>
									
La <strong>"sexualidad"</strong> sería la vivencia de ese "ser sexuad@", el resultado vivencial del proceso de sexuación, donde se perfilarán todos aquellos aspectos, que vendrán desgajados del anterior proceso, como son los motivacionales, cognitivos, emocionales..., que le harán tomar conciencia al sujeto de su "ser sexuad@".<br><br>
 
La<strong> "erótica"</strong> sería "la puesta en escena" a cualquier nivel de esa vivencia sexual, perteneciente a ese "ser sexuad@". Es decir la expresión, el gesto, la conducta. Y todo esto, tanto a nivel interno como externo. Aquí entraría el deseo, la seducción, las fantasías, las caricias, la genitalidad, etc.<br><br>
										
									</p>
											
											<hr>
											
									</div>
								</div>

										
							</div>
							<!-- 	Blog1-END 	-->
							
						</div>
					</div>
				</div>
			</div>
		</div>
		

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
