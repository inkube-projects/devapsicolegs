<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">PSICOLOGÍA</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Psicología Clínica</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-md-push-8 col-lg-3 col-lg-push-9">
						<div class="mobileSearch large">
							Search
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<ul class="categoryList normall">
								<li class="activeCat"><a href="#">Psicología Clínica</a></li>
								<li><a href="sicoterapia-infanto-juvenil.php">Psicoterapia Infanto-juvenil</a></li>
								<li><a href="psicologia_educativa.php">Psicología Educativa</a></li>
							</ul>
							
														<div class="openingHours">
								<h6 class="h6 as">Cita Previa:</h6>
								<ul class="normall">
									<li><span>Raquel Valero</span> <span>666 474 813 </span> <div class="clear"></div></li>
									<li><span>Gloria Sempere</span> <span>697 660 173</span> <div class="clear"></div></li>
								</ul>
							</div>
							
							<hr>

							<div class="openingHours">
								<h6 class="h6 as">Horario</h6>
								<ul class="normall">
									<li><span>De lunes a viernes</span> <span>Mañanas y tardes</span> <div class="clear"></div></li>
									<li><span>Sábado mañana</span> <span>Con cita previa</span> <div class="clear"></div></li>
								</ul>
							</div>

						</aside>
					</div>
					<div class="col-sm-12 col-md-8 col-md-pull-4 col-lg-9 col-lg-pull-3">
						<div class="mainServicesContent">
							
							
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<div class="row">
									<div class="col-sm-7">
										<div class="imgWrapper">
											<img src="img/psicologia1.jpg" alt="">
										</div>
										<div class="emptySpace-xs20"></div>
									</div>
									<div class="col-sm-5">
										<div class="imgWrapper">
											<img src="img/psicologia2.jpg" alt="">
										</div>
									</div>
								</div>
								
								<div class="emptySpace50 emptySpace-xs30"></div>
								
								<div class="blogContent">
									<div class="simple-article normall">
										<h5>PSICOLOGÍA CLÍNICA</h5>
										<p>Contemplamos la <strong>Psicoterapia </strong>en la línea de la Psicología Humanista, preferentemente "Gestalt", en conjunción con un enfoque Psico-dinámico, dependiendo del profesional que intervenga, y lo que el caso en sí, requiera..</p>
										<img src="img/stress-img3.jpg" alt="" class="float_right">
							
										<h6>Aquí encontraremos tres áreas de intervención:</h6>
										<ul>
											<li>Psicoterapia Individual</li>
											<li>Psicoterapia de Familia</li>
											<li>Psicoterapia Grupo</li>
										<ul>
										<hr>
									</div>
								</div>
								<div class="contactsAdviceWrapper normall">
									<h4 class="h4 as">INTRODUCCIÓN</h4>
									<p>Recogemos de la <strong>Psicología Humanista</strong> su interés por colocar al ser humano como punto central y de mayor relieve. Es una corriente dentro de la Psicología Clínica que nace como parte de un movimiento cultural más general, surgido en Estados Unidos en la década de los sesenta del siglo XX, y que involucra planteamientos en otros ámbitos como la política, las artes, etc. </p>
									<br>
									<p>La Psicología Humanista, en especial la <strong>Terapia Gestáltica</strong>, es una escuela que va más allá de "la palabra", poniendo de relieve "la experiencia no verbal" y "los estados de conciencia", como medio para que la persona se realice en su pleno potencial humano.</p>
									<br>
									
									<p>Pretende la consideración global de la persona y la acentuación en sus aspectos existenciales (la libertad, el conocimiento, la responsabilidad, la historicidad, etc.).</p>
									<br>
									
									<p>El <strong>"darse cuenta"</strong> (Awareness) es el concepto clave sobre el que se asienta la Teoría Gestáltica. En pocas palabras, "darse cuenta": es entrar en contacto, de una forma natural y espontánea, con el "aquí y ahora", con lo que uno es, siente y percibe.
									</p>
									
									<p>El <strong>enfoque Psico-dinámico </strong>pone el acento en las fuerzas inconscientes. Plantea una terapia de mayor profundidad, intentando llegar a la raíz del síntoma. Parte del supuesto, que en lo más profundo de nuestra psiquis se anidan impulsos, deseos, temores y recuerdos traumáticos, de los que no somos conscientes.
									</p><br>
									
									<p>Un método que investiga los <strong>aspectos inconscientes</strong> de la vida psíquica humana, a través de sus manifestaciones en la libre asociación de ideas, en los sueños y fantasías, y en los actos erróneos e involuntarios.
									</p><br>
									
									<p>Ambas teorías utilizan la idea de <strong>"insight"</strong> como comprensión súbita o darse cuenta, aunque le den significados diferentes: el insight es para la Gestalt la configuración mental que organiza el campo perceptivo en el aquí y ahora, y para el enfoque Psicodinámico, la toma de conciencia de los contenidos (deseos, temores) inconscientes.<br><br>
										(Psicoanálisis por enfoque Psicodinámico)
									</p><br>
									
									
									<h6 class="h6 as">Psicoterapia individual adultos</h6>
									
									<p>
										Las personas que acuden a consulta presentan distinta sintomatología. Ésta puede ir aislada o acompañada de otros síntomas o problemas que le impiden desarrollar su vida adecuadamente, en algún nivel de ésta, o en todos.
<br><br>
Los más habituales son: <strong>stress, ansiedad, depresión, fobias o problemas obsesivos, falta de motivación o sentido de la vida, fracaso laboral o en otros ámbitos, carencia de habilidades sociales o de comunicación….etc. </strong>En muchos casos van acompañados de problemas físicos o éstos se presentan solos, encubriendo un problema psicológico que, de alguna forma, se encuentra bloqueado detrás de ellos, impidiendo así el acceso a éste y su tratamiento o solución.
<br><br>
En <strong>Psicoterapia individual </strong>tendremos en cuenta las aportaciones de dos escuelas y nos nutriremos de ellas, dando pie a nuestra intervención que como anteriormente ya hemos mencionado, estará en función de la problemática a tratar, y del profesional que intervenga. 
<br><br>
Por un lado está la<strong> “gestalt”</strong>, teoría holística que contempla al ser humano como un todo, en contraposición a la eterna dualidad: mente-cuerpo. 
<br><br>
Uno de los puntos relevantes para esta escuela es la experiencia del: <strong>“aquí y ahora”</strong>. Éste se inicia siempre con la sensación. Cuando la persona inhibe sus sensaciones, ésta no pasa al segundo estadio del ciclo, no llega a la toma de conciencia.
<br><br>
La segunda fase del ciclo, la <strong>“toma de conciencia”</strong>, representa uno de los elementos esenciales de la Terapia Gestalt. Por medio del <strong>“darse cuenta” </strong>(awareness), el/la terapeuta logra que el/la paciente tome conciencia de sí mismo, de su cuerpo, de sus emociones, del medio en el que se encuentra, aprenda a tomar conciencia de sus propias necesidades, desarrolle habilidades que le puedan satisfacer sin dañarse a sí mismx, ni a otros, establezca contacto con su entorno…….y un largo etc. 
<br><br>
Desde corrientes más <strong>psico-dinámicas</strong> planteamos una intervención más profunda, contemplando a la persona que viene a terapia como un ser que está sufriendo, que no sabe por qué le suceden las cosas que le suceden, que ha intentado todo tipo de explicaciones y de cambios, y sin embargo, hay "algo" que se presenta como ingobernable a toda voluntad haciéndole tropezar una y otra vez, aparentemente, con el mismo obstáculo <strong>-la Repetición-</strong>. 
<br><br>
Para poder comenzar el proceso psicoterapéutico es importante ser consciente de esta situación, y estar dispuesta a <strong>comprometerse</strong> en éste. 
<br><br>
Y finalmente, volver a hacer mención al <strong>“insight”</strong>, como fenómeno de suma importancia y clave en el proceso terapéutico, en ambas teorías. 
</p><br><br>
									<h6 class="h6 as">Psicoterapia de grupo</h6>
									
									<p>La Psicoterapia de Grupo puede resultar de gran ayuda en el proceso terapéutico, entendiendo que en éste es primordial la asistencia a Psicoterapia Individual, a priori o paralelamente. Pero, no hay duda, que si a ésta se le añaden sesiones de terapia grupal, la mayoría de las veces, podríamos decir que no sólo son beneficiosas, sino que aceleran dicho proceso. 
<br><br>
Este modelo de intervención tiene<strong> grandes ventajas</strong>, entre ellas la <strong>observación del/la paciente</strong> en sociedad, aunque sea en un micro-espacio, además de posibilitar a la persona un medio donde pueda experimentar y ensayar situaciones que le angustian o le crean conflicto. 
<br><br>
Un grupo da siempre origen a una <strong>red implícita de identificaciones</strong>, que va creando la Gestalt del grupo, la mentalidad grupal. El resultado de esta mentalidad grupal, es que cada uno de los miembros del grupo siente cómo tiene a su disposición un canal afectivo y simbólico que le permite la expresión de lo más profundo y primitivo de sí mismx. 
<br><br>
Y esa es la primera y gran razón por la que un grupo produce <strong>cambios importantes,</strong> en palabras de otros profesionales: "sana". Porque permite <strong>elaborar</strong> (es decir, experimentar de forma repetida y cada vez más adaptada), las emociones, las ansiedades y los conflictos originarios de la vida humana. 
<br><br>
En<strong> terapia individual </strong>el/la terapeuta, generalmente, se ve maniatado cuando intenta proporcionar a sus pacientes datos que les supongan un aprendizaje, ya que éstx acompaña, señala, abre perspectivas, subraya lo que sobresale y un largo etc., pero su función, prioritariamente, es intentar que sea la persona la que elija su camino, tome su decisión o reconozca sus propias claves o los indicios más pertinentes para salir del paso, y con ello evolucionar. 
<br><br>
Los miembros de un grupo, sin embargo, pueden permitirse "explicar" en qué consiste eso de estar mal, cuáles son los pasos que han ayudado a cada unx, y lo que conviene hacer en este caso particular, desde un plano de igualdad altamente<strong> terapéutico y no comprometedor</strong> para el proceso. 
<br><br>
Junto al intercambio de datos, propio de toda interacción, y las devoluciones (feedback) de cada compañero o compañera, se da en el grupo un aprendizaje de suma importancia que es el "aprendizaje de imitación". Los/las participantes cuentan con una gran variedad de conductas que pueden imitar y "ensayar". De ellas, unas serán abandonadas más tarde, otras serán modificadas. Todas ellas pertenecerán, como ensayos válidos, <strong>al proceso de crecimiento.</strong>
<br><br>
Por lo tanto, la <strong>Terapia de Grupo</strong>, en sí, facilita el proceso de cambio, pues cada individuo no sólo podrá ensayar formas de manejar los problemas o los conflictos que le angustian, sino también observar cómo sus compañerxs abordan problemáticas similares, de maneras diferentes; además de encontrar nuevos recursos y de enfrentarse a ellos.<br><br>
									</p><br><br>
										
									
									<h6 class="h6 as">Psicoterapia familiar</h6>
<p>La<strong> familia</strong> es un micro-núcleo social, que constituye un elemento esencial en nuestra cultura, pasando a formar parte de un núcleo más importante que es "la sociedad". Además, generalmente, será el reflejo de ésta y se configurará de acuerdo a las reglas vigentes, según el lugar donde esté asentada.
<br><br>
La familia suele encontrarse en un <strong>proceso continuo</strong>, sobretodo en las últimas décadas del siglo XX, y en especial en el actual. Va evolucionando gracias a los movimientos de sus miembros, pero también encontrándose con ciertas dificultades o problemas que detienen su proceso normal, generando sufrimiento en alguna o en todas las personas que la conforman. Esto hará que haya necesidad de un continuo reciclaje y readaptación, y a veces, tener que solicitar <strong>ayuda profesional.</strong>
<br><br>
Este núcleo <strong>familiar</strong> formado a partir del encuentro de una<strong> pareja</strong> (la mayoría de veces, aún) y su asentamiento, adopta en la actualidad diversos formatos. Nos podemos encontrar con configuraciones familiares diversas (separadas, monoparentales, reconstituidas, adoptivas, etc.) que enriquecen a la sociedad, pero que se tropiezan no sólo con los problemas clásicos, sino también con otros, que generalmente, no tienen referentes a que acogerse.
<br><br>
En todo caso, en las <strong>familias</strong>, sea cual sea su configuración, aparecen momentos en los que hay que definir y negociar las relaciones, tanto dentro de la familia como fuera de ella. Para ello, es necesario que la comunicación entre los miembros del <strong>núcleo familiar </strong>sea adecuada y fluida, y que sean capaces de adaptarse a las distintas situaciones externas y momentos evolutivos, por los que irán pasando. 
<br><br>
La <strong>Terapia de familia </strong>ofrece un nuevo contexto en el que pueden comunicarse, expresar asuntos pendientes o no expresados, negociar la forma de relacionarse, como de convivir, etc. 
<br><br>
Se abordarán <strong>problemáticas relacionales </strong>entre padres e hijo/as, entre hermano/as, como también con cualquier otro familiar o persona que conviva con ello/as. En ocasiones se trabaja sobre conflictos que acaban de aparecer y han supuesto una crisis importante en la familia, y en otras, será necesario revisar y sacar a la luz conflictos cronificados, que llevan años impidiendo una evolución deseable en esa familia.
</p>
									
									<div class="emptySpace70 emptySpace-xs30"></div>
				
								</div>
										
							</div>
							<!-- 	Blog1-END 	-->
							
						</div>
					</div>
				</div>
			</div>
		</div>
		

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
