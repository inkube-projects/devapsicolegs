<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->

     	<div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">EL CENTRO</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Historia</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		<!-- 	About our-START 	-->
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-9 col-md-offset-1 center-block">
						<div class="aboutOur">
							<h2 class="h2 as">Historia del centro</h2>
							<span>DEVA <span>Psicología y Sexología</span></span>
					
							<div class="emptySpace10"></div>
							<div class="simple-article normall">
								<p>En los primeros años (1982) recibía el nombre de “Gabinete de Crecimiento Personal”. Sus actividades iban en la línea de la Psicología Clínica y Educativa.  
A partir de 1984 se van introduciendo otras actividades, que priorizan un cambio de nombre. La Sexología empieza a tomar cuerpo aquí. Así nace INTESEX. (Investigación,  Terapia Psicológica y Sexología). Durante todos esos años el Centro está enclavado en el barrio de Ayora.
<br><br>
En el 2006 se traslada a Benimaclet, donde nos encontramos desde entonces en la calle Francisco Martínez, 1-6º-18ª, pasando a ser <strong>Deva: Centro de Psicología y Sexología (Intesex)</strong>, y estamos pendientes de abrir otro local, en breve.</p>
							</div>
						</div>
						<div class="emptySpace-xs30"></div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-xs-12">
								<div class="imgWrapper">
									<img src="img/sofa.jpg" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	About our-END 	-->
		

		
		<!-- 	Our team-START 	-->
		<div  id="Equipo" class="contentPaddingB">
			<div class="container">
				<div class="row clearFix">
					
															<!-- 	persone1-START 	-->
					<div class="col-xs-12">
						<div class="titleShortocode">
							<h3 class="h3 as">NUESTRO EQUIPO</h3>
							<div class="emptySpace30"></div>
							<div class="simple-article normall">
								<p>Apostamos por un servicio holístico, donde las personas sean atendidas según sus necesidades, por lo que somos un equipo multidisciplinar en continuo reciclaje formativo para poder ofrecer el mejor servicio.</p><br><br>

							</div>
						</div>
						<div class="emptySpace-sm30"></div>
					</div>
					<!-- 	persone1-END 	-->
					
					
					<!-- 	persone2-START 	-->
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="tumbWrapper persone">
							<div class="imgWrapper">
								<img src="img/raquelCV.jpg" alt="">
							</div>
							<div class="blockContent">
								<span>Raquel Valero Oltra</span>
								<p>Psicología Clínica y Sexología</p>
								<p>Directora y fundadora del Centro</p>
								<p><a href="#" class="button btn button2" data-toggle="modal" data-target="#RaquelModal">+info</a></p>
							</div>
						</div>
						<div class="emptySpace-sm30"></div>
					</div>
					<!-- 	persone2-END 	-->
					
					<!-- 	persone3-START 	-->
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="tumbWrapper persone">
							<div class="imgWrapper">
								<img src="img/gloriaCV.jpg" alt="">
							</div>
							<div class="blockContent">
								<span>Gloria Sempere Figuérez</span>
								<p>Psicóloga con Habilitación Sanitaria</p>
								<p><a href="#" class="button btn button2" data-toggle="modal" data-target="#GloriaModal">+info</a></p>
							</div>
						</div>
						<div class="emptySpace-xs30"></div>
					</div>
					<!-- 	persone3-END 	-->
					
					
					<!-- 	persone3-START 	-->
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="tumbWrapper persone">
							<div class="imgWrapper">
								<img src="img/juliaCV.jpg" alt="">
							</div>
							<div class="blockContent">
								<span>Julia Tortajada Sanfeliu</span>
								<p>Doble Grado en Psicología y Logopedia</p>
								<p><a href="#" class="button btn button2" data-toggle="modal" data-target="#JuliaModal">+info</a></p>
							</div>
						</div>
						<div class="emptySpace-xs30"></div>
					</div>
					<!-- 	persone3-END 	-->
					
										<!-- 	persone3-START 	-->
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="tumbWrapper persone">
							<div class="imgWrapper">
								<img src="img/AntonioCV.jpg" alt="">
							</div>
							<div class="blockContent">
								<span>Antonio Nortes Pastor</span>
								<p>Grado en Psicología y Máster en Sexología</p>
								<p><a href="#" class="button btn button2" data-toggle="modal" data-target="#AntonioModal">+info</a></p>
							</div>
						</div>
						<div class="emptySpace-xs30"></div>
					</div>
					<!-- 	persone3-END 	-->
					
										<!-- 	persone1-START 	-->
					<div class="col-xs-12">
						<hr>
						<div class="titleShortocode" style="margin-top: 40px;">

							<div class="simple-article normall">
								<strong>Colaboradores: </strong><br>
- Virginia Madrid del Toro. Licenciada en Derecho. Especialista en Mediación.<br>

								</p>
							</div>
						</div>
						<div class="emptySpace-sm30"></div>
					</div>
					<!-- 	persone1-END 	-->
					
					

				</div>
			</div>
		</div>
		<!-- 	Our team-END 	-->
		

							<div class="col-xs-12">
		<hr>
		</div>
		
				<!-- 	Abot us-START 	-->
		<div  id="Servicios" class="contentPaddingB ccs-padding-Bottom60">
			<div class="container">
				<div class="row">
					
					<div class="col-xs-12 col-sm-6   ccs-padding-Bottom30 ccs-padding-top30">
						<h3>¿QUÉ SERVICIOS OFRECEMOS?</h3>
						<div class="simple-article normall">
<p>Disponemos de 2 áreas de intervención principales Psicología y Sexología que a su vez se dividen en otras áreas.
	Utilizamos diferentes métodos y técnicas según el caso de cada persona.</p>
						</div>
					</div>
					
				</div>
				
				<div class="row ccs-padding-top30">
					<div class="col-sm-6">
						<div class="titleShortocode">
							<h3 class="h3 as ">1 Psicología</h3>
						</div>
							<div class="emptySpace40 emptySpace-xs30"></div>
						<!-- 	Accordeon-START 	-->
						<div class="accordeon normall">
							
							<div class="accordeon-title active">
								<div class="accrodeonButton"><span></span><span></span></div>
							Psicoterapia de adultos
							</div>
							<div class="accordeon-toggle" style="display: block;">
								<div class="simple-article">
									<p>Contemplamos la Psicoterapia en la línea de la Psicología Humanista, preferentemente "Gestalt", en conjunción con un enfoque Psico-dinámico, dependiendo del profesional que intervenga, y lo que el caso en sí, requiera.<br><br>
										Aquí encontraremos tres áreas de intervención:
										</p><ul>
									<li>Psicoterapia Individual: Las personas que acuden a consulta presentan distinta sintomatología. Ésta puede ir aislada o acompañada de otros síntomas o problemas que le impiden desarrollar su vida adecuadamente, en algún nivel de ésta, o en todos.</li><br><br>
									<li>Psicoterapia de Grupo: Puede resultar de gran ayuda en el proceso terapéutico, aunque es importante la Psicoterapia Individual, a priori o paralelamente.</li><br><br>
									<li>Psicoterapia de Familia: Ofrece un nuevo contexto en el que pueden comunicarse, expresar asuntos pendientes o no expresados, negociar la forma de relacionarse, como de convivir, etc. </li><br><br>
										 </ul>
								
									<a href="#" class="button btn button2">+info</a>
									<p></p>
								</div>
							</div>
						
							<div class="emptySpace20"></div>
						
							<div class="accordeon-title">
								<div class="accrodeonButton"><span></span><span></span></div>
								Psicoterapia Infanto-juvenil
							</div>
							<div class="accordeon-toggle">
								<div class="simple-article">
									<p>Se tratará al o la infante o adolescente, como un ser individual, que necesita una atención personalizada y propia, sin perder de vista, sus familiares más cercanos, a l@s que se les solicitará que acudan a consulta, en algunas ocasiones y que colaboren y participen, si fuera necesaria ésta.<br>
										<a href="#" class="button btn button2">+info</a>
									</p>
								</div>
							</div>
						
							<div class="emptySpace20"></div>
						
							<div class="accordeon-title">
								<div class="accrodeonButton"><span></span><span></span></div>
								Psicología Educativa y Logopedia
							</div>
							<div class="accordeon-toggle">
								<div class="simple-article">
									<p>
									Aquí podemos ver las diferentes áreas de intervención: 
										</p><ul>
									<li>Dificultades de aprendizaje: Dislexia, disgrafía y disortografía.</li>
									<li>Déficit en comprensión, producción y uso del lenguaje.</li>
									<li> Déficit de Atención e Hiperactividad.</li>
									<li>Bajo rendimiento o fracaso escolar.</li>
									<li>Técnicas de estudio.</li>
									<li>Orientación y apoyo a las familias.</li>

										 </ul>
										<a href="#" class="button btn button2">+info</a>
									<p></p>
								</div>
							</div>
						
						</div>
						<!-- 	Accordeon-END 	-->
					</div>

					<div class="col-sm-6">
						<div class="titleShortocode">
							<h3 class="h3 as ">2 Sexología</h3>
						</div>
							<div class="emptySpace40 emptySpace-xs30"></div>
						<!-- 	Accordeon-START 	-->
						<div class="accordeon normall">
							
						<div class="accordeon-title active">
								<div class="accrodeonButton"><span></span><span></span></div>
								Sexología Clínica
							</div>
							<div class="accordeon-toggle " style="display: block;">
								<div class="simple-article">
									<p>Terapia Sexual: Suelen ser problemas o dificultades comunes, y no graves, pero pueden presentarse de mayor gravedad, de esto dependerá las características de la intervención. Puede ser individual o en pareja, aunque es conveniente acudir en pareja, si ésta existe. Eyaculación precoz. Problemas de erección. Falta de deseo. Vaginismo. Conflictos de orientación sexual… 


									</p>
									<p>
									 -Terapia de pareja: Ésta estará focalizada en la pareja, principalmente, y la intervención se efectuará sobre estos sujetos sexuados y la relación que mantienen. Celos. Problemas de relación y en la vivencia sexual. Intervención y acompañamiento en el proceso de separación 
									</p>
									<p>
																		<a href="#" class="button btn button2">+info</a>
									</p>
								</div>
							</div>
							
														
							<div class="emptySpace20"></div>
							
							<div class="accordeon-title">
								<div class="accrodeonButton"><span></span><span></span></div>
								Asesoramiento Sexológico
							</div>
							<div class="accordeon-toggle">
								<div class="simple-article">
									<p>Información, Asesoramiento u Orientación Sexual y resolver problemas de poca gravedad, tanto a nivel individual como en pequeños  grupos.<br>
									<a href="#" class="button btn button2">+info</a></p>
								</div>
							</div>
							
							<div class="emptySpace20"></div>
							
							<div class="accordeon-title">
								<div class="accrodeonButton"><span></span><span></span></div>
								Educación Sexual
							</div>
							<div class="accordeon-toggle">
								<div class="simple-article">
									<p>
										Informar y formar, es el punto de partida. Enfatizará en la resolución de pequeños problemas, dudas, aclaraciones, en especial, sobre los distintos aspectos de la vivencia sexual y sus expresiones, en grupo. <br>
										<a href="#" class="button btn button2">+info</a>
									</p>
								</div>
							</div>



						</div>
						<!-- 	Accordeon-END 	-->
					</div>
				</div>
			</div>
		</div>
		<!-- 	Abot us-END 	-->
		
		<!-- 	Request-START 	-->
		<div class="contentPadding grey colorBlack">
			<div class="contactBg bgShadow style2" style="background-image: url('img/bg-layer.jpg')"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="contactContent normall">
							<span>Cita Previa</span><br>
							<span>Raquel Valero :</span> <p>: 666 474 813</p>
							<div class="emptySpace5"></div>
							<span>Gloria Sempere :</span> <p>697 660 173</p>
							<div class="emptySpace5"></div>
							<span>1º Cita</span>
							<p>Primera cita concertada gratuita en formato de entrevista, duración de 15 min, aproximadamente.</p>
						</div>

						<div class="emptySpace30"></div>
						<div class="contactContent normall">
							<span>DEVA: PSICOLOGÍA Y SEXOLOGÍA</span>
							<p>C/ Francisco Martínez, 1-6º-18ª, 46020 - Benimaclet (Valencia).</p>
							<p>Próxima apertura de una segunda dirección.</p>
						</div>
						<div class="emptySpace30"></div>
						<div class="contactContent normall">
							<span>Horario:</span>
							<p>De lunes a viernes (mañanas y tardes).<br>
							Sábado mañana, con cita previa.<br>
							Algunas actividades pueden realizarse durante todo un sábado.</p>
						</div>
	
						<div class="emptySpace-xs30"></div>
					</div>
					<div class="col-sm-6 col-sm-offset-2">
						<form class="requestForm">
							<div class="contentTitle">
								<h3 class="h3 as">CONTACTA CON Deva: Psicología y Sexología. <span>Cita previa</span></h3><br>
								<p>Puedes hacernos una consulta o solicitar una cita previa, rellenado el siguiente formulario o contactando con nosotras por teléfono o email.</p><br>
							</div>
							<div class="emptySpace15"></div>
							<div class="row">
								<div class="col-xs-12 col-md-6">
									<input class="simple-input" type="text" value="" placeholder="Nombre Apellidos" />
									<div class="emptySpace15"></div>
								</div>
								<div class="col-xs-12 col-md-6">
									<input class="simple-input" type="email" value="" placeholder="Email" />
									<div class="emptySpace15"></div>
								</div>
								<div class="col-xs-12 col-md-6">
									<input class="simple-input" type="text" value="" placeholder="Teléfono" />
									<div class="emptySpace15"></div>
								</div>
								<div class="col-xs-12 col-md-6">
									<!-- 	SumoSelect-START 	-->
									<div class="sumoWrapper">
										<select name="form" class="SelectBox">
											<option selected disabled>Motivo</option>
											<option>Ampliar información</option>
											<option>Pedir cita</option>
											<option>Otro</option>
										</select>
									</div>
									<!-- 	SumoSelect-END 	-->
									<div class="emptySpace-sm15"></div>
								</div>
								<div class="col-xs-12">
									<textarea class="simple-input" placeholder="Describe el motivo de tu consulta"></textarea>
								</div>
							</div>
							<div class="emptySpace30"></div>
							<div class="btnWrapper">
								<button type="submit" class="button">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Request-END 	-->
		
    <?php include("includes/maps.php"); ?>
	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->


<!-- Modal  Raquel-->
<div class="modal fade" id="RaquelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel">Presentación</h5>
		  
      </div>
      <div class="modal-body" style="padding: 40px;">
		  <img src="img/raquelCV2.jpg" width="100" height="100" class="img-circle" alt=""/>
		  <div class="simple-article">
			  <h3>Raquel Valero Oltra</h3>
		    <p>Especialista en Psicología Clínica y Sexología.  Diploma Post-universitario en Mediación. 
<br>Colegiada CV-01040</p>
			  <h6>Teléfono : 666 474 813</h6>
			  <hr>
<ul style="line-height: 20px;">
		    <li>Colegiada CV-01040. Especialista en Psicología Clínica y Sexología. DEA (Diploma de Estudios Avanzados).</li><br>
			 <li>Diploma Post-universitario en Mediación. Formación en distintos enfoques terapéuticos, resaltando: Gestalt, Psicoanálisis y Sistémica.</li><br>
			 <li>Fundadora de INTESEX (Investigación, Terapia Psicológica y Sexología) y de DEVA:: centro de Psicología y Sexología.</li><br>
			 <li>Miembro de la Mesa Permanente de Salud y Responsable de la Comisión de Sexología y Planificación Familiar del Colegio Oficial de Psicología de la Comunidad Valenciana.</li><br>
<li>Ha trabajado en el Gabinete Psicopedagógico del Ayuntamiento de  Burriana (1983 a 1988) y como Sexóloga en el Centro de Planificación Familiar de Burriana y en el Hospital Dr. Peset de Valencia (1987 a 1993). </li><br>
			  <li>Asesora para la Consellería de Sanidad en materia de Planificación Familiar (1989). Directora de dos promociones de Máster de Sexología (1982 a 2000), avalados por el COP-CV.</li><br>
		  </ul>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">volver</button>
      </div>
    </div>
  </div>
</div>
<!-- End. Modal  Raquel-->

<!-- Modal  Gloria-->
<div class="modal fade" id="GloriaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabe2l">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel2">Presentación</h5>
		  
      </div>
      <div class="modal-body" style="padding: 40px;">
		  <img src="img/gloriaCV2.jpg" width="100" height="100" class="img-circle" alt=""/>
		  <div class="simple-article">
			  <h3>Gloria Sempere Figuérez</h3>
		    <p>Psicóloga con Habilitación Sanitaria. Especialista en Psicoterapia Infanto-Juvenil. 
<br>Colegiada CV-12818</p>
			  <h6>Teléfono : 697 660 173</h6>
			  <hr>
<ul style="line-height: 20px;">
		    <li>Especialista en Psicoterapia Infanto-Juvenil.</li><br>
			 <li>Miembro de la Comisión de Sexología y Planificación Familiar del COPCV.</li><br>
			 <li>Formación práctica de Postgrado en Mindfulness.</li><br>
			 <li>Prácticas voluntarias en Centro de Enfermedad Mental.</li><br>
			<li>Formación para Profesionales que Intervienen en el ámbito de la Infancia y la Adolescencia de la Comunidad Valenciana </li><br>
			<li>Coterapeuta en Grupos de Crecimiento Personal </li><br>
			<li>Perteneciente a grupo de Crecimiento Personal</li><br>
			<li>Tutorizada en el área de Psicología Clínica y Salud mediante el programa Galatea Mentoring del Colegio Oficial de Psicología de la Comunitat Valenciana</li><br>
		  </ul>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">volver</button>
      </div>
    </div>
  </div>
</div>
<!-- End. Modal  Glorial-->

<!-- Modal  Julia-->
<div class="modal fade" id="JuliaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabe2l">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel2">Presentación</h5>
		  
      </div>
      <div class="modal-body" style="padding: 40px;">
		  <img src="img/juliaCV2.jpg" width="100" height="100" class="img-circle" alt=""/>
		  <div class="simple-article">
			  <h3>Julia Tortajada Sanfeliu</span></h3>
		    <p>Doble Grado en Psicología y Logopeda<br>Colegiada CV-15428</p>
			  <h6> </h6>
			  <hr>
<ul style="line-height: 20px;">
<li>Logopeda</li>
<li>Técnico de Selección de Personal de Recursos Humanos.</li><br>
<li>Formación en Técnicas Gestálticas aplicadas en la Infancia y la Adolescencia.</li><br>
<li>Perteneciente a Grupo de Crecimiento  Personal.</li><br>
<li>Psicóloga voluntaria en Acción Social UCV y Coordinadora Voluntaria en Proyecte Somriure (Cáritas) .</li><br>
<li>Miembro de la Comisión de Sexología y Planificación Familiar del COPCV.</li><br>

	<br>

		  </ul>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">volver</button>
      </div>
    </div>
  </div>
</div>
<!-- End. Modal  Julia-->

<!-- Modal  Antonio-->
<div class="modal fade" id="AntonioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabe2l">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel2">Presentación</h5>
		  
      </div>
      <div class="modal-body" style="padding: 40px;">
		  <img src="img/ANTONIOCV2.jpg" width="100" height="100" class="img-circle" alt=""/>
		  <div class="simple-article">
			  <h3>Antonio Nortes Pastor</span></h3>
		    <p>Grado en Psicología<br>Colegiada CV-15482</p>
			  <h6> </h6>
			  <hr>
<ul style="line-height: 20px;">
		    <li>Máster en Sexología: Educación Sexual y Asesoramiento Sexológico.</li><br>
<li>Prácticas en Intervención Comunitarias.</li><br>
<li>Colaborador en CAVAS (Centro de Asistencia a Víctimas de Agresión Sexual): Puntos Violetas y charlas de prevención de Violencia Machista a niños con riesgo de exclusión social.</li><br>
<li>Miembro de la Comisión de Sexología y Planificación Familiar del COPCV.</li><br>


	<br>

		  </ul>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">volver</button>
      </div>
    </div>
  </div>
</div>
<!-- End. Modal  Antonio-->




     <?php include("includes/js.php"); ?>

</body>
</html>
