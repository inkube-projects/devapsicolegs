<?php
   require('includes/core.php');
?>
<!DOCTYPE html>
<html lang="es">
	   <head>
      <meta charset="utf-8">
      <title>Raquel Valero - Psicología y Sexología en Valencia</title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	    <?php include("includes/analytics.php"); ?>
   </head><!--/head-->
<body>
	
	<body>
 	<?php include("includes/cookies.php"); ?>
 	<div id="loader-wrapper"></div>
	 <div id="content-block">
        <!-- Header-START -->
		 <header class="tt-header header2 ccs_BG_header">
			<?php include("includes/infoTop.php"); ?>
	   	  <?php include("includes/navBar.php"); ?>
	</div>
    	</header>
		<!-- Header-END -->
		
		 <div class="headerClearFix headerfix2"></div>
		

      	
		<!-- 	Top banner-START 	-->
		<div class="contentPadding bg" style="background-image: url('img/banner-img2.jpg')">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="servicesTitle">
							<div class="cell-view">
								<h1 class="h1 light as">BLOG</h1>
								<div class="breadCrumbs small">
									<a href="index_desarrollo.php">home</a> <i class="fa fa-angle-right"></i> <span>Blog</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 	Top banner-END 	-->
		
		<div class="contentPadding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-3">
						<div class="mobileSearch large">
							Buscar
							<i class="fa fa-angle-down"></i>
						</div>
						<aside class="blogAside">
							<div class="searchWrapper">
								<input class="simple-input" type="text" placeholder="Buscar">
								<button class="searchBtn" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
							<!-- 	Category-START 	-->
							<div class="recentTitle">
								<h5 class="h5 as">Categorias</h5>
							</div>
							<ul class="categoriesList normall">
								<li><a href="#">Categoria 1</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 3</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 4</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 5</a> <i class="fa fa-angle-right"></i></li>
								<li><a href="#">Categoria 6</a> <i class="fa fa-angle-right"></i></li>
							</ul>
							
							<!-- 	Category-END 	-->
							
							<!-- 	Recent news-START-->
							<div class="recentTitle">
								<h5 class="h5 as">Más recientes</h5>
							</div>
							
							<!-- 	News1-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor </a>
									<span>28/08/</span>
								</div>
							</div>
							<!-- 	News1-END 	-->
							
							<!-- 	News2-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor</a>
									<span>28/08/</span>
								</div>
							</div>
							<!-- 	News2-END 	-->
							
							<!-- 	News3-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor</a>
									<span>28/08/</span>
								</div>
							</div>
							<!-- 	News3-END 	-->
							
							<!-- 	News4-START 	-->
							<div class="recentNewsBlock normall">
								<div class="recentNews">
									<a href="blog.html#">Título del post de prueba, lorem ipsut dolor</a>
									<span>28/08/</span>
								</div>
							</div>
							<!-- 	News4-END 	-->
							
							<!-- 	Recent news-START-->
							<div class="emptySpace30 emptySpace-xs10"></div>
							<!-- 	Tags-START 	-->
							<div class="recentTitle">
								<h5 class="h5 as">TAGS/ETIQUETAS</h5>
							</div>
							<a href="blog.html#" class="tags small">Psicología</a>
							<a href="blog.html#" class="tags small">Depresion</a>
							<a href="blog.html#" class="tags small active">Stress</a>
							<a href="blog.html#" class="tags small">problemas infantiles</a>
							<a href="blog.html#" class="tags small">Anorexia</a>
							<a href="blog.html#" class="tags small">terapia de pareja</a>
							<!-- 	Tags-END 	-->
							<div class="emptySpace-xs30"></div>
						</aside>
					</div>
					
					<!--DETALLE BLOG-->
					
						<div class="col-sm-12 col-md-9" style="margin-bottom: 30px;">
							<div class="mainBlogContent">
							<p style="float: left;"><a href="blog_detalle.php" class="button"> < volver </a></p> 
							<p style="float: right;"><a href="blog_detalle.php" class="button">  siguiente > </a></p>
								<p style="float: right"><a href="blog_detalle.php" class="button"> < anterior </a></p>
							</p>
					 </div>
					</div>
					
					<div class="col-sm-12 col-md-9">

						<div class="mainBlogContent">
							
							<!-- 	Blog1-START 	-->
							<div class="blogWrapper">
								<a href="blog_detalle.php" class="imgWrapper blogThumbnail">
									<img src="img/blog-img.jpg" alt="">
									<span class="timeBlock">18/08/</span>
								</a>
								<div class="blogInfo">
									<p><i class="fa fa-user"></i> Raquel Valero</p>
									<p><i class="fa fa-tag"></i> Nombre de la categoría</p>
									<p><i class="fa fa-comments-o"></i> comentarios: <span>5</span></p>
								</div>
								
								
								<div class="blogContent normall">
									<h5 class="h5 as">A Few Words about Depress Therapy for Couples</h5>
									<div class="simple-article">
										<p>Hot, muggy summers can bring about significant growth issues in your greenery enclosure. While summer fungus–such as the scandalous fine mildew–might not be specifically deadly to your plants, it can bring about compelling scourge that spreads quickly and quickens fall lethargy. What's more, if left untreated, scourge can at last murder a plant. Try not to give growth a chance to assume control  Hot, muggy summers can bring about significant growth issues in your greenery enclosure.</p>
										<p>While summer fungus–such as the scandalous fine mildew–might not be specifically deadly to your plants, it can bring about compelling scourge that spreads quickly and quickens fall lethargy. What's more, if left untreated, scourge can at last murder a plant. Try not to give growth a chance to assume control.</p>
									</div>
									<div class="emptySpace45 emptySpace-xs30"></div>
									<!-- 	Two column content-START 	-->
									<div class="twocolumn">
										<h5 class="h5 as">Two Column Text Sample</h5>
										<div class="row">
											<div class="col-xs-12 col-sm-6">
												<div class="simple-article">
													Ut enim ad minima veniam, quis nostrum exercitatio nem ullam corporis suscipit laboriosam, nisi ut aliqu id ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui.
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="simple-article">
													Ut enim ad minima veniam, quis nostrum exercitatio nem ullam corporis suscipit laboriosam, nisi ut aliqu id ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui.
												</div>
											</div>
										</div>
									</div>
									<!-- 	Two column content-END 	-->
									<div class="emptySpace30"></div>
									<!-- 	Blockquote-START 	-->
									<blockquote>
										<i class="fa fa-quote-left"></i>
										<p>Ut enim ad minima veniam, quis nostrum exercitatio nem ullam corporis suscipit labori osam, nisi ut aliqu id ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur.</p>
										<a href="#">- Michale John</a>
									</blockquote>
									<!-- 	Blockquote-END 	-->
									<div class="emptySpace30"></div>
									<div class="simple-article">
										<p>
											<span>Here is main text</span> quis nostrud exercitation ullamco laboris nisi here is itealic text ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla rure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat here is link cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
										</p>
									</div>
									<div class="emptySpace30"></div>
									<!-- 	Comments-START 	-->
									<div class="commentsWrapper">
										<h5 class="h5 as">Comments <span>4</span></h5>
										<ol class="commentBlock normall commentlist">
										<!-- 	Comment1-START 	-->
										<li>
											<div class="comment">
												<div class="commentContent">
													<div class="imgWrapper">
														<img src="img/comment-img.jpg" alt="">
													</div>
													<a href="#">Merry John</a>
													<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
												</div>
												<div class="commentTime small">
													<p>Dec 09 2019 | <a href="#">Reply</a></p>
												</div>
											</div>
										</li>
										<!-- 	Comment1-END 	-->
										
										<!-- 	Comment2-START 	-->
										<li>
											<div class="comment">
												<div class="commentContent">
													<div class="imgWrapper">
														<img src="img/comment-img2.jpg" alt="">
													</div>
													<a href="#">Michale</a>
													<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
												</div>
												<div class="commentTime small">
													<p>Dec 09 2019 | <a href="#">Reply</a></p>
												</div>
											</div>
										</li>
										<!-- 	Comment2-END 	-->
										
										<!-- 	Comment3-START 	-->
										<li>
											<div class="comment">
												<div class="commentContent">
													<div class="imgWrapper">
														<img src="img/comment-img3.jpg" alt="">
													</div>
													<a href="#">David</a>
													<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
												</div>
												<div class="commentTime small">
													<p>Dec 09 2019 | <a href="#">Reply</a></p>
												</div>
											</div>
											<!-- 	Reply comment-START 	-->
											<ul>
												<li>
													<div class="comment">
														<div class="commentContent">
															<div class="imgWrapper">
																<img src="img/comment-img4.jpg" alt="">
															</div>
															<a href="#">Merry John</a>
															<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
														</div>
														<div class="commentTime small">
															<p>Dec 09 2019 | <a href="#">Reply</a></p>
														</div>
													</div>
												</li>
											</ul>
											<!-- 	Reply comment-START 	-->
										</li>
										<!-- 	Comment3-END 	-->
										
										<!-- 	Comment4-START 	-->
										<li>
											<div class="comment">
												<div class="commentContent">
													<div class="imgWrapper">
														<img src="img/comment-img5.jpg" alt="">
													</div>
													<a href="#">Merry John</a>
													<p>Duis aute irure dolor in reprehenderit in vol uptate velit esse cillum dolore eu fugiat nulla pari atur. Excepteur sint occaecat cupidatat non proid pent.</p>
												</div>
												<div class="commentTime small">
													<p>Dec 09 2019 | <a href="#">Reply</a></p>
												</div>
											</div>
										</li>
										<!-- 	Comment4-END 	-->
										</ol>
									</div>
									<!-- 	Comments-END 	-->
									<div class="emptySpace60 emptySpace-xs30"></div>
									<!-- 	Leave comments-START 	-->
									<div class="commentsForm">
										<h5 class="h5 as">Escribe un comentario</h5>
										<form>
											<div class="row">
												<div class="col-sm-6">
													<input class="simple-input" type="text" value="" placeholder="Enter your name" />
													<div class="emptySpace-xs20"></div>
												</div>
												<div class="col-sm-6">
													<input class="simple-input" type="email" value="" placeholder="Enter your email address" />
												</div>
											</div>
											<div class="emptySpace20"></div>
											<textarea class="simple-input" placeholder="Message"></textarea>
											<div class="emptySpace30"></div>
											<button type="submit" class="button">Enviar comentaro</button>
										</form>
									</div>
									<!-- 	Leave comments-END 	-->
								</div>
								
							</div>
							<!-- 	Blog1-END 	-->

						</div>
					</div>
					
					<!--END. DETALLE BLOG-->
					
					
				</div>
			</div>
		</div>

	 <?php include("includes/footer.php"); ?>


    </div>
    <!--END-->	

     <?php include("includes/js.php"); ?>

</body>
</html>
