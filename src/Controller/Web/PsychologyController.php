<?php

namespace NaS\DevaPsicolegs\Controller\Web;

use NaS\Classes\Controller;
use NaS\Classes\DQLfunctions;
use NaS\DevaPsicolegs\Controller\Web\WebControllerAware;


/**
 * Controlador de los archivos iniciales
 */
class PsychologyController extends Controller
{
    /**
     * Psicología clínica
     *
     * @return view
     */
    public function psychologyAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Psychology/clinic-psychology.twig', [
            'act' => 1
        ]);
    }

    /**
     * Psicoterapia juvenil
     *
     * @return view
     */
    public function psychotherapyAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Psychology/psychotherapy.twig', [
            'act' => 2
        ]);
    }

    public function educationalPsychologyAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Psychology/educational-psychology.twig', [
            'act' => 3
        ]);
    }
}

?>