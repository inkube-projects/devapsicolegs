<?php

namespace NaS\DevaPsicolegs\Controller\Web;

class WebControllerAware
{

    function __construct()
    {
        $this->em = getEntityManager();
    }

    /**
     * Obtiene las categorías
     * @return object
     */
    public function getCategories()
    {
        $em = $this->em;
        $postCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        return $postCategoryRepository->findAll();
    }

    /**
     * Obtiene los ultimos 4 posts
     * @return object
     */
    public function lastPosts()
    {
        $em = $this->em;
        $postRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Post');
        // Publicaciones
        $arr_filters = [
            'src_draft' => false,
            'src_active' => true,
            'order_id' => true,
            'offset' => 0,
            'limit' => 4
        ];

        return $postRepository->getPostByFilter($arr_filters);
    }

    /**
     * Obtiene los tags
     * @return object
     */
    public function getTags()
    {
        $em = $this->em;
        $tagsRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Tags');
        return $tagsRepository->findAll();
    }
}


?>
