<?php

namespace NaS\DevaPsicolegs\Controller\Web;

use NaS\Classes\Controller;
use NaS\Classes\DQLfunctions;
use NaS\DevaPsicolegs\Controller\Web\WebControllerAware;


/**
 * Controlador de los archivos iniciales
 */
class IndexController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Pantalla de inicio de sesión
     *
     * @return void
     */
    public function homeAction()
    {
        $em = $this->em;
        $postRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Post');
        $sliderRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Slider');
        $sliders = $sliderRepository->findAll();

        $arr_last_post = [];
        $arr_filters = [
            'src_draft' => false,
            'src_active' => true,
            'order_id' => true,
            'offset' => "0",
            'limit' => 3
        ];
        $posts = $postRepository->getPostByFilter($arr_filters);
        foreach ($posts as $key => $post) {
            $arr_last_post[$key] = [
                'coverImage' => ($post->getCoverImage()) ? APP_IMG_VIEW_POST."/".$post->getCode()."/cover/".$post->getCoverImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
                'title' => $post->getTitle(),
                'date' => $post->getCreateAt()->format('d/m/Y'),
                'content' => substr(sanitize($post->getContent()), 0, 150)."...",
                'url' => $post->getPostSeo()->getUrl()
            ];
        }


        echo $this->twig->render('Web/Index/home.twig', [
            'slider' => $sliders,
            'lastPosts' => $arr_last_post
        ]);
    }

    /**
     * Cookies
     *
     * @return view
     */
    public function cookiesAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Index/cookies.twig');
    }

    /**
     * Legal
     *
     * @return view
     */
    public function legalAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Index/legal.twig');
    }


// ---------------------------------------------------------------------------------------------------------------------------------------------------
}
?>
