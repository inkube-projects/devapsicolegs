<?php

namespace NaS\DevaPsicolegs\Controller\Web;

use NaS\Classes\Controller;
use NaS\Classes\DQLfunctions;
use NaS\DevaPsicolegs\Controller\Web\WebControllerAware;


/**
 * Controlador de los archivos iniciales
 */
class SexologyController extends Controller
{
    /**
     * Introducción
     *
     * @return view
     */
    public function introductionAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Sexology/introduction.twig', [
            'act' => 1
        ]);
    }

    /**
     * Educación sexual
     *
     * @return view
     */
    public function sexualEducationAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Sexology/sexual-education.twig', [
            'act' => 2
        ]);
    }

    /**
     * Consueling
     *
     * @return view
     */
    public function consuelingAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Sexology/consueling.twig', [
            'act' => 3
        ]);
    }

    /**
     * Sexologia clinica
     *
     * @return void
     */
    public function clinicalSexologyAction()
    {
        $em = $this->em;
        echo $this->twig->render('Web/Sexology/clinical-sexology.twig', [
            'act' => 4
        ]);
    }
}

?>