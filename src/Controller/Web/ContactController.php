<?php

namespace NaS\DevaPsicolegs\Controller\Web;

use NaS\Classes\Controller;



/**
 * Controlador de los archivos iniciales
 */
class ContactController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Pantalla de contacto
     *
     * @return void
     */
    public function contactAction()
    {
        $em = $this->em;
        $contactReasonRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\ContactReason');
        $contactReason = $contactReasonRepository->findAll();

        echo $this->twig->render('Web/Contact/contact.twig', [
            'token'         => $this->createFormToken('contact'),
            'contactReason' => $contactReason
        ]);
    }


// ---------------------------------------------------------------------------------------------------------------------------------------------------
}
?>
