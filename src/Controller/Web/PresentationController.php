<?php

namespace NaS\DevaPsicolegs\Controller\Web;

use NaS\Classes\Controller;
use NaS\Classes\DQLfunctions;
use NaS\DevaPsicolegs\Controller\Web\WebControllerAware;


/**
 * Controlador de los archivos iniciales
 */
class PresentationController extends Controller
{
    /**
     * Presentacion
     *
     * @return void
     */
    public function centerAction()
    {
        $em = $this->em;
        $informationRepository = $em->getRepository('\NaS\DevaPsicolegs\Entity\Information');
        $information = $informationRepository->findOneBy(['id' => 1]);

        // Equipo
        $teamRepository = $em->getRepository('\NaS\DevaPsicolegs\Entity\Team');
        $team = $teamRepository->findAll();

        // Razones
        $contactReasonRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\ContactReason');
        $contactReason = $contactReasonRepository->findAll();

        $arr_member = [];
        foreach ($team as $key => $member) {
            $arr_member[] = [
                'id' => $member->getId(),
                'profileImage' => ($member->getProfileImage()) ? APP_IMG_VIEW_INFORMATION."/team/".$member->getProfileImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
                'names' => $member->getName(),
                'description' => $member->getDescription(),
                'shortDescription' => $member->getShortDescription(),
                'phone' => $member->getPhone(),
                'experience' => $member->getExperience()
            ];
        }

        // Serivicios
        $servicesRepository = $em->getRepository('\NaS\DevaPsicolegs\Entity\Services');
        $services = $servicesRepository->findAll();

        // Subservices
        $subservicesRepository = $em->getRepository('\NaS\DevaPsicolegs\Entity\SubServices');
        $subservices = $subservicesRepository->findAll();

        echo $this->twig->render('Web/Presentation/center.twig', [
            'information' => $information,
            'team'        => $arr_member,
            'services'    => $services,
            'subservices' => $subservices,
            'contactReason' => $contactReason
        ]);
    }
}
?>
