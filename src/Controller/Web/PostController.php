<?php

namespace NaS\DevaPsicolegs\Controller\Web;

use NaS\Classes\Controller;
use NaS\Classes\Paginator;
use NaS\Classes\Request;
use NaS\DevaPsicolegs\Controller\Web\WebControllerAware;


/**
 * Controlador de los archivos iniciales
 */
class PostController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Pantalla de inicio de sesión
     *
     * @return void
     */
    public function listAction($request, $entry = "", $arr_filters_ext = "", $breadcrumb = "")
    {
        $webController = new WebControllerAware;
        $em = $this->em;
        $postRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Post');
        $entry = ($entry == "") ? "/blog" : "/".$entry;

        // Publicaciones
        $arr_filters = [
            'src_draft' => "0",
            'src_active' => true,
            'order_id' => true,
        ];
        $arr_filters = ($arr_filters_ext != "") ? array_merge($arr_filters, $arr_filters_ext) : $arr_filters;
        $query = $postRepository->getPostByFilter($arr_filters, false);
        $paginator = new Paginator(BASE_URL.$entry, '');
        $pagination = $paginator->paginate($query, $request->getParameter('page'), 10);

        $arr_posts = array();
        foreach ($pagination['results'] as $key => $post) {
            $arr_posts[$key]['coverImage'] = ($post->getCoverImage()) ? APP_IMG_VIEW_POST."/".$post->getCode()."/cover/".$post->getCoverImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg";
            $arr_posts[$key]['title'] = $post->getTitle();
            $arr_posts[$key]['date'] = $post->getCreateAt()->format('d/m/Y');
            $arr_posts[$key]['content'] = substr(sanitize($post->getContent()), 0, 250)."...";
            $arr_posts[$key]['url'] = $post->getPostSeo()->getUrl();
            $arr_posts[$key]['seo'] = $post->getPostSeo();
            $arr_posts[$key]['categories'] = $post->getPostCategory();
            $arr_posts[$key]['cnt_comments'] = count($post->getPostComment());
        }

        echo $this->twig->render('Web/Post/post-list.twig', [
            'posts'      => $arr_posts,
            'breadcrumb' => $breadcrumb,
            'pagination' => $pagination['paginator'],
            'categories' => $webController->getCategories(),
            'lastPost'   => $webController->lastPosts(),
            'tags'       => $webController->getTags()
        ]);
    }

    /**
     * Detalle de la publicacion
     *
     * @param integer  $id  ID de la publicación
     * @return view
     */
    public function showAction($id)
    {
        $webController = new WebControllerAware;
        $em = $this->em;
        $postRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Post');
        $post = $postRepository->findOneBy(['id' => $id]);
        $comments = $postRepository->getComments($id);

        if (!$post) {
            $request = new Request;
            $this->listAction($request); exit;
        }

        $arr_post = [
            'id' => $id,
            'title' => $post->getTitle(),
            'content' => $post->getContent(),
            'coverImage' => ($post->getCoverImage()) ? APP_IMG_VIEW_POST."/".$post->getCode()."/cover/".$post->getCoverImage() : false,
            'banner' => ($post->getBannerImage()) ? APP_IMG_VIEW_POST."/".$post->getCode()."/banner/".$post->getBannerImage() : false,
            'gallery' => $post->getPostGalleryImage(),
            'create' => $post->getCreateAt()->format('d/m/Y'),
            'seo' => $post->getPostSeo(),
            'category' => $post->getPostCategory(),
            'cnt_comments' => count($post->getPostComment())
        ];

        echo $this->twig->render('Web/Post/post-detail.twig', [
            'post'       => $arr_post,
            'breadcrumb' => '',
            'categories' => $webController->getCategories(),
            'lastPost'   => $webController->lastPosts(),
            'tags'       => $webController->getTags(),
            'token'      => $this->createFormToken('comment'),
            'comments'   => $comments
        ]);
    }


// ---------------------------------------------------------------------------------------------------------------------------------------------------
}
?>
