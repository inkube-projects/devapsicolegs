<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;
use NaS\Classes\DQLfunctions;

/**
 * Controlador de los archivos iniciales
 */
class IndexController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Pantalla de inicio de sesión
     *
     * @return void
     */
    public function loginAction($message = NULL)
    {
        echo $this->twig->render('Admin/Index/login.twig', array(
            'token' => $this->createFormToken('login'),
            'alert' => $message
        ));
    }

    /**
     * Pantalla principal del gestor
     *
     * @return void
     */
    public function homeAction()
    {   
        $DQLfunctions = new DQLfunctions;

        echo $this->twig->render('Admin/Index/home.twig', [
            'cnt_post' => $DQLfunctions->getCount("NaS\DevaPsicolegs\Entity\Post", "p", "p.draft=0"),
            'cnt_category' => $DQLfunctions->getCount("NaS\DevaPsicolegs\Entity\PostCategory", "pc", "pc.id <> ''"),
            'cnt_users' => $DQLfunctions->getCount("NaS\DevaPsicolegs\Entity\User", "u", "u.role=3")
        ]);
    }

    /**
     * Cerrar sesión
     *
     * @return void
     */
    public function logOutAction()
    {
        @$_SESSION['SES_ADMIN_DEVAPSICOLEGS'] = "";
        session_unset(); //para eliminar las variables de sesion
        session_destroy(); //con esto destruyes la sesion
        header('location: '.BASE_URL.'/inkcms/login');
    }
}


?>
