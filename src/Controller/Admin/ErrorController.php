<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * Controlador de los archivos iniciales
 */
class ErrorController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function notFoundAction()
    {
        echo '404';
    }
}


?>
