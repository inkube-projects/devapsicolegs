<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 *
 */
class SeoSectionController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * List
     * @return view
     */
    public function listAction()
    {
        $em = $this->em;
        $seoSectionRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\SeoSection');
        $sections = $seoSectionRepository->findAll();

        echo $this->twig->render('Admin/SEO/seo-section-list.twig', array(
            'sections' => $sections,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Add
     * @return view
     */
    public function addAction()
    {
        $em = $this->em;

        echo $this->twig->render('Admin/SEO/seo-section-form.twig', array(
            'token'      => $this->createFormToken('seoSection'),
            'flash'      => '',
            'id'         => '',
            'act'        => 1,
            'form'       => 'frm-seo-section',
            'seoSection' => ""
        ));
    }

    /**
     * Edit
     *
     * @param integer $id ID de la sección
     * @return view
     */
    public function editAction($id)
    {
        $em = $this->em;
        $seoSectionRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\SeoSection');
        $section = $seoSectionRepository->findOneBy(array('id' => $id));
        if (!$section) { $this->listAction(); exit; }

        echo $this->twig->render('Admin/SEO/seo-section-form.twig', array(
            'token'      => $this->createFormToken('seoSection'),
            'flash'      => $this->flashMessageGlobal(),
            'id'         => $id,
            'act'        => 2,
            'form'       => 'frm-seo-section',
            'seoSection' => $section
        ));
    }
}


?>
