<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 *
 */
class SeoController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * List
     * @return view
     */
    public function listAction()
    {
        $em = $this->em;
        $seoRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Seo');
        $seo = $seoRepository->findAll();

        echo $this->twig->render('Admin/SEO/seo-list.twig', array(
            'seo' => $seo,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Add
     * @return view
     */
    public function addAction()
    {
        $em = $this->em;
        $seoSectionRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\SeoSection');
        $sections = $seoSectionRepository->findAll();

        echo $this->twig->render('Admin/SEO/seo-form.twig', array(
            'token'       => $this->createFormToken('seo'),
            'flash'       => '',
            'id'          => '',
            'act'         => 1,
            'form'        => 'frm-seo',
            'seo'         => "",
            'seoSections' => $sections
        ));
    }

    /**
     * Edit
     *
     * @param integer $id ID de la sección
     * @return view
     */
    public function editAction($id)
    {
        $em = $this->em;
        $seoRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Seo');
        $seo = $seoRepository->findOneBy(array('id' => $id));
        if (!$seo) { $this->listAction(); exit; }
        $seoSectionRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\SeoSection');
        $sections = $seoSectionRepository->findAll();

        echo $this->twig->render('Admin/SEO/seo-form.twig', array(
            'token'       => $this->createFormToken('seo'),
            'flash'       => $this->flashMessageGlobal(),
            'id'          => $id,
            'act'         => 2,
            'form'        => 'frm-seo',
            'seo'         => $seo,
            'seoSections' => $sections
        ));
    }
}


?>
