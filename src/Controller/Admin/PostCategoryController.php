<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * Controller
 */
class PostCategoryController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de publicacioness
     *
     * @return object
     */
    public function listAction()
    {
        $em = $this->em;
        $postCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        $postCategories = $postCategoryRepository->findAll();

        echo $this->twig->render('Admin/Post/category-list.twig', array(
            'categories' => $postCategories,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Agregar categoría
     * @return object
     */
    public function addAction()
    {
        $em = $this->em;

        // Información del usuario
        $arr_category = array(
            'name' => "",
            'description' => "",
            'coverImage' => APP_IMG_VIEW_NOIMAGE."/no_image.jpg"
        );

        echo $this->twig->render('Admin/Post/category-form.twig', array(
            'token'     => $this->createFormToken('postCategory'),
            'flash'     => '',
            'id'        => '',
            'act'       => 1,
            'category'  => $arr_category,
            'form'      => 'frm-post-category'
        ));
    }

    /**
     * Edita una categoría
     *
     * @param integer $id
     * @return void
     */
    public function editAction($id)
    {
        $em = $this->em;
        $postCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        $postCategory = $postCategoryRepository->findOneBy(array('id' => $id));
        if (!$postCategory) { $this->listAction(); exit; }

        // Información del usuario
        $arr_category = array(
            'name' => $postCategory->getName(),
            'description' => $postCategory->getDescription(),
            'coverImage' => ($postCategory->getCoverImage()) ? APP_IMG_VIEW_POST_CATEGORY."/".$postCategory->getCoverImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg"
        );

        echo $this->twig->render('Admin/Post/category-form.twig', array(
            'token'     => $this->createFormToken('postCategory'),
            'flash'     => $this->flashMessageGlobal(),
            'id'        => $id,
            'act'       => 2,
            'category'  => $arr_category,
            'form'      => 'frm-post-category'
        ));
    }
}


?>
