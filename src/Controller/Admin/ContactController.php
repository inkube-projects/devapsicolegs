<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * Controller
 */
class ContactController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de mensjaes
     *
     * @return object
     */
    public function listAction()
    {
        $em = $this->em;
        $contactRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Contact');
        $contact = $contactRepository->findAll();

        echo $this->twig->render('Admin/Contact/contact-list.twig', array(
            'token'   => $this->createFormToken('contact'),
            'contact' => $contact,
            'flash'   => $this->flashMessageGlobal()
        ));
    }

    /**
     * Edita un mensaje
     *
     * @param integer $id
     * @return void
     */
    public function editAction($id)
    {
        $em = $this->em;
        $contactRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Contact');
        $contact = $contactRepository->findOneBy(array('id' => $id));
        if (!$contact) { $this->listAction(); exit; }

        $contact->setReaded(1);
        $em->persist($contact);
        $em->flush();

        echo $this->twig->render('Admin/Contact/contact-form.twig', array(
            'token'     => $this->createFormToken('contact'),
            'flash'     => $this->flashMessageGlobal(),
            'id'        => $id,
            'act'       => 2,
            'contact'   => $contact,
            'form'      => 'frm-contact'
        ));
    }
}


?>
