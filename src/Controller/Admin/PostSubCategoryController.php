<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * Controller
 */
class PostSubCategoryController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de publicacioness
     *
     * @return object
     */
    public function listAction()
    {
        $em = $this->em;
        $postSubCategory = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory');
        $postSubCategories = $postSubCategory->findAll();

        echo $this->twig->render('Admin/Post/subcategory-list.twig', array(
            'subcategories' => $postSubCategories,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Agregar categoría
     * @return object
     */
    public function addAction()
    {
        $em = $this->em;

        // Información del usuario
        $arr_subcategory = array(
            'name' => "",
            'description' => "",
            'cateory' => ''
        );

        $postCategory = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        $categories = $postCategory->findAll();

        echo $this->twig->render('Admin/Post/subcategory-form.twig', array(
            'token'       => $this->createFormToken('postSubCategory'),
            'flash'       => '',
            'id'          => '',
            'act'         => 1,
            'subcategory' => $arr_subcategory,
            'categories'  => $categories,
            'form'        => 'frm-post-subcategory'
        ));
    }

    /**
     * Edita una categoría
     *
     * @param integer $id
     * @return void
     */
    public function editAction($id)
    {
        $em = $this->em;

        $postSubCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory');
        $postSubCategory = $postSubCategoryRepository->findOneBy(array('id' => $id));
        if (!$postSubCategory) { $this->listAction(); exit; }

        // Información del usuario
        $arr_subcategory = array(
            'name' => $postSubCategory->getName(),
            'description' => $postSubCategory->getDescription(),
            'category' => $postSubCategory->getPostCategory()->getId()
        );

        $postCategory = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        $categories = $postCategory->findAll();

        echo $this->twig->render('Admin/Post/subcategory-form.twig', array(
            'token'       => $this->createFormToken('postSubCategory'),
            'flash'       => $this->flashMessageGlobal(),
            'id'          => $id,
            'act'         => 2,
            'subcategory' => $arr_subcategory,
            'categories'  => $categories,
            'form'        => 'frm-post-subcategory'
        ));
    }
}


?>
