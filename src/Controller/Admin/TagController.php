<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * TagController
 */
class TagController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * List
     *
     * @return object
     */
    public function listAction()
    {
        $em = $this->em;
        $tagRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Tags');
        $tags = $tagRepository->findAll();

        echo $this->twig->render('Admin/Tag/tag-list.twig', array(
            'tags' => $tags,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Add
     *
     * @return object
     */
    public function addAction()
    {
        $em = $this->em;

        // Información del usuario
        $arr_tag = array(
            'name' => ""
        );

        echo $this->twig->render('Admin/Tag/tag-form.twig', array(
            'token'     => $this->createFormToken('tags'),
            'flash'     => '',
            'id'        => '',
            'act'       => 1,
            'form'      => 'frm-tag',
            'tag'       => $arr_tag
        ));
    }

    /**
     * Edit
     *
     * @param integer $id  ID de la etiqueta
     * @return void
     */
    public function editAction($id)
    {
        $em = $this->em;
        $tagsRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Tags');
        $tags = $tagsRepository->findOneBy(array('id' => $id));
        if (!$tags) { $this->listAction(); exit; }

        // Información del usuario
        $arr_tag = array(
            'name' => $tags->getName()
        );

        echo $this->twig->render('Admin/Tag/tag-form.twig', array(
            'token'     => $this->createFormToken('tags'),
            'flash'     => $this->flashMessageGlobal(),
            'id'        => $id,
            'act'       => 2,
            'form'      => 'frm-tag',
            'tag'       => $arr_tag
        ));
    }
}


?>