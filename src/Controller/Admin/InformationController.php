<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * Controlador para la información extra
 */
class InformationController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Hitoria
     *
     * @return view
     */
    public function historyAction()
    {
        $em = $this->em;
        $informationRepossitory = $em->getRepository('NaS\DevaPsicolegs\Entity\Information');
        $information = $informationRepossitory->findOneBy(['id' => 1]);

        echo $this->twig->render('Admin/Information/information-form.twig', array(
            'token'       => $this->createFormToken('history'),
            'flash'       => $this->flashMessageGlobal(),
            'act'         => 1,
            'form'        => 'frm-history',
            'content'     => $information->getHistory(),
            'currentPath' => 'history'
        ));
    }

    /**
     * Información del equipo
     *
     * @return view
     */
    public function teamAction()
    {
        $em = $this->em;
        $informationRepossitory = $em->getRepository('NaS\DevaPsicolegs\Entity\Information');
        $information = $informationRepossitory->findOneBy(['id' => 1]);

        $teamRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Team');
        $team = $teamRepository->findAll();
        $arr_team = array();
        foreach ($team as $key => $member) {

            $arr_team[] = [
                'id' => $member->getId(),
                'name' => $member->getName(),
                'profileImage' => ($member->getProfileImage()) ? APP_IMG_VIEW_INFORMATION."/team/".$member->getProfileImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
                'create' => $member->getCreateAt()->format('d-m-Y')
            ];
        }

        echo $this->twig->render('Admin/Information/information-form.twig', array(
            'token'       => $this->createFormToken('team'),
            'flash'       => $this->flashMessageGlobal(),
            'act'         => 2,
            'form'        => 'frm-team',
            'content'     => $information->getTeam(),
            'teamList'    => $arr_team,
            'currentPath' => 'team',
            'removeURL'   => '/inkcms/ajax/team/remove'
        ));
    }

    /**
     * Editar miembro del equipo
     * @param  integer $id ID del miembro
     * @return view
     */
    public function editMemberAction($id)
    {
        $em = $this->em;
        $teamRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Team');
        $team = $teamRepository->findOneBy(array('id' => $id));
        if (!$team) { $this->teamAction(); exit; }

        $arr_member = [
            'id' => $team->getId(),
            'name' => $team->getName(),
            'shortDescription' => $team->getShortDescription(),
            'description' => $team->getDescription(),
            'profileImage' => ($team->getProfileImage()) ? APP_IMG_VIEW_INFORMATION."/team/".$team->getProfileImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
            'phone' => $team->getPhone(),
            'experience' => $team->getExperience(),
            'create' => $team->getCreateAt()->format('d-m-Y')
        ];

        echo $this->twig->render('Admin/Information/team-form.twig', array(
            'token'       => $this->createFormToken('team'),
            'flash'       => $this->flashMessageGlobal(),
            'form'        => 'frm-team',
            'team'        => $arr_member
        ));
    }

    /**
     * Servicios
     * @return view
     */
    public function servicesAction()
    {
        $em = $this->em;
        $informationRepossitory = $em->getRepository('NaS\DevaPsicolegs\Entity\Information');
        $information = $informationRepossitory->findOneBy(['id' => 1]);

        $servicesRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Services');
        $services = $servicesRepository->findAll();


        echo $this->twig->render('Admin/Information/information-form.twig', array(
            'token'       => $this->createFormToken('services'),
            'flash'       => $this->flashMessageGlobal(),
            'act'         => 3,
            'form'        => 'frm-services',
            'content'     => $information->getServices(),
            'services'    => $services,
            'currentPath' => 'services',
            'removeURL'   => '/inkcms/ajax/service/remove'
        ));
    }

    /**
     * Editar servicio
     * @param  integer $id ID del servicio
     * @return view
     */
    public function editServiceAction($id)
    {
        $em = $this->em;
        $servicesRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Services');
        $service = $servicesRepository->findOneBy(array('id' => $id));
        if (!$service) { $this->servicesAction(); exit; }

        echo $this->twig->render('Admin/Information/service-form.twig', array(
            'token'       => $this->createFormToken('service'),
            'flash'       => $this->flashMessageGlobal(),
            'form'        => 'frm-service',
            'service'     => $service,
            'id'          => $id
        ));
    }

    /**
     * Listado de subservicios
     *
     * @return view
     */
    public function listSubServicesAction()
    {
        $em = $this->em;
        $subServicesRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\SubServices');
        $subServices = $subServicesRepository->findAll();

        echo $this->twig->render('Admin/Information/subservices-list.twig', array(
            'flash'       => $this->flashMessageGlobal(),
            'subServices' => $subServices
        ));
    }

    /**
     * Agregar subservicio
     *
     * @return void
     */
    public function addSubServicesAction()
    {
        $em = $this->em;
        $servicesRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Services');
        $services = $servicesRepository->findAll();

        echo $this->twig->render('Admin/Information/subservice-form.twig', array(
            'token'      => $this->createFormToken('subservice'),
            'flash'      => $this->flashMessageGlobal(),
            'id'         => '',
            'act'        => 1,
            'services'   => $services,
            'subservice' => '',
            'form'       => 'frm-subservice'
        ));
    }

    /**
     * Edit action - subservice
     *
     * @param integer $id
     * @return view
     */
    public function editSubServicesAction($id)
    {
        $em = $this->em;
        $subServiceRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\SubServices');
        $subService = $subServiceRepository->findOneBy(['id' => $id]);
        if (!$subService) { $this->listSubServicesAction(); exit; }

        $servicesRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Services');
        $services = $servicesRepository->findAll();

        echo $this->twig->render('Admin/Information/subservice-form.twig', array(
            'token'      => $this->createFormToken('subservice'),
            'flash'      => $this->flashMessageGlobal(),
            'id'         => $id,
            'act'        => 2,
            'services'   => $services,
            'subservice' => $subService,
            'form'       => 'frm-subservice'
        ));
    }
}


?>
