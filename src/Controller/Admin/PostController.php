<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * Controller
 */
class PostController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de publicacioness
     *
     * @return object
     */
    public function listAction($request = null)
    {
        $em = $this->em;
        $arr_filter = [];
        if ($request) {
            $arr_filter = [
                'src_draft' => ($request->getParameter('draft')) ? @number_format($request->getParameter('draft'),0,'','') : "",
                'src_headline' => ($request->getParameter('headline')) ? @number_format($request->getParameter('headline'),0,'','') : "",
                'src_active' => ($request->getParameter('active')) ? @number_format($request->getParameter('active'),0,'','') : "",
                'src_category' => ($request->getParameter('category') != "all") ? explode(",", sanitize(secure_mysql($request->getParameter('category')))) : "",
                'src_subcategory' => ($request->getParameter('subcategory') != "all") ? explode(",", sanitize(secure_mysql($request->getParameter('subcategory')))) : "",
                'src_tags' => ($request->getParameter('tags') != "all") ? explode(",", sanitize(secure_mysql($request->getParameter('tags')))) : "",
                'src_user' => ($request->getParameter('user') != "all") ? @number_format($request->getParameter('user'),0,'','') : "",
                'src_start_date' => ($request->getParameter('dateFrom') != "all") ? sanitize(secure_mysql($request->getParameter('dateFrom'))) : "",
                'src_end_date' => ($request->getParameter('dateTo') != "all") ? sanitize(secure_mysql($request->getParameter('dateTo'))) : ""
            ];
        }


        $postRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Post');
        $posts = $postRepository->getPostByFilter($arr_filter);

        // Categorías
        $postCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        $postCategory = $postCategoryRepository->findAll();

        // Subcategorias
        $postSubCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory');
        $postSubCategory = $postSubCategoryRepository->findAll();

        // Tags
        $tagsRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Tags');
        $tags = $tagsRepository->findAll();

        // Moderadores
        $userRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\User');
        $users = $userRepository->getModerators('all');

        echo $this->twig->render('Admin/Post/post-list.twig', array(
            'posts' => $posts,
            'search' => $arr_filter,
            'categories' => $postCategory,
            'subcategories' => $postSubCategory,
            'tags' => $tags,
            'users' => $users,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Formulario de nueva publicación
     * @return void
     */
    public function addAction()
    {
        $em = $this->em;
        // Se limpia el directorio temporal
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/category/");
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/CK/");
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/gallery/");
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/cover/");

        // Información de la publicación
        $arr_post = array(
            'title' => "",
            'content' => "",
            'coverImage' => APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
            'bannerImage' => APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
            'video' => "",
            'user' => "",
            'category' => "",
            'subCategory' => "",
            'tags' => "",
            'gallery' => "",
            'seo' => "",
            'headline' => false,
            'active' => true,
            'draft' => true
        );

        // Categorías
        $postCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        $postCategory = $postCategoryRepository->findAll();

        // Subcategorias
        $postSubCategory = array();

        // Tags
        $tagsRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Tags');
        $tags = $tagsRepository->findAll();

        echo $this->twig->render('Admin/Post/post-form.twig', array(
            'token'         => $this->createFormToken('post'),
            'flash'         => '',
            'id'            => '',
            'act'           => 1,
            'post'          => $arr_post,
            'categories'    => $postCategory,
            'subCategories' => $postSubCategory,
            'tags'          => $tags,
            'form'          => 'frm-post'
        ));
    }

    /**
     * Formulario para editar publicación
     * @param  integer $id ID de la publicación
     * @return void
     */
    public function editAction($id)
    {
        $em = $this->em;
        $postRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Post');
        $post = $postRepository->findOneBy(array('id' => $id));
        if (!$post) { $this->listAction(); exit; }

        // Se limpia el directorio temporal
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/category/");
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/CK/");
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/gallery/");
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts/cover/");

        // Tags seleccionados
        $selected_tags = array();
        foreach ($post->getTags() as $tag) {
            $selected_tags[] = $tag->getId();
        }

        // Categorías seleccionadas
        $selected_categories = array();
        foreach ($post->getPostCategory() as $cat) {
            $selected_categories[] = $cat->getId();
        }

        // Subcategorías seleccionadas
        $selected_subcategories = array();
        foreach ($post->getPostSubCategory() as $sub) {
            $selected_subcategories[] = $sub->getId();
        }

        // Información de la publicación
        $arr_post = array(
            'title' => $post->getTitle(),
            'content' => $post->getContent(),
            'coverImage' =>  ($post->getCoverImage()) ? APP_IMG_VIEW_POST."/".$post->getCode()."/cover/".$post->getCoverImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
            'bannerImage' => ($post->getBannerImage()) ? APP_IMG_VIEW_POST."/".$post->getCode()."/banner/".$post->getBannerImage() : APP_IMG_VIEW_NOIMAGE."/no_image.jpg",
            'video' => $post->getVideo(),
            'user' => $post->getUser(),
            'category' => $selected_categories,
            'subCategory' => $selected_subcategories,
            'tags' => $selected_tags,
            'gallery' => $post->getPostGalleryImage(),
            'code' => $post->getCode(),
            'seo' => $post->getPostSeo(),
            'headline' => ($post->getHeadline()) ? "1" : "0",
            'active' => ($post->getActive()) ? "1" : "0",
            'draft' => ($post->getDraft()) ? "1" : "0",
        );

        // Categorías
        $postCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory');
        $postCategory = $postCategoryRepository->findAll();

        // Subcategorias
        $postSubCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory');
        $postSubCategory = $postSubCategoryRepository->findSubByCat($selected_categories);

        // Tags
        $tagsRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Tags');
        $tags = $tagsRepository->findAll();

        echo $this->twig->render('Admin/Post/post-form.twig', array(
            'token'         => $this->createFormToken('post'),
            'flash'         => $this->flashMessageGlobal(),
            'id'            => $id,
            'act'           => 2,
            'post'          => $arr_post,
            'categories'    => $postCategory,
            'subCategories' => $postSubCategory,
            'tags'          => $tags,
            'form'          => 'frm-post'
        ));
    }
}


?>
