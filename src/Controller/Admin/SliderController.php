<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 *
 */
class SliderController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * List
     * @return view
     */
    public function listAction()
    {
        $em = $this->em;
        $sliderRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Slider');
        $slider = $sliderRepository->findAll();

        $arr_slider = array();
        foreach ($slider as $key => $img) {
            $arr_slider[] = [
                'id' => $img->getId(),
                'image' => APP_IMG_VIEW_SLIDER."/".$img->getImage(),
                'create' => $img->getCreateAt()->format('d-m-Y')
            ];
        }

        echo $this->twig->render('Admin/Slider/slider-list.twig', array(
            'slider' => $arr_slider,
            'flash'  => $this->flashMessageGlobal()
        ));
    }

    /**
     * Add
     *
     * @return view
     */
    public function addAction()
    {
        $em = $this->em;

        echo $this->twig->render('Admin/Slider/slider-form.twig', array(
            'slider' => '',
            'flash'  => '',
            'form'   => 'frm-add',
            'id'     => '',
            'act'    => '1',
            'token'  => $this->createFormToken('slider'),
        ));
    }

    /**
     * Edit
     *
     * @param inetger $id   ID del slider
     * @return view
     */
    public function editAction($id)
    {
        $em = $this->em;
        $sliderRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Slider');
        $slider = $sliderRepository->findOneBy(array('id' => $id));
        if (!$slider) { $this->listAction(); exit; }

        $arr_slider = [
            'url' => $slider->getUrl(),
            'content' => $slider->getContent(),
            'image' => APP_IMG_VIEW_SLIDER."/".$slider->getImage()
        ];

        echo $this->twig->render('Admin/Slider/slider-form.twig', array(
            'slider' => $arr_slider,
            'flash'  => $this->flashMessageGlobal(),
            'form'   => 'frm-edit',
            'id'     => $id,
            'act'    => '2',
            'token'  => $this->createFormToken('slider'),
        ));
    }
}


?>
