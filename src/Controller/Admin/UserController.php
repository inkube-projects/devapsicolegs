<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

class UserController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de usuarios
     *
     * @return object
     */
    public function listAction()
    {
        $em = $this->em;
        $userRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\User');
        $users = $userRepository->findBy(array('role' => 3));

        echo $this->twig->render('Admin/User/user-list.twig', array(
            'users' => $users,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Agregar usuario
     *
     * @return void
     */
    public function addAction()
    {
        $em = $this->em;

        // Información del usuario
        $arr_user = array(
            'email' => "",
            'username' => "",
            'name' => "",
            'last_name' => "",
            'birthdate' => "",
            'country' => "",
            'city' => "",
            'address_1' => "",
            'address_2' => "",
            'phone_1' => "",
            'phone_2' => "",
            'note' => "",
            'postal_code' => "",
            'profile_image' => APP_IMG_VIEW_NOIMAGE."/profile.jpg",
            'status' => ''
        );

        // Paises
        $countryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Country');
        $countries = $countryRepository->findAll();

        // Estado de los usuarios
        $userStatusRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\UserStatus');
        $status = $userStatusRepository->findAll();

        echo $this->twig->render('Admin/User/user-form.twig', array(
            'token'     => $this->createFormToken('user'),
            'id'        => '',
            'act'       => 1,
            'user'      => $arr_user,
            'countries' => $countries,
            'status'    => $status,
            'flash'     => ''
        ));
    }

    /**
     * Formulario para editar usuario
     * @param  integer $id ID del usuario
     * @return object
     */
    public function editAction($id)
    {
        $em = $this->em;
        $userRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\User');
        $user = $userRepository->findOneBy(array('id' => $id));

        if (!$user) { $this->listAction(); exit; }

        // Se limpia los directorios temporales
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/users/profile/");

        // Información del usuario
        $arr_user = array(
            'email' => $user->getEmail(),
            'username' => $user->getUsername(),
            'name' => $user->getUserInformation()->getName(),
            'last_name' => $user->getUserInformation()->getLastName(),
            'birthdate' => $user->getUserInformation()->getBirthdate()->format('d-m-Y'),
            'country' => $user->getUserInformation()->getCountry()->getId(),
            'city' => $user->getUserInformation()->getCity(),
            'address_1' => $user->getUserInformation()->getAddress1(),
            'address_2' => $user->getUserInformation()->getAddress2(),
            'phone_1' => $user->getUserInformation()->getPhone1(),
            'phone_2' => $user->getUserInformation()->getPhone2(),
            'note' => $user->getUserInformation()->getNote(),
            'postal_code' => $user->getUserInformation()->getPostalCode(),
            'profile_image' => ($user->getProfileImage()) ? APP_IMG_VIEW_USER."/user_{$user->getCode()}/profile/{$user->getProfileImage()}" : APP_IMG_VIEW_NOIMAGE."/profile.jpg",
            'status' => $user->getUserStatus()->getId()
        );

        // Paises
        $countryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Country');
        $countries = $countryRepository->findAll();

        // Estado de los usuarios
        $userStatusRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\UserStatus');
        $status = $userStatusRepository->findAll();

        echo $this->twig->render('Admin/User/user-form.twig', array(
            'token'     => $this->createFormToken('user'),
            'id'        => $id,
            'act'       => 2,
            'user'      => $arr_user,
            'countries' => $countries,
            'status'    => $status,
            'flash'     => $this->flashMessageGlobal()
        ));
    }

    public function profileAction()
    {
        $em = $this->em;
        $user = $this->user;

        if (!$user) { $this->listAction(); exit; }

        // Se limpia los directorios temporales
        @clearDirectory(APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/users/profile/");

        // Información del usuario
        $arr_user = array(
            'email' => $user->getEmail(),
            'username' => $user->getUsername(),
            'name' => $user->getUserInformation()->getName(),
            'last_name' => $user->getUserInformation()->getLastName(),
            'birthdate' => ($user->getUserInformation()->getBirthdate()) ? $user->getUserInformation()->getBirthdate()->format('d-m-Y') : "",
            'country' => $user->getUserInformation()->getCountry()->getId(),
            'city' => $user->getUserInformation()->getCity(),
            'address_1' => $user->getUserInformation()->getAddress1(),
            'address_2' => $user->getUserInformation()->getAddress2(),
            'phone_1' => $user->getUserInformation()->getPhone1(),
            'phone_2' => $user->getUserInformation()->getPhone2(),
            'note' => $user->getUserInformation()->getNote(),
            'signature' => $user->getUserInformation()->getSignature(),
            'postal_code' => $user->getUserInformation()->getPostalCode(),
            'profile_image' => ($user->getProfileImage()) ? APP_IMG_VIEW_USER."/user_{$user->getCode()}/profile/{$user->getProfileImage()}" : APP_IMG_VIEW_NOIMAGE."/profile.jpg",
            'status' => $user->getUserStatus()->getId(),
            'role' => $user->getRole()
        );

        // Paises
        $countryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Country');
        $countries = $countryRepository->findAll();

        // Estado de los usuarios
        $userStatusRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\UserStatus');
        $status = $userStatusRepository->findAll();

        echo $this->twig->render('Admin/User/profile-form.twig', array(
            'token'     => $this->createFormToken('profile'),
            'user'      => $arr_user,
            'countries' => $countries,
            'status'    => $status,
            'flash'     => $this->flashMessageGlobal(),
            'form'      => 'frm-user'
        ));
    }
}


?>
