<?php

namespace NaS\DevaPsicolegs\Controller\Admin;

use NaS\Classes\Controller;

/**
 * Controller
 */
class PostCommentController extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de publicacioness
     *
     * @return view
     */
    public function listAction()
    {
        $em = $this->em;
        $postCommentRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostComment');
        $postComments = $postCommentRepository->findAll();

        echo $this->twig->render('Admin/Post/post-comments.twig', array(
            'comments' => $postComments,
            'flash' => $this->flashMessageGlobal()
        ));
    }

    /**
     * Editar comentario
     * @return view
     */
    public function editAction($id)
    {
        $em = $this->em;
        $postCommentRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostComment');
        $postComment = $postCommentRepository->findOneBy(['id' => $id]);
        if (!$postComment) { $this->listAction(); exit; }

        echo $this->twig->render('Admin/Post/post-comments-form.twig', array(
            'token'         => $this->createFormToken('comment'),
            'flash'         => $this->flashMessageGlobal(),
            'id'            => $id,
            'act'           => 2,
            'comment'       => $postComment,
            'form'          => 'frm-comment'
        ));
    }
}


?>
