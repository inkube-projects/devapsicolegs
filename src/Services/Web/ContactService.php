<?php

namespace NaS\DevaPsicolegs\Services\Web;

use NaS\Classes\DQLfunctions;
use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\Contact;

/**
 * Servicio para contactos
 */
class ContactService extends ServiceAware
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Agrega un mensaje de contacto
     * @return void
     */
    public function persist()
    {
        $em = $this->em;
        $this->validate();

        $reason = $this->getEntry('\NaS\DevaPsicolegs\Entity\ContactReason', array("id" => $_POST['c_reason']));
        // var_dump($reason->getId()); exit;

        $contact = new Contact;
        $contact->setNames(mb_convert_case(secure_mysql(sanitize(addslashes($_POST['c_name']))), MB_CASE_UPPER, "UTF-8"));
        $contact->setEmail(mb_convert_case(secure_mysql(sanitize($_POST['c_email'])), MB_CASE_LOWER, "UTF-8"));
        $contact->setPhone(secure_mysql($_POST['c_phone']));
        $contact->setMessage(secure_mysql(sanitize(addslashes($_POST['c_message']))));
        $contact->setContactReason($reason);
        $contact->setReaded(0);

        $em->persist($contact);
        $em->flush();

        $arr_data = [
            'names' => mb_convert_case(secure_mysql(sanitize(addslashes($_POST['c_name']))), MB_CASE_UPPER, "UTF-8"),
            'email' => mb_convert_case(secure_mysql(sanitize($_POST['c_email'])), MB_CASE_LOWER, "UTF-8"),
            'phone' => $_POST['c_phone'],
            'subject' => $reason->getName(),
            'message' => secure_mysql(sanitize($_POST['c_message']))
        ];
        $arr_to['hola@psicologiaysexologia.es'] = "Psicologia y Sexologia";
        $this->sendEmail("Web/Contact/contact-email-template.twig", $arr_data, "Nuevo contacto web", $arr_to);
    }

    /**
     * Validaciones
     * @return void
     */
    public function validate()
    {
        $arr_required = array(
            'c_name' => 'Debes ingresar al menos un nombre',
            'c_email' => 'Debes ingresar tu email',
            'c_phone' => 'Debes ingresar tu número telefónico',
            'c_reason' => 'Debes seleccionar un motivo',
            'c_message' => 'Debes ingresar tu mensaje'
        );
        requiredPost($_POST, $arr_required);

        isValidString($_POST['c_name'], "Nombres: no se permiten caracteres especiales (#%^*\|)", "#%^*\|");
        isValidEmail($_POST['c_email']);
        isValidPhoneNumber($_POST['c_phone']);
        isValidString($_POST['c_message'], "Mensaje: no se permiten caracteres especiales (#%^*\|)", "#%^*\|");

        $reason_id = @number_format($_POST['c_reason'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\ContactReason", "r", "r.id='{$reason_id}'", "La razón no es válida");
    }
}


?>
