<?php

namespace NaS\DevaPsicolegs\Services\Web;

use NaS\Classes\DQLfunctions;
use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\PostComment;

class PostService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Agrega un comentario
     * @return object
     */
    public function persist()
    {
        $em = $this->em;
        $this->validate('persist');
        $post = $this->getEntry('\NaS\DevaPsicolegs\Entity\Post', array("id" => $_POST['p']));

        $postComment = new PostComment;
        $postComment->setNames(mb_convert_case(secure_mysql(sanitize($_POST['names'])), MB_CASE_UPPER, "UTF-8"));
        $postComment->setEmail(mb_convert_case(secure_mysql(sanitize($_POST['email'])), MB_CASE_LOWER, "UTF-8"));
        $postComment->setComment(secure_mysql(sanitize($_POST['comment'])));
        $postComment->setActive(1);
        $postComment->setPost($post);

        $em->persist($postComment);
        $em->flush();
        return $postComment;
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string  $mode Modo de ejecución [persist, update]
     * @param integer $id ID de la entrada a editar
     * @return void
     */
    public function validate($mode)
    {
        $arr_required = array(
            'names' => 'Debes ingresar al menos un nombre',
            'email' => 'Debes ingresar tu email',
            'comment' => 'Debes ingresar tu comentario',
            'p' => 'La publicación no es válida'
        );
        requiredPost($_POST, $arr_required);

        $post_id = @number_format($_POST['p'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Post", "p", "p.id='{$post_id}'", "La publicación no es válida");

        isValidString($_POST['names'], "Nombres: no se permiten caracteres especiales (#%^*\|)", "#%^*\|");
        isValidString($_POST['comment'], "Comentario: no se permiten caracteres especiales (#%^*\|)", "#%^*\|");
        isValidEmail($_POST['email']);
    }
}


?>
