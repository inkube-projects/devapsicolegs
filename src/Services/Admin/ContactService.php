<?php


namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;

/**
 * Servicio para los mensajes de contacto
 */
class ContactService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Actualiza un mensaje
     * @param  integer $id ID del mensaje
     * @return object
     */
    public function update($id)
    {
        $em = $this->em;
        $this->validate('update', $id);
        $contactRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Contact');
        $contact = $contactRepository->findOneBy(['id' => $id]);

        if (isset($_POST['readed'])) {
            $contact->setReaded($_POST['readed']);
        }

        if (isset($_POST['note'])) {
            $contact->setNote(secure_mysql(sanitize($_POST['note'])));
            $this->generateFlash('edited', "El <b>Mensaje</b> ha sido editada correctamente");
        }

        $em->persist($contact);
        $em->flush();

        $this->addLogs($this->user, "Editando mensaje. ID: {$contact->getId()}");
        return $contact;
    }

    /**
     * Elimina un mensaje
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Contact", "c", "c.id='{$id}'", "El mensaje no es válido");
        $contact = $this->em->getRepository('NaS\DevaPsicolegs\Entity\Contact')->findOneBy(array('id' => $id));
        $this->em->remove($contact);
        $this->em->flush();

        $this->generateFlash('removed', "El <b>Mensaje</b> ha sido eliminado correctamente");
    }

    /**
     * Validaciones
     * @param  string   $mode  Modo de ejecución
     * @param  integer  $id   ID de la entrada
     * @return void
     */
    public function validate($mode, $id = NULL)
    {
        if ($mode == "update") {
            $id = @number_format($id,0,"","");

            $this->existRecord("NaS\DevaPsicolegs\Entity\Contact", "c", "c.id='{$id}'", "El mensaje no es válido");

            if (isset($_POST['note'])) {
                isValidString($_POST['note'], "Nota: No se permiten caracteres especiales (#$%^*\|)", "#$%^*\|");
            }

            if (isset($_POST['readed'])) {
                isValidOption(0, 1, $_POST['readed'], "La opcion (leído) no es válida");
            }
        }
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }
}


?>
