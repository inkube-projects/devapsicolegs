<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\Seo;
use NaS\DevaPsicolegs\Entity\Team;
use NaS\DevaPsicolegs\Entity\Services;
use NaS\DevaPsicolegs\Entity\SubServices;

/**
 * Seo Section Service
 */
class InformationService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Edita un SEO
     *
     * @param integer $id
     * @return object
     */
    public function persistHistory()
    {
        $em = $this->em;
        // Validación
        $this->validate('history');

        $temp_path = APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/information";
        $base_path = APP_IMG_INFORMATION;
        $arr_paths = array(
            APP_IMG_INFORMATION."/",
            $base_path."/",
            $base_path."/CK/",
            $temp_path."/",
            $temp_path."/CK/",
        );
        createDirectory($arr_paths);
        $placeHolders = array(
            $temp_path."/CK/",
            "//"
        );
        $replacements = array(
            $base_path."/CK/",
            "\/\/"
        );
        $content = str_replace($placeHolders, $replacements, $_POST['content']);

        $information = $em->getRepository('NaS\DevaPsicolegs\Entity\Information')->findOneBy(array('id' => 1));
        $information->setHistory($content);
        $em->persist($information);
        $em->flush();

        // Imagenes de CKeditor
        if (verifyPathFiles($temp_path."/CK/")) {
            createDirectory($arr_paths);
            $this->moveImages($temp_path."/CK/", $base_path."/CK/");
        }

        $this->addLogs($this->user, "Editando Informacion / Historia");
        $this->generateFlash('edited', "La <b>Hitoria</b> ha sido editada correctamente");
        return $information;
    }

    /**
     * guarda información del equipo
     *
     * @return object
     */
    public function persistTeam()
    {
        $em = $this->em;
        // Validación
        $this->validate('team');

        $temp_path = APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/information";
        $base_path = APP_IMG_INFORMATION;
        $arr_paths = array(
            APP_IMG_INFORMATION."/",
            $base_path."/",
            $base_path."/CK/",
            $base_path."/team/",
            $temp_path."/",
            $temp_path."/CK/",
            $temp_path."/team/",
        );
        createDirectory($arr_paths);
        $placeHolders = array(
            $temp_path."/CK/",
            "//"
        );
        $replacements = array(
            $base_path."/CK/",
            "\/\/"
        );
        $content = str_replace($placeHolders, $replacements, $_POST['content']);

        $information = $em->getRepository('NaS\DevaPsicolegs\Entity\Information')->findOneBy(array('id' => 1));
        $information->setTeam($content);
        $em->persist($information);

        if ($_POST['name'] != "") {
            $team = new Team;
            $team->setName(mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8"));
            $team->setDescription(\secure_mysql($_POST['description']));

            // Imagen de perfil
            if (isset($_FILES['profile_image']['tmp_name'])) {
                $this->persistImage($team, 'profile_image');
            }

            $em->persist($team);
        }

        $em->flush();

        // Imagenes de CKeditor
        if (verifyPathFiles($temp_path."/CK/")) {
            createDirectory($arr_paths);
            $this->moveImages($temp_path."/CK/", $base_path."/CK/");
        }

        $this->addLogs($this->user, "Editando Informacion / Equipo");
        $this->generateFlash('edited', "<b>Equipo</b> ha sido editado correctamente");
        return $information;
    }

    /**
     * Guarda los servicios
     * @return object
     */
    public function persistServices()
    {
        $em = $this->em;
        // Validación
        $this->validate('service');

        $temp_path = APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/information";
        $base_path = APP_IMG_INFORMATION;
        $arr_paths = array(
            APP_IMG_INFORMATION."/",
            $base_path."/",
            $base_path."/CK/",
            $temp_path."/",
            $temp_path."/CK/",
        );
        createDirectory($arr_paths);
        $placeHolders = array(
            $temp_path."/CK/",
            "//"
        );
        $replacements = array(
            $base_path."/CK/",
            "\/\/"
        );
        $content = str_replace($placeHolders, $replacements, $_POST['content']);

        $information = $em->getRepository('NaS\DevaPsicolegs\Entity\Information')->findOneBy(array('id' => 1));
        $information->setServices($content);
        $em->persist($information);

        if ($_POST['name'] != "") {
            $services = new Services;
            $services->setName(mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8"));
            $services->setDescription(\secure_mysql($_POST['description']));

            $em->persist($services);
        }

        $em->flush();

        // Imagenes de CKeditor
        if (verifyPathFiles($temp_path."/CK/")) {
            createDirectory($arr_paths);
            $this->moveImages($temp_path."/CK/", $base_path."/CK/");
        }

        $this->addLogs($this->user, "Editando Informacion / Servicios");
        $this->generateFlash('edited', "<b>Servicios</b> ha sido editado correctamente");
        return $information;
    }

    /**
     * Guarda imagen del equipo
     * @param  \NaS\DevaPsicolegs\Entity\Team  $team      Objeto del Team
     * @param  string                          $parameter Nombre del parametro
     * @return void
     */
    public function persistImage(\NaS\DevaPsicolegs\Entity\Team $team, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_IMG_INFORMATION."/team/";

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "team_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($team->getProfileImage()) {
                        @unlink($path.$team->getProfileImage());
                    }

                    $team->setProfileImage($file_name);
                }
            }
        }
    }

    /**
     * Crea la imagen temporal del CKeditor
     * @return void
     */
    public function uploadCKeditorImage()
    {
        $user_code = $this->user->getCode();
        $dir_temp = APP_IMG_USER."/user_".$user_code."/temp_files/information/CK";

        $arr_paths = array(
            APP_IMG_USER."/user_".$user_code."/temp_files/information/",
            APP_IMG_USER."/user_".$user_code."/temp_files/information/CK"
        );
        createDirectory($arr_paths);
        $imgage_name = $this->prepareCoverTemp($dir_temp, "upload", "ck".uniqid());
        return BASE_URL."/".$dir_temp."/".$imgage_name;
    }

    /**
     * Edita un miembro del equipo
     *
     * @param integer $id ID del mimebro del equpo
     * @return object
     */
    public function updateMember($id)
    {
        $em = $this->em;
        // Validación
        $this->validate('member');

        $teamRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Team');
        $team = $teamRepository->findOneBy(['id' => $id]);
        $team->setName(secure_mysql(mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8")));
        $team->setDescription(\secure_mysql($_POST['description']));
        $team->setShortDescription(\secure_mysql($_POST['short_description']));
        $team->setExperience(\secure_mysql($_POST['experience']));
        $team->setPhone(\secure_mysql($_POST['phone']));


        // Imagen de perfil
        if (isset($_FILES['profile_image']['tmp_name'])) {
            $this->persistImage($team, 'profile_image');
        }

        $em->persist($team);
        $em->flush();
        $this->addLogs($this->user, "Editando miembro. ID:{$id} - nombre: {$_POST['name']}");
        $this->generateFlash('edited', "El <b>miembro</b> ha sido editado correctamente");
        return $team;
    }

    public function updateService($id)
    {
        $em = $this->em;
        // Validación
        $this->validate('service');

        $serviceRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Services');
        $service = $serviceRepository->findOneBy(['id' => $id]);
        $service->setName(secure_mysql(mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8")));
        $service->setDescription(\sanitize(\secure_mysql($_POST['description'])));

        $em->persist($service);
        $em->flush();
        $this->addLogs($this->user, "Editando servicio. ID:{$id} - nombre: {$_POST['name']}");
        $this->generateFlash('edited', "El <b>servicio</b> ha sido editado correctamente");
        return $service;
    }

    /**
     * Elimina un mimebro de equipo
     * @return void
     */
    public function removeMember()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Team", "t", "t.id='{$id}'", "El miembro del equipo no es válido");
        $team = $this->em->getRepository('NaS\DevaPsicolegs\Entity\Team')->findOneBy(array('id' => $id));

        $base_path = APP_IMG_INFORMATION."/team/";

        @unlink($base_path.$team->getProfileImage());
        @unlink($base_path."min_{$team->getProfileImage()}");

        $this->em->remove($team);
        $this->em->flush();

        $this->generateFlash('removed', "El <b>Miembro</b> ha sido eliminado correctamente");
    }

    /**
     * Elimina un servicio
     * @return void
     */
    public function removeService()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Services", "s", "s.id='{$id}'", "El servicio no es válido");
        $services = $this->em->getRepository('NaS\DevaPsicolegs\Entity\Services')->findOneBy(array('id' => $id));

        $this->em->remove($services);
        $this->em->flush();

        $this->generateFlash('removed', "El <b>Servicio</b> ha sido eliminado correctamente");
    }

    /**
     * Guarda un subservicio
     *
     * @return object
     */
    public function persistSubservice()
    {
        $em = $this->em;
        // Validación
        $this->validate('subservice');
        $temp_path = APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/information";
        $base_path = APP_IMG_INFORMATION;
        $arr_paths = array(
            APP_IMG_INFORMATION."/",
            $base_path."/",
            $base_path."/CK/",
            $temp_path."/",
            $temp_path."/CK/",
        );
        createDirectory($arr_paths);
        $placeHolders = array(
            $temp_path."/CK/",
            "//"
        );
        $replacements = array(
            $base_path."/CK/",
            "\/\/"
        );
        $description = str_replace($placeHolders, $replacements, $_POST['description']);
        $service = $this->getEntry('\NaS\DevaPsicolegs\Entity\Services', array("id" => $_POST['service']));

        $subServices = new SubServices;
        $subServices->setName(secure_mysql(mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8")));
        $subServices->setDescription(secure_mysql($description));
        $subServices->setServices($service);

        $em->persist($subServices);
        $em->flush();

        // Imagenes de CKeditor
        if (verifyPathFiles($temp_path."/CK/")) {
            createDirectory($arr_paths);
            $this->moveImages($temp_path."/CK/", $base_path."/CK/");
        }
        $this->addLogs($this->user, "Agregando sub-servicio. ID:{$subServices->getId()} - nombre: {$_POST['name']}");
        $this->generateFlash('added', "El <b>subservicio</b> ha sido agregado correctamente");
        return $subServices;
    }

    /**
     * Editando sub servicio
     *
     * @param integer $id
     * @return void
     */
    public function updateSubService($id)
    {
        $em = $this->em;
        // Validación
        $this->validate('subservice', $id);
        $temp_path = APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/information";
        $base_path = APP_IMG_INFORMATION;
        $arr_paths = array(
            APP_IMG_INFORMATION."/",
            $base_path."/",
            $base_path."/CK/",
            $temp_path."/",
            $temp_path."/CK/",
        );
        createDirectory($arr_paths);
        $placeHolders = array(
            $temp_path."/CK/",
            "//"
        );
        $replacements = array(
            $base_path."/CK/",
            "\/\/"
        );
        $description = str_replace($placeHolders, $replacements, $_POST['description']);
        $service = $this->getEntry('\NaS\DevaPsicolegs\Entity\Services', array("id" => $_POST['service']));

        $subServicesRepository = $em->getRepository('\NaS\DevaPsicolegs\Entity\SubServices');
        $subServices = $subServicesRepository->findOneBy(['id' => $id]);
        $subServices->setName(secure_mysql(mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8")));
        $subServices->setDescription(secure_mysql($description));
        $subServices->setServices($service);

        $em->persist($subServices);
        $em->flush();

        // Imagenes de CKeditor
        if (verifyPathFiles($temp_path."/CK/")) {
            createDirectory($arr_paths);
            $this->moveImages($temp_path."/CK/", $base_path."/CK/");
        }
        $this->addLogs($this->user, "Editando sub-servicio. ID:{$subServices->getId()} - nombre: {$_POST['name']}");
        $this->generateFlash('edited', "El <b>subservicio</b> ha sido agregado correctamente");
        return $subServices;
    }

    /**
     * Elimina un subservicio
     *
     * @param integer $id
     * @return void
     */
    public function removeSubService()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\SubServices", "s", "s.id='{$id}'", "El sub-servicio no es válido");
        $subservices = $this->em->getRepository('NaS\DevaPsicolegs\Entity\SubServices')->findOneBy(array('id' => $id));

        $this->em->remove($subservices);
        $this->em->flush();

        $this->generateFlash('removed', "El <b>Sub-servicio</b> ha sido eliminado correctamente");
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string $act persist, update
     * @return void
     */
    public function validate($act, $id = NULL)
    {
        if ($act == "member" || $act == "service" || $act == "subservice") {
            $arr_required = array('name' => 'Debes ingresar el nombre');
            requiredPost($_POST, $arr_required);

            isValidString($_POST['name'], "Nombre: No se permiten caracteres especiales (#$%^*\|)", "#$%^*\|");
            isValidString($_POST['description'], "Descripción: No se permiten caracteres especiales (#$%^*\|)", "#$%^*\|");
            if (isset($_POST['phone'])) {
                isValidPhoneNumber($_POST['phone']);
            }

            if (isset($_FILES["profile_image"]['tmp_name'])) {
                if ($_FILES["profile_image"]['tmp_name'] != "") {
                    isValidImage('profile_image', 80, 80);
                }
            }

            if ($act == "subservice") {
                $arr_required = array('service' => 'Debes seleccionar un servicio');
                requiredPost($_POST, $arr_required);
                $service_id = @number_format($_POST['service'],0,"","");
                $this->existRecord("NaS\DevaPsicolegs\Entity\Services", "s", "s.id='{$service_id}'", "El servicio no es válido");
                if ($id) {
                    $this->existRecord("NaS\DevaPsicolegs\Entity\SubServices", "s", "s.id='{$id}'", "El sub-servicio no es válido");
                }
            }
        } else {
            $arr_required = array('content' => 'Debes ingresar el contenido');

            if ($act == "team") {
                isValidString($_POST['name'], "Nombre: No se permiten caracteres especiales (#$%^*\|)", "#$%^*\|");
                isValidString($_POST['description'], "Descripción: No se permiten caracteres especiales (#$%^*\|)", "#$%^*\|");
                if ($_FILES["profile_image"]['tmp_name'] != "") {
                    isValidImage('profile_image', 80, 80);
                }
            }
            // isValidString($_POST['content'], "Contenido: No se permiten caracteres especiales (#$%^*\|)", "#$%^*\|");
            requiredPost($_POST, $arr_required);
        }
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }
}


?>
