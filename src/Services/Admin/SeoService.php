<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\Seo;

/**
 * Seo Section Service
 */
class SeoService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Guarda un SEO
     *
     * @return object
     */
    public function persist()
    {
        $em = $this->em;
        // Validación
        $this->validate('persist');

        $seo = new Seo;
        $seoSection = $this->getEntry('NaS\DevaPsicolegs\Entity\SeoSection', ['id' => $_POST['section']]);
        $seo->setTitle(secure_mysql($_POST['title']));
        $seo->setAuthor(secure_mysql($_POST['author']));
        $seo->setKeywords(secure_mysql($_POST['keywords']));
        $seo->setDescription(secure_mysql($_POST['description']));
        $seo->setSeoSection($seoSection);
        
        $em->persist($seo);
        $em->flush();

        $this->addLogs($this->user, "Creando SEO. ID: {$seoSection->getId()}, Nombre: {$seo->getTitle()}");
        $this->generateFlash('added', "El <b>SEO</b> ha sido creado correctamente");
        return $seo;
    }

    /**
     * Edita un SEO
     *
     * @param integer $id
     * @return object
     */
    public function update($id)
    {
        $em = $this->em;
        // Validación
        $this->validate('update', $id);

        $seo = $em->getRepository('NaS\DevaPsicolegs\Entity\Seo')->findOneBy(array('id' => $id));
        $seoSection = $this->getEntry('NaS\DevaPsicolegs\Entity\SeoSection', ['id' => $_POST['section']]);
        $seo->setTitle(secure_mysql($_POST['title']));
        $seo->setAuthor(secure_mysql($_POST['author']));
        $seo->setKeywords(secure_mysql($_POST['keywords']));
        $seo->setDescription(secure_mysql($_POST['description']));
        $seo->setSeoSection($seoSection);
        
        $em->persist($seo);
        $em->flush();

        $this->addLogs($this->user, "Editando SEO. ID: {$seoSection->getId()}, Nombre: {$seo->getTitle()}");
        $this->generateFlash('edited', "El <b>SEO</b> ha sido creado correctamente");
        return $seo;
    }

    /**
     * Elimina una sección
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Seo", "s", "s.id='{$id}'", "El SEO no es válido");
        $seo = $this->em->getRepository('NaS\DevaPsicolegs\Entity\Seo')->findOneBy(array('id' => $id));
        $this->em->remove($seo);
        $this->em->flush();

        $this->generateFlash('removed', "El <b>SEO</b> ha sido eliminado correctamente");
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string $act persist, update
     * @return void
     */
    public function validate($act, $id = NULL)
    {
        $arr_required = array(
            'title' => 'Debes ingresar el título del SEO',
            'description' => 'Debes ingresar la descripción',
            'keywords' => 'Debes ingresar al menos una palabra clave'

        );
        requiredPost($_POST, $arr_required);

        if ($act == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\Seo", "s", "s.id='{$id}'", "El SEO no es válido");
        }
        $section_id = @number_format($_POST['section'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\SeoSection", "ss", "ss.id='{$section_id}'", "La sección no es válida");
        isValidString($_POST['title'], "Título: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['author'], "Autor: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['description'], "Descripción: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['keywords'], "Palabras Clave: No se permiten caracteres especiales", "#$%^*\|");
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }
}


?>
