<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\JWThandler;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\User;
use NaS\DevaPsicolegs\Entity\UserInformation;

/**
 * Servicio para los procesos de usuarios
 */
class UsersService extends ServiceAware
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Se guarda un nuevo usuario
     *
     * @return object
     */
    public function persist()
    {
        $em = $this->em;

        // Validaciones
        $this->validate('persist');

        // Se guarda la entrada
        $user = new User;
        $userInformation = new UserInformation;
        $role = $this->getEntry('\NaS\DevaPsicolegs\Entity\Role', array("role" => "ROLE_USER"));
        $userStatus = $this->getEntry('\NaS\DevaPsicolegs\Entity\UserStatus', array("id" => $_POST['status']));
        $country = $this->getEntry('\NaS\DevaPsicolegs\Entity\Country', array("id" => $_POST['country']));

        // User
        $user->setUsername(mb_convert_case(secure_mysql($_POST['username']), MB_CASE_UPPER, "UTF-8"));
        $user->setEmail(mb_convert_case(secure_mysql($_POST['email']), MB_CASE_LOWER, "UTF-8"));
        $user->setPassword(hashPass($_POST['pass'], 12));
        $user->setUsernameCanonical(mb_convert_case(secure_mysql($_POST['username']), MB_CASE_UPPER, "UTF-8"));
        $user->setEmailCanonical(mb_convert_case(secure_mysql($_POST['email']), MB_CASE_LOWER, "UTF-8"));
        $user->setRole($role);
        $user->setUserStatus($userStatus);

        // User Information
        $user->setUserInformation($userInformation);
        $user->getUserInformation()->setName(secure_mysql($_POST['name']));
        $user->getUserInformation()->setLastName(secure_mysql($_POST['last_name']));
        $user->getUserInformation()->setBirthdate(new \DateTime(to_date($_POST['birthdate'])));
        $user->getUserInformation()->setCity(secure_mysql($_POST['city']));
        $user->getUserInformation()->setPhone1(secure_mysql($_POST['phone_1']));
        $user->getUserInformation()->setPhone2(secure_mysql($_POST['phone_2']));
        $user->getUserInformation()->setNote(secure_mysql($_POST['note']));
        $user->getUserInformation()->setPostalCode(secure_mysql($_POST['postal_code']));
        $user->getUserInformation()->setCountry($country);
        $user->getUserInformation()->setAddress1(secure_mysql($_POST['address_1']));
        $user->getUserInformation()->setAddress2(secure_mysql($_POST['address_2']));

        $em->persist($user);
        $em->flush();

        // Codigo
        $code = genPass($user->getId(), 5);
        $user->setCode($code);

        // Se crean las carpetas
        mkdir(APP_IMG_USER."/user_{$code}/");
        // Temporales de ususarios
        mkdir(APP_IMG_USER."/user_{$code}/temp_files/");
        mkdir(APP_IMG_USER."/user_{$code}/temp_files/users/");
        mkdir(APP_IMG_USER."/user_{$code}/temp_files/users/profile/");
        // Temporales de publicaciones
        mkdir(APP_IMG_USER."/user_{$code}/temp_files/posts/");
        mkdir(APP_IMG_USER."/user_{$code}/temp_files/posts/cover/");
        mkdir(APP_IMG_USER."/user_{$code}/temp_files/posts/gallery/");
        // Imagen de perfil
        mkdir(APP_IMG_USER."/user_{$code}/profile/");

        // Se guarda la imagen de perfil
        if (isset($_POST['hid-name'])) {
            if ($_POST['hid-name'] != "") {
                $this->saveImage(
                    $_POST['hid-name'],
                    "",
                    APP_IMG_USER."/user_{$this->user->getCode()}/temp_files/users/profile/",
                    APP_IMG_USER."/user_{$code}/profile/"
                );
                $user->setProfileImage(secure_mysql($_POST['hid-name']));
            }
        }

        $em->persist($user);
        $em->flush();

        $this->generateFlash('added', "El <b>Usuario</b> ha sido creado correctamente");
        return $user;
    }

    /**
     * Edita un usuario
     * @param  integer $id ID del ususario
     * @return object
     */
    public function update($id)
    {
        $em = $this->em;

        // Validaciones
        $this->validate('update', $id);

        // Se guarda la entrada
        $user = $em->getRepository('NaS\DevaPsicolegs\Entity\User')->findOneBy(array('id' => $id));
        $userStatus = $this->getEntry('\NaS\DevaPsicolegs\Entity\UserStatus', array("id" => $_POST['status']));
        $country = $this->getEntry('\NaS\DevaPsicolegs\Entity\Country', array("id" => $_POST['country']));

        // User
        $user->setUsername(mb_convert_case(secure_mysql($_POST['username']), MB_CASE_UPPER, "UTF-8"));
        $user->setEmail(mb_convert_case(secure_mysql($_POST['email']), MB_CASE_LOWER, "UTF-8"));
        $user->setUsernameCanonical(mb_convert_case(secure_mysql($_POST['username']), MB_CASE_UPPER, "UTF-8"));
        $user->setEmailCanonical(mb_convert_case(secure_mysql($_POST['email']), MB_CASE_LOWER, "UTF-8"));
        $user->setUserStatus($userStatus);

        // Si la contraseña no viene vacía se edita
        if (!empty($_POST['pass'])) {
            $user->setPassword(hashPass($_POST['pass'], 12));
        }

        // User Information
        $user->getUserInformation()->setName(mb_convert_case(secure_mysql($_POST['name']), MB_CASE_UPPER, "UTF-8"));
        $user->getUserInformation()->setLastName(mb_convert_case(secure_mysql($_POST['last_name']), MB_CASE_UPPER, "UTF-8"));
        $user->getUserInformation()->setBirthdate(new \DateTime(to_date($_POST['birthdate'])));
        $user->getUserInformation()->setCity(secure_mysql($_POST['city']));
        $user->getUserInformation()->setPhone1(secure_mysql($_POST['phone_1']));
        $user->getUserInformation()->setPhone2(secure_mysql($_POST['phone_2']));
        $user->getUserInformation()->setNote(secure_mysql($_POST['note']));
        $user->getUserInformation()->setPostalCode(secure_mysql($_POST['postal_code']));
        $user->getUserInformation()->setCountry($country);
        $user->getUserInformation()->setAddress1(secure_mysql($_POST['address_1']));
        $user->getUserInformation()->setAddress2(secure_mysql($_POST['address_2']));

        if (isset($_POST['signature'])) {
            $user->getUserInformation()->setSignature(secure_mysql(sanitize($_POST['signature'])));
        }

        $em->persist($user);
        $em->flush();

        // Codigo
        $code = $user->getCode();

        // Se crean las carpetas
        if (!directoryExist(APP_IMG_USER."/user_{$code}/")) { mkdir(APP_IMG_USER."/user_{$code}/"); }
        // Temporales de ususarios
        if (!directoryExist(APP_IMG_USER."/user_{$code}/temp_files/")) { mkdir(APP_IMG_USER."/user_{$code}/temp_files/"); }
        if (!directoryExist(APP_IMG_USER."/user_{$code}/temp_files/users/")) { mkdir(APP_IMG_USER."/user_{$code}/temp_files/users/"); }
        if (!directoryExist(APP_IMG_USER."/user_{$code}/temp_files/users/profile/")) { mkdir(APP_IMG_USER."/user_{$code}/temp_files/users/profile/"); }

        // Temporales de publicaciones
        if (!directoryExist(APP_IMG_USER."/user_{$code}/temp_files/posts/")) { mkdir(APP_IMG_USER."/user_{$code}/temp_files/posts/"); }
        if (!directoryExist(APP_IMG_USER."/user_{$code}/temp_files/posts/cover/")) { mkdir(APP_IMG_USER."/user_{$code}/temp_files/posts/cover/"); }
        if (!directoryExist(APP_IMG_USER."/user_{$code}/temp_files/posts/gallery/")) { mkdir(APP_IMG_USER."/user_{$code}/temp_files/posts/gallery/"); }

        // Imagen de perfil
        if (!directoryExist(APP_IMG_USER."/user_{$code}/profile/")) { mkdir(APP_IMG_USER."/user_{$code}/profile/"); }

        // Se guarda la imagen de perfil
        if (isset($_POST['hid-name'])) {
            if ($_POST['hid-name'] != "") {
                $this->saveImage(
                    $_POST['hid-name'],
                    $user->getProfileImage(),
                    APP_IMG_USER."/user_{$this->user->getCode()}/temp_files/users/profile/",
                    APP_IMG_USER."/user_{$code}/profile/"
                );
                $user->setProfileImage(secure_mysql($_POST['hid-name']));
            }
        }

        $em->persist($user);
        $em->flush();

        $this->generateFlash('edited', "El <b>Usuario</b> ha sido editado correctamente");
        return $user;
    }

    /**
     * Edita el perfil del ususario
     *
     * @return void
     */
    public function profile()
    {
        $user_id = $this->user->getId();
        $user = $this->update($user_id);

        $_SESSION['SES_ADMIN_DEVAPSICOLEGS']['token'] = $user->getSessionToken();
        $_SESSION['SES_ADMIN_DEVAPSICOLEGS']['id'] = $user->getId();
        $_SESSION['SES_ADMIN_DEVAPSICOLEGS']['username'] = $user->getUsername();
    }

    /**
     * Elimina un uusuario
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\User", "user", "user.id='{$id}'", "El usuario no es válido");
        $user = $this->em->getRepository('NaS\DevaPsicolegs\Entity\User')->findOneBy(array('id' => $id));
        $code = $user->getCode();
        $this->em->remove($user);
        $this->em->flush();

        removeDir(APP_IMG_USER."/user_{$code}/");
        $this->generateFlash('removed', "El <b>Usuario</b> ha sido eliminado correctamente");
    }

    /**
     * Prepara la imagen de perfil
     * @param  integer $act Acción
     * @return string
     */
    public function uploadProfileImage($act)
    {
        $user_code = $this->user->getCode();
        $dir_temp = APP_IMG_USER."/user_".$user_code."/temp_files/users/profile";
        if ($act == 1) {
            isValidImage('profile_image', 150, 150);
            $tempIMG = $this->prepareCoverTemp($dir_temp, 'profile_image');
            return $this->showFormCoverIMG($tempIMG, $dir_temp, '/inkcms/ajax/user/proccessImage', 'frm-user', $user_code);
        } else {
            $arr_required = array(
                'x' => 'Eje X no se ha enviado',
                'y' => 'Eje Y no se ha enviado',
                'w' => 'El ancho no se ha enviado',
                'h' => 'La altura no se ha enviado',
            );
            requiredPost($_POST, $arr_required);
            return $this->createIMG($dir_temp."/", $user_code);
        }
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string $act persist, update
     * @return void
     */
    public function validate($act, $id = NULL)
    {
        $arr_required = array(
            'email' => 'Debes ingresar un E-mail',
            'username' => 'Debes ingresar un NOmbre de Usuario',
            'name' => 'Debes ingresar al menos un nombre',
            'last_name' => 'Debes ingresar al menos un apellido',
            'country' => 'Debes seleccionar un país'
        );

        if ($act == "persist") {
            $arr_required['pass'] = 'Debes ingresar una contraseña';
        }

        $country_id = @number_format($_POST['country'],0,"","");
        $status_id = @number_format($_POST['status'],0,"","");

        requiredPost($_POST, $arr_required);
        isValidEmail($_POST['email']);
        isValidString($_POST['username'], "Nombre de ususario: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['name'], "Nombre: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['last_name'], "Nombre: No se permiten caracteres especiales", "#$%^*\|");
        isValidDate($_POST['birthdate'], "es", "-");
        isValidString($_POST['city'], "Ciudad: No se permiten caracteres especiales", "#$%^*\|");
        isValidPhoneNumber($_POST['phone_1']);
        isValidPhoneNumber($_POST['phone_2']);
        isValidString($_POST['note'], "Nota: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['address_1'], "Dirección: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['address_2'], "Dirección 2: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['postal_code'], "Código postal: No se permiten caracteres especiales", "#$%^*\|");
        
        $this->existRecord("NaS\DevaPsicolegs\Entity\UserStatus", "us", "us.id='{$status_id}'", "El estado no es válido");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Country", "c", "c.id='{$country_id}'", "El país no es válido");

        if (isset($_POST['signature'])) {
            isValidString($_POST['signature'], "Firma: No se permiten caracteres especiales", "#$%^*\|");
        }

        if ($act == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\User", "user", "user.id='{$id}'", "El usuario no es válido");
            $this->duplicatedRecord("NaS\DevaPsicolegs\Entity\User", "user", "user.email='{$_POST['email']}' AND NOT user.id='{$id}'", "El email ya se encuentra en uso");
            $this->duplicatedRecord("NaS\DevaPsicolegs\Entity\User", "user", "user.username='{$_POST['username']}' AND NOT user.id='{$id}'", "El nombre de usuario ya se encuentra en uso");
        } else {
            $this->duplicatedRecord("NaS\DevaPsicolegs\Entity\User", "user", "user.email='{$_POST['email']}'", "El email ya se encuentra en uso");
            $this->duplicatedRecord("NaS\DevaPsicolegs\Entity\User", "user", "user.username='{$_POST['username']}'", "El nombre de usuario ya se encuentra en uso");
        }
    }
}


?>
