<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\Tags;

class TagService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Guarda una etiqueta
     *
     * @return object
     */
    public function persist()
    {
        $em = $this->em;
        // Validación
        $this->validate('persist');

        $tags = new Tags;
        $tags->setName($_POST['name']);
        $em->persist($tags);
        $em->flush();

        $this->addLogs($this->user, "Creando etiqueta. ID: {$tags->getId()}, Nombre: {$tags->getName()}");
        $this->generateFlash('added', "La <b>Etiqueta</b> ha sido creada correctamente");
        return $tags;
    }

    /**
     * Edita una etiqueta
     *
     * @param integer $id
     * @return object
     */
    public function update($id)
    {
        $em = $this->em;
        // Validación
        $this->validate('update');

        $tags = $em->getRepository('NaS\DevaPsicolegs\Entity\Tags')->findOneBy(array('id' => $id));
        $tags->setName($_POST['name']);
        $em->persist($tags);
        $em->flush();

        $this->addLogs($this->user, "Creando etiqueta. ID: {$tags->getId()}, Nombre: {$tags->getName()}");
        $this->generateFlash('edited', "La <b>Etiqueta</b> ha sido editada correctamente");
        return $tags;
    }

    /**
     * Elimina una etiqueta
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Tags", "t", "t.id='{$id}'", "La etiqueta no es válida");
        $tag = $this->em->getRepository('NaS\DevaPsicolegs\Entity\Tags')->findOneBy(array('id' => $id));
        $this->em->remove($tag);
        $this->em->flush();

        $this->generateFlash('removed', "La <b>Etiqueta</b> ha sido eliminada correctamente");
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string $act persist, update
     * @return void
     */
    public function validate($act, $id = NULL)
    {
        $arr_required = array(
            'name' => 'Debes ingresar el nombre de la etiqueta'
        );
        requiredPost($_POST, $arr_required);

        if ($act == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\Tags", "t", "t.id='{$id}'", "La etiqueta no es válida");
        }

        isValidString($_POST['name'], "Nombre: No se permiten caracteres especiales", "#$%^*\|");
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }
}


?>
