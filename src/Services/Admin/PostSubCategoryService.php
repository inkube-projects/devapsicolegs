<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\PostSubCategory;

/**
 * Servicio para las subcategorías
 */
class PostSubCategoryService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Guarda una nueva entrada
     * @return object
     */
    public function persist()
    {
        $em = $this->em;
        // Validaciones
        $this->validate('persist');

        // Guardado
        $postSubCategory = new PostSubCategory;
        $category = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostCategory', array("id" => $_POST['category']));

        $postSubCategory->setName($_POST['name']);
        $postSubCategory->setDescription($_POST['description']);
        $postSubCategory->setPostCategory($category);
        $postSubCategory->setCanonicalName(friendlyURL($_POST['name'], "lower"));
        $em->persist($postSubCategory);
        $em->flush();

        $this->addLogs($this->user, "Creando sub-categoría. ID: {$postSubCategory->getId()}, Nombre: {$postSubCategory->getName()}");
        $this->generateFlash('added', "La <b>Sub-categoría</b> ha sido creada correctamente");
        return $postSubCategory;
    }

    /**
     * Edita una entrada
     *
     * @param integer $id
     * @return object
     */
    public function update($id)
    {
        $em = $this->em;
        // Validaciones
        $this->validate('update', $id);
        $postSubCategory = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory')->findOneBy(array('id' => $id));
        $category = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostCategory', array("id" => $_POST['category']));

        $postSubCategory->setName($_POST['name']);
        $postSubCategory->setDescription($_POST['description']);
        $postSubCategory->setPostCategory($category);
        $postSubCategory->setCanonicalName(friendlyURL($_POST['name'], "lower"));
        $em->persist($postSubCategory);
        $em->flush();

        $this->addLogs($this->user, "Creando sub-categoría. ID: {$postSubCategory->getId()}, Nombre: {$postSubCategory->getName()}");
        $this->generateFlash('edited', "La <b>Sub-categoría</b> ha sido creada correctamente");
        return $postSubCategory;
    }

    /**
     * Elimina una categoría
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\PostSubCategory", "psc", "psc.id='{$id}'", "La subcategoría no es válida");
        $postSubCategory = $this->em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory')->findOneBy(array('id' => $id));
        $this->em->remove($postSubCategory);
        $this->em->flush();

        $this->generateFlash('removed', "La <b>Sub-categoría</b> ha sido eliminada correctamente");
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string $act persist, update
     * @return void
     */
    public function validate($act, $id = NULL)
    {
        $arr_required = array(
            'name' => 'Debes ingresar el nombre de la categoría',
            'category' => 'debes seleccionar una categoría'
        );
        requiredPost($_POST, $arr_required);

        if ($act == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\PostSubCategory", "psc", "psc.id='{$id}'", "La subcategoría no es válida");
        }

        $category = @number_format($_POST['category'],0,"","");
        isValidString($_POST['name'], "Nombre: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['description'], "Descripción: No se permiten caracteres especiales", "#$%^*\|");
        $this->existRecord("NaS\DevaPsicolegs\Entity\PostCategory", "pc", "pc.id='{$category}'", "La categoría no es válida");
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }
}


?>
