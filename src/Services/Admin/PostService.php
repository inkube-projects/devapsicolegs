<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\TwigConfigLoader;
use NaS\Classes\DQLfunctions;
use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\Post;
use NaS\DevaPsicolegs\Entity\PostGalleryImage;
use NaS\DevaPsicolegs\Entity\PostSeo;


/**
 * Post service
 */
class PostService extends ServiceAware
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Guarda una publicación
     *
     * @return object
     */
    public function persist()
    {
        $em = $this->em;
        $this->validate('persist');
        $temp_path = APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts";
        $base_path = APP_IMG_POST."/".date('m_Y');
        $arr_paths = array(
            $base_path."/",
            $base_path."/CK/",
            $base_path."/gallery/",
            $base_path."/cover/",
            $base_path."/banner/"
        );
        createDirectory($arr_paths);
        $placeHolders = array(
            $temp_path."/CK/",
            "//"
        );
        $replacements = array(
            $base_path."/CK/",
            "\/\/"
        );
        $content = str_replace($placeHolders, $replacements, $_POST['content']);
        $video = str_replace($placeHolders, $replacements, $_POST['video']);
        $post = new Post;

        $post->setTitle(secure_mysql($_POST['title']));
        $post->setContent(secure_mysql($content));
        $post->setCode(date('m_Y'));
        $post->setVideo($video);
        $post->setHeadline((isset($_POST['headline'])) ? $_POST['headline'] : "0");
        $post->setActive((isset($_POST['active'])) ? $_POST['active'] : "0");
        $post->setDraft((isset($_POST['draft'])) ? $_POST['draft'] : "0");
        $post->setUser($this->getEntry('\NaS\DevaPsicolegs\Entity\User', array("id" => $this->user->getId())));
        // categorías
        foreach ($_POST['category'] as $key => $cat) {
            $e_cat = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostCategory', array("id" => $cat));
            $post->addPostCategory($e_cat);
        }
        // Sub-categorías
        if (isset($_POST['subcategory'])) {
            foreach ($_POST['subcategory'] as $key => $sub) {
                $e_sub = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostSubCategory', array("id" => $sub));
                $post->addPostSubCategory($e_sub);
            }
        }
        // Etiquetas
        if (isset($_POST['tag'])) {
            foreach ($_POST['tag'] as $key => $tag) {
                $e_tag = $this->getEntry('\NaS\DevaPsicolegs\Entity\Tags', array("id" => $tag));
                $post->addTag($e_tag);
            }
        }

        // SEO
        $postSeo = new PostSeo;
        $seo_title = (!empty($_POST['seo_title'])) ? $_POST['seo_title'] : $_POST['title'];
        $seo_url = (!empty($_POST['seo_url'])) ? friendlyURL($_POST['seo_url']) : friendlyURL($_POST['title']);
        $seo_author = (!empty($_POST['seo_author'])) ? secure_mysql($_POST['seo_author']) : $this->user->getUserInformation()->getName()." ".$this->user->getUserInformation()->getLastName();

        $post->setPostSeo($postSeo);
        $post->getPostSeo()->setTitle(secure_mysql($seo_title));
        $post->getPostSeo()->setAuthor($seo_author);
        $post->getPostSeo()->setUrl(secure_mysql($this->getSeoURL($seo_url)));
        $post->getPostSeo()->setKeywords(secure_mysql($_POST['seo_keywords']));
        $post->getPostSeo()->setDescription(secure_mysql($_POST['seo_description']));

        // Imagen de portada
        if (isset($_POST['hid-name'])) {
            if ($_POST['hid-name'] != "") {
                $this->saveImage(
                    $_POST['hid-name'],
                    "",
                    APP_IMG_USER."/user_{$this->user->getCode()}/temp_files/posts/cover/",
                    $base_path."/cover/"
                );
                $post->setCoverImage(secure_mysql($_POST['hid-name']));
            }
        }

        // Imágenes de galería
        if (verifyPathFiles($temp_path."/gallery/")) {
            $images = $this->moveImages($temp_path."/gallery/", $base_path."/gallery/");
            foreach ($images as $key => $img) {
                $postGalleryImage = new PostGalleryImage;
                $post->addPostGalleryImage($postGalleryImage);
                $post->getPostGalleryImage()[$key]->setImageName($img);
            }
        }

        // Imagenes de CKeditor
        if (verifyPathFiles($temp_path."/CK/")) {
            $this->moveImages($temp_path."/CK/", $base_path."/CK/");
        }

        // Banner
        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($post, 'banner');
        }

        $em->persist($post);
        $em->flush();

        $this->addLogs($this->user, "Creando publicación. ID: {$post->getId()}, Nombre: {$post->getTitle()}");
        $this->generateFlash('added', "La <b>Publicación</b> ha sido creada correctamente");
        return $post;
    }

    /**
     * Actualiza una publicación
     *
     * @param integer $id ID de la publicación
     * @return void
     */
    public function update($id)
    {
        $em = $this->em;
        $conn = $em->getConnection();
        $this->validate('update', $id);
        $post = $this->getEntry('\NaS\DevaPsicolegs\Entity\Post', array("id" => $id));

        $temp_path = APP_IMG_USER."/user_".$this->user->getCode()."/temp_files/posts";
        $base_path = APP_IMG_POST."/".$post->getCode();
        $arr_paths = array(
            $base_path."/",
            $base_path."/CK/",
            $base_path."/gallery/",
            $base_path."/cover/",
            $base_path."/banner/"
        );
        createDirectory($arr_paths);
        $placeHolders = array(
            $temp_path."/CK/",
            "//"
        );
        $replacements = array(
            $base_path."/CK/",
            "\/\/"
        );
        $content = str_replace($placeHolders, $replacements, $_POST['content']);
        $video = str_replace($placeHolders, $replacements, $_POST['video']);

        $post->setTitle(secure_mysql($_POST['title']));
        $post->setContent(secure_mysql($content));
        $post->setVideo(secure_mysql($video));
        $post->setHeadline((isset($_POST['headline'])) ? $_POST['headline'] : "0");
        $post->setActive((isset($_POST['active'])) ? $_POST['active'] : "0");
        $post->setDraft((isset($_POST['draft'])) ? $_POST['draft'] : "0");
        $post->setUser($this->getEntry('\NaS\DevaPsicolegs\Entity\User', array("id" => $this->user->getId())));
        $postRepository = $em->getRepository('\NaS\DevaPsicolegs\Entity\Post');

        // categorías
        $postRepository->removeCategoryByPost($id);
        foreach ($_POST['category'] as $key => $cat) {
            $e_cat = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostCategory', array("id" => $cat));
            $post->addPostCategory($e_cat);
        }
        // Sub-categorías
        $postRepository->removeSubCategoryByPost($id);
        if (isset($_POST['subcategory'])) {
            foreach ($_POST['subcategory'] as $key => $sub) {
                $e_sub = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostSubCategory', array("id" => $sub));
                $post->addPostSubCategory($e_sub);
            }
        }
        // Etiquetas
        $postRepository->removeTagByPost($id);
        if (isset($_POST['tag'])) {
            foreach ($_POST['tag'] as $key => $tag) {
                $e_tag = $this->getEntry('\NaS\DevaPsicolegs\Entity\Tags', array("id" => $tag));
                $post->addTag($e_tag);
            }
        }

        // SEO
        $seo_url = (!empty($_POST['seo_url'])) ? friendlyURL($_POST['seo_url']) : friendlyURL($_POST['title']);
        $seo_title = (!empty($_POST['seo_title'])) ? $_POST['seo_title'] : $_POST['title'];
        $seo_author = (!empty($_POST['seo_author'])) ? secure_mysql($_POST['seo_author']) : $this->user->getUserInformation()->getName()." ".$this->user->getUserInformation()->getLastName();
        
        $post->getPostSeo()->setTitle(secure_mysql($seo_title));
        $post->getPostSeo()->setAuthor($seo_author);
        $post->getPostSeo()->setUrl(secure_mysql($this->getSeoURL($seo_url, $post)));
        $post->getPostSeo()->setKeywords(secure_mysql($_POST['seo_keywords']));
        $post->getPostSeo()->setDescription(secure_mysql($_POST['seo_description']));

        // Imagen de portada
        if (isset($_POST['hid-name'])) {
            if ($_POST['hid-name'] != "") {
                $this->saveImage(
                    $_POST['hid-name'],
                    $post->getCoverImage(),
                    APP_IMG_USER."/user_{$this->user->getCode()}/temp_files/posts/cover/",
                    $base_path."/cover/"
                );
                $post->setCoverImage(secure_mysql($_POST['hid-name']));
            }
        }

        // Imágenes de galería
        if (verifyPathFiles($temp_path."/gallery/")) {
            $images = $this->moveImages($temp_path."/gallery/", $base_path."/gallery/");

            foreach ($images as $key => $img) {
                $postGalleryImage = new PostGalleryImage;
                $post->addPostGalleryImage($postGalleryImage);
                $arr_gallery = $post->getPostGalleryImage();
                $pos = count($arr_gallery) - 1;
                $arr_gallery[$pos]->setImageName($img);
            }
        }

        // Imagenes de CKeditor
        if (verifyPathFiles($temp_path."/CK/")) {
            $this->moveImages($temp_path."/CK/", $base_path."/CK/");
        }

        // Banner
        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($post, 'banner');
        }

        $em->persist($post);
        $em->flush();

        $this->addLogs($this->user, "Editando publicación. ID: {$post->getId()}, Nombre: {$post->getTitle()}");
        $this->generateFlash('edited', "La <b>Publicación</b> ha sido editada correctamente");
        return $post;
    }

    /**
     * Obtiene las sub-categorías
     *
     * @return object
     */
    public function getSubcateogries()
    {
        $arr_cat = array();
        $post_id = @number_format($_POST['post_id'],0,"","");

        if (isset($_POST['category'])) {
            foreach ($_POST['category'] as $key => $val) {
                $cat = @number_format($val,0,"","");
                $this->existRecord("NaS\DevaPsicolegs\Entity\PostCategory", "pc", "pc.id='{$cat}'", "La categoría no es válida");
                $arr_cat[] = $val;
            }
        }

        $selected_subcategories = array();
        if ($post_id != 0 && $post_id != "") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\Post", "p", "p.id='{$post_id}'", "La publicación no es válida");
            $post = $this->getEntry('\NaS\DevaPsicolegs\Entity\Post', array("id" => $post_id));

            // Subcategorías seleccionadas
            foreach ($post->getPostSubCategory() as $sub) {
                $selected_subcategories[] = $sub->getId();
            }
        }

        $twigConfigLoader = new TwigConfigLoader;
        $twig = $twigConfigLoader->loadConfig();
        $em = $this->em;

        $postSubCategoryRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory');
        $postSubCategory = $postSubCategoryRepository->findSubByCat($arr_cat);

        $view = $twig->render('Admin/Ajax/select2-subcategories.twig', array(
            'subcategories' => $postSubCategory,
            'selectedSubCategories' => $selected_subcategories
        ));

        return $view;
    }

    /**
     * Prepara la imagen de perfil
     * @param  integer $act Acción
     * @return string
     */
    public function uploadProfileImage($act)
    {
        $user_code = $this->user->getCode();
        $dir_temp = APP_IMG_USER."/user_".$user_code."/temp_files/posts/cover";

        $arr_paths = array(
            APP_IMG_USER."/user_".$user_code."/temp_files/posts/",
            APP_IMG_USER."/user_".$user_code."/temp_files/posts/cover/",
            APP_IMG_USER."/user_".$user_code."/temp_files/posts/gallery/"
        );
        createDirectory($arr_paths);

        if ($act == 1) {
            isValidImage('cover_image', 150, 150);
            $tempIMG = $this->prepareCoverTemp($dir_temp, 'cover_image', 'cover_');
            return $this->showFormCoverIMG($tempIMG, $dir_temp, '/inkcms/ajax/post/proccessImage', 'frm-post', $user_code);
        } else {
            $arr_required = array(
                'x' => 'Eje X no se ha enviado',
                'y' => 'Eje Y no se ha enviado',
                'w' => 'El ancho no se ha enviado',
                'h' => 'La altura no se ha enviado',
            );
            requiredPost($_POST, $arr_required);
            return $this->createIMG($dir_temp."/", $user_code);
        }
    }

    /**
     * Crea la imagen temporal del CKeditor
     * @return void
     */
    public function uploadCKeditorImage()
    {
        $user_code = $this->user->getCode();
        $dir_temp = APP_IMG_USER."/user_".$user_code."/temp_files/posts/CK";

        $arr_paths = array(
            $dir_temp."/"
        );
        createDirectory($arr_paths);
        $imgage_name = $this->prepareCoverTemp($dir_temp, "upload", "ck".uniqid());
        return BASE_URL."/".$dir_temp."/".$imgage_name;
    }

    /**
     * Sube imagenes de galería
     *
     * @return void
     */
    public function uploadGallery()
    {
        $user_code = $this->user->getCode();
        $temp_path = APP_IMG_USER."/user_".$user_code."/temp_files/posts/gallery";

        $arr_paths = array(
            $temp_path."/"
        );

        createDirectory($arr_paths);
        isValidGalleryImage("gallery_images", 150, 100);
        return $this->uploadImages("gallery_images", $temp_path, "g_");
    }

    /**
     * Elimina las imágenes de galería
     *
     * @param integer $act  Modo: 1, Temporales, 2: finales
     * @param string $image Nombre de la imagen
     * @return void
     */
    public function removeGalleryImage($act, $image)
    {
        if ($act == 1) {
            $user_code = $this->user->getCode();
            $path = APP_IMG_USER."/user_".$user_code."/temp_files/posts/gallery/";
            if (!fileExist($path.$image)) {
                throw new \Exception("La imagen no existe", 1);
            }

            @unlink($path.$image);
            @unlink($path."min_".$image);

            $result = $this->getTempImages($path);
        } elseif ($act == 2) {
            $post_id = @number_format($_POST['post_id'],0,"","");
            $this->existRecord("NaS\DevaPsicolegs\Entity\Post", "p", "p.id='{$post_id}'", "La publicación no es válida");
            $this->existRecord("NaS\DevaPsicolegs\Entity\PostGalleryImage", "pg", "pg.id='{$image}'", "La imagen no es válida");

            $post = $this->getEntry('\NaS\DevaPsicolegs\Entity\Post', array("id" => $post_id));
            $image = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostGalleryImage', array("id" => $image));

            $code = $post->getCode();
            @unlink(APP_IMG_POST."/".$code."/gallery/".$image->getImageName());
            @unlink(APP_IMG_POST."/".$code."/gallery/min_".$image->getImageName());

            $this->em->remove($image);
            $this->em->flush();

            $result = $this->getGalleryImages($post);
        }

        return $result;
    }

    /**
     * Retorna la galería de imagen de una publicación
     *
     * @param \NaS\DevaPsicolegs\Entity\Post $Post Objeto de la publicación
     * @return void
     */
    public function getGalleryImages($post)
    {
        $twigConfigLoader = new TwigConfigLoader;
        $twig = $twigConfigLoader->loadConfig();

        $arr_temp_images = array();
        foreach ($post->getPostGalleryImage() as $img) {
            $arr_temp_images[] = array(
                'file' => $img->getImageName(),
                'id' => $img->getId()
            );
        }

        $view = $twig->render("Admin/Ajax/gallery.twig", array(
            'images' => $arr_temp_images,
            'base_path' => APP_IMG_VIEW_POST."/".$post->getCode()."/gallery/"
        ));

        return $view;
    }

    /**
     * Guarda el banner
     * @param  \NaS\DevaPsicolegs\Entity\Post  $post      Objeto del post
     * @param  string                        $parameter Nombre del parametro
     * @return void
     */
    public function persistImage(\NaS\DevaPsicolegs\Entity\Post $post, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_IMG_POST."/".$post->getCode()."/banner/";

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "banner_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($post->getBannerImage()) {
                        @unlink($path.$post->getBannerImage());
                    }

                    $post->setBannerImage($file_name);
                }
            }
        }
    }

    public function updateComment($id)
    {
        $em = $this->em;

        //Validar
        $arr_required = array(
            'names' => 'Debes ingresar al menos un nombre',
            'email' => 'Debes ingresar tu email',
            'comment' => 'Debes ingresar tu comentario',
            'p' => 'La publicación no es válida'
        );
        requiredPost($_POST, $arr_required);

        $post_id = @number_format($_POST['p'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Post", "p", "p.id='{$post_id}'", "La publicación no es válida");

        isValidString($_POST['names'], "Nombres: no se permiten caracteres especiales (#%^*\|)", "#%^*\|");
        isValidString($_POST['comment'], "Comentario: no se permiten caracteres especiales (#%^*\|)", "#%^*\|");
        isValidEmail($_POST['email']);

        if (isset($_POST['active'])) {
            isValidOption(0, 1, $_POST['active'], "La opción no es válida");
        } else {
            $_POST['active'] = "0";
        }

        $post = $this->getEntry('\NaS\DevaPsicolegs\Entity\Post', array("id" => $_POST['p']));
        $postComment = $this->getEntry('\NaS\DevaPsicolegs\Entity\PostComment', array("id" => $id));

        $postComment->setNames(mb_convert_case(secure_mysql(sanitize($_POST['names'])), MB_CASE_UPPER, "UTF-8"));
        $postComment->setEmail(mb_convert_case(secure_mysql(sanitize($_POST['email'])), MB_CASE_LOWER, "UTF-8"));
        $postComment->setComment(secure_mysql(sanitize($_POST['comment'])));
        $postComment->setActive($_POST['active']);
        $postComment->setPost($post);

        $em->persist($postComment);
        $em->flush();

        $this->addLogs($this->user, "Editando Comentario. ID: {$postComment->getId()}");
        $this->generateFlash('edited', "El <b>Comentario</b> ha sido editado correctamente");

        return $postComment;
    }


    /**
     * Valida las entradas del formlario
     *
     * @param string  $mode Modo de ejecución [persist, update]
     * @param integer $id ID de la entrada a editar
     * @return void
     */
    public function validate($mode, $id = NULL)
    {
        $arr_required = array(
            'title' => 'Debes seleccionar al menos una categoría',
            'content' => 'Debes ingresar el contenido'
        );
        requiredPost($_POST, $arr_required);
        if ($mode == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\Post", "p", "p.id='{$id}'", "La publicación no es válida");
        }
        isValidString($_POST['title'], "Título: no se permiten caracteres especiales", "#%^*\|");
        isValidString($_POST['seo_title'], "SEO título: no se permiten caracteres especiales", "#%^*\|");
        isValidString($_POST['seo_author'], "SEO Autor: no se permiten caracteres especiales", "#%^*\|");
        isValidString($_POST['seo_url'], "SEO URL: no se permiten caracteres especiales", "#%^*\|");
        isValidString($_POST['seo_keywords'], "SEO palabras clave: no se permiten caracteres especiales", "#%^*\|");
        if (isset($_POST['headline'])) { isValidOption(0, 1, $_POST['headline'], "El titular es inválido"); }
        if (isset($_POST['active'])) { isValidOption(0, 1, $_POST['active'], "El estado es inválido"); }
        if (isset($_POST['draft'])) { isValidOption(0, 1, $_POST['draft'], "El borrador no es válido"); }

        // Se validan las categorías
        if (!isset($_POST['category'])) {
            throw new \Exception("Debes seleccionar al menos una categoría");
        }

        if (count($_POST['category']) == 0) {
            throw new \Exception("Debes seleccionar al menos una categoría");
        }

        foreach ($_POST['category'] as $key => $cat) {
            $this->existRecord("NaS\DevaPsicolegs\Entity\PostCategory", "pc", "pc.id='{$cat}'", "La categoría no es válida");
        }

        // Se validan las subcategorías
        if (isset($_POST['subcategory'])) {
            if (count($_POST['subcategory']) > 0) {
                foreach ($_POST['subcategory'] as $key => $sub) {
                    $this->existRecord("NaS\DevaPsicolegs\Entity\PostSubCategory", "psc", "psc.id='{$sub}'", "La sub-categoría no es válida");
                }
            }
        }

        // Se validan los tags
        if (isset($_POST['tag'])) {
            if (count($_POST['tag']) > 0) {
                foreach ($_POST['tag'] as $key => $tag) {
                    $this->existRecord("NaS\DevaPsicolegs\Entity\Tags", "t", "t.id='{$tag}'", "El tag no es válido");
                }
            }
        }

        if ($_FILES["banner"]['tmp_name'] != "") {
            isValidImage('banner', 80, 80);
        }
    }

    /**
     * Elimina una publicación
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\Post", "p", "p.id='{$id}'", "La publicacion no es válida");
        $post = $this->em->getRepository('NaS\DevaPsicolegs\Entity\Post')->findOneBy(array('id' => $id));
        $code = $post->getCode();

        $base_path = APP_IMG_POST."/".$code;
        $arr_paths = array(
            $base_path."/",
            $base_path."/CK/",
            $base_path."/gallery/",
            $base_path."/cover/",
            $base_path."/banner/"
        );

        // imagenes de galería
        foreach ($post->getPostGalleryImage() as $img) {
            @unlink($base_path."/gallery/{$img->getImageName()}");
            @unlink($base_path."/gallery/min_{$img->getImageName()}");
        }
        // banner
        if ($post->getBannerImage()) {
            @unlink($base_path."/banner/{$post->getBannerImage()}");
            @unlink($base_path."/banner/min_{$post->getBannerImage()}");
        }
        // imagen de portada
        if ($post->getCoverImage()) {
            @unlink($base_path."/cover/{$post->getCoverImage()}");
            @unlink($base_path."/cover/min_{$post->getCoverImage()}");
        }

        $this->em->remove($post);
        $this->em->flush();

        $this->generateFlash('removed', "La <b>Publicación</b> ha sido eliminada correctamente");
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }

    /**
     * Genera el url de la publicación verificandolo en la base de datos
     * @param  string $title Título de la publicación
     * @return string
     */
    public function getSeoURL($title, $post = NULL)
    {
        $dql = new DQLfunctions;
        $seo_url = friendlyURL($title);

        if ($post) {
            $cnt_post = $dql->getCount('NaS\DevaPsicolegs\Entity\PostSeo', "s", "s.url='{$title}' AND NOT s.post='{$post->getId()}'");
        } else {
            $cnt_post = $dql->getCount('NaS\DevaPsicolegs\Entity\PostSeo', "s", "s.url='{$title}'");
        }

        return ($cnt_post == 0) ? $seo_url : $seo_url."-".($cnt_post + 1);
    }
}


?>
