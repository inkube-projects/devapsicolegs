<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\TwigConfigLoader;
use NaS\Classes\DQLfunctions;
use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\Slider;

class SliderService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Agrega una nueva entrada
     *
     * @return void
     */
    public function persist()
    {
        $em = $this->em;
        $this->validate('persist');

        // Se verifican los directorios
        $arr_paths = array(APP_IMG_SLIDER."/");
        createDirectory($arr_paths);

        $slider = new Slider;
        $slider->setUrl($_POST['url']);
        $slider->setContent(secure_mysql($_POST['content']));

        // imagen
        $this->persistImage($slider, 'banner');
        $em->persist($slider);
        $em->flush();

        $this->addLogs($this->user, "Creando imagen de carrusel. ID: {$slider->getId()}");
        $this->generateFlash('added', "La <b>Imagen de carrusel</b> ha sido creada correctamente");
        return $slider;
    }

    /**
     * Edita una entrada existente
     *
     * @param integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $em = $this->em;
        $this->validate('update', $id);

        // Se verifican los directorios
        $arr_paths = array(APP_IMG_SLIDER."/");
        createDirectory($arr_paths);

        $slider = $this->getEntry('\NaS\DevaPsicolegs\Entity\Slider', array("id" => $id));
        $slider->setUrl($_POST['url']);
        $slider->setContent(secure_mysql($_POST['content']));

        // imagen
        if ($_FILES['banner']['tmp_name'] != "") {
            $this->persistImage($slider, 'banner');
        }

        $em->persist($slider);
        $em->flush();

        $this->addLogs($this->user, "Editando imagen de carrusel. ID: {$slider->getId()}");
        $this->generateFlash('edited', "La <b>Imagen de carrusel</b> ha sido editada correctamente");
        return $slider;
    }

    /**
     * Guarda el banner
     * @param  \NaS\DevaPsicolegs\Entity\Slider  $slider      Objeto del Slider
     * @param  string                            $parameter Nombre del parametro
     * @return void
     */
    public function persistImage(\NaS\DevaPsicolegs\Entity\Slider $slider, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_IMG_SLIDER."/";

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "slider_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($slider->getImage()) {
                        @unlink($path.$slider->getImage());
                    }

                    $slider->setImage($file_name);
                }
            }
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }

    /**
     * Valida las entradas del formlario
     *
     * @param string  $mode Modo de ejecución [persist, update]
     * @param integer $id ID de la entrada a editar
     * @return void
     */
    public function validate($mode, $id = NULL)
    {
        if ($mode == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\Slider", "s", "s.id='{$id}'", "El slider no es válido");
        } else {
            if ($_FILES["banner"]['tmp_name'] == "") {
                throw new \Exception("Debes agregar una imagen", 400);
            }
        }
        isValidURL($_POST['url']);
        isValidString($_POST['content'], "Contenido: no se permiten caracteres especiales", "#%^*\|");
        isValidImage('banner', 500, 100);
    }
}


?>
