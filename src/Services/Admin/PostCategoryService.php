<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\PostCategory;

/**
 * Servicio para los procesos de usuarios
 */
class PostCategoryService extends ServiceAware
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * se crea una categoría
     *
     * @return object
     */
    public function persist()
    {
        $em = $this->em;
        $this->validate('persist');

        $postCategory = new PostCategory;
        $postCategory->setName($_POST['name']);
        $postCategory->setDescription($_POST['description']);
        $postCategory->setCanonicalName(friendlyURL($_POST['name'], "lower"));

        if (isset($_POST['hid-name'])) {
            if ($_POST['hid-name'] != "") {
                $this->saveImage(
                    $_POST['hid-name'],
                    "",
                    APP_IMG_USER."/user_{$this->user->getCode()}/temp_files/posts/category/",
                    APP_IMG_POST_CATEGORY."/"
                );
                $postCategory->setCoverImage(secure_mysql($_POST['hid-name']));
            }
        }

        $em->persist($postCategory);
        $em->flush();

        $this->addLogs($this->user, "Creando categoría. ID: {$postCategory->getId()}, Nombre: {$postCategory->getName()}");
        $this->generateFlash('added', "La <b>Categoría</b> ha sido creada correctamente");
        return $postCategory;
    }

    /**
     * Edita una categoría
     * @param  integer $id ID del ususario
     * @return object
     */
    public function update($id)
    {
        $em = $this->em;
        // Validaciones
        $this->validate('update', $id);
        $postCategory = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory')->findOneBy(array('id' => $id));
        $postCategory->setName($_POST['name']);
        $postCategory->setDescription($_POST['description']);
        $postCategory->setCanonicalName(friendlyURL($_POST['name'], "lower"));

        if (isset($_POST['hid-name'])) {
            if ($_POST['hid-name'] != "") {
                $this->saveImage(
                    $_POST['hid-name'],
                    $postCategory->getCoverImage(),
                    APP_IMG_USER."/user_{$this->user->getCode()}/temp_files/posts/category/",
                    APP_IMG_POST_CATEGORY."/"
                );
                $postCategory->setCoverImage(secure_mysql($_POST['hid-name']));
            }
        }

        $em->persist($postCategory);
        $em->flush();

        $this->addLogs($this->user, "Editando categoría. ID: {$postCategory->getId()}, Nombre: {$postCategory->getName()}");
        $this->generateFlash('edited', "La <b>Categoría</b> ha sido editada correctamente");
        return $postCategory;
    }

    /**
     * Elimina una categoría
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\PostCategory", "pc", "pc.id='{$id}'", "La categoría no es válida");
        $postCategory = $this->em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory')->findOneBy(array('id' => $id));
        $image = $postCategory->getCoverImage();
        $this->em->remove($postCategory);
        $this->em->flush();

        @unlink(APP_IMG_POST_CATEGORY."/{$image}");
        $this->generateFlash('removed', "La <b>Categoria</b> ha sido eliminado correctamente");
    }

    /**
     * Prepara la imagen de perfil
     * @param  integer $act Acción
     * @return string
     */
    public function uploadProfileImage($act)
    {
        $user_code = $this->user->getCode();
        $dir_temp = APP_IMG_USER."/user_".$user_code."/temp_files/posts/category";

        $arr_paths = array(
            APP_IMG_USER."/user_".$user_code."/temp_files/posts/",
            APP_IMG_USER."/user_".$user_code."/temp_files/posts/category/",
            APP_IMG_POST_CATEGORY."/"
        );
        createDirectory($arr_paths);

        if ($act == 1) {
            isValidImage('cover_image', 150, 150);
            $tempIMG = $this->prepareCoverTemp($dir_temp, 'cover_image');
            return $this->showFormCoverIMG($tempIMG, $dir_temp, '/inkcms/ajax/post/category/proccessImage', 'frm-post-category', $user_code);
        } else {
            $arr_required = array(
                'x' => 'Eje X no se ha enviado',
                'y' => 'Eje Y no se ha enviado',
                'w' => 'El ancho no se ha enviado',
                'h' => 'La altura no se ha enviado',
            );
            requiredPost($_POST, $arr_required);
            return $this->createIMG($dir_temp."/", $user_code);
        }
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string $act persist, update
     * @return void
     */
    public function validate($act, $id = NULL)
    {
        $arr_required = array(
            'name' => 'Debes ingresar el nombre de la categoría'
        );
        requiredPost($_POST, $arr_required);

        if ($act == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\PostCategory", "pc", "pc.id='{$id}'", "La categoría no es válida");
        }

        isValidString($_POST['name'], "Nombre: No se permiten caracteres especiales", "#$%^*\|");
        isValidString($_POST['description'], "Descripción: No se permiten caracteres especiales", "#$%^*\|");
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }
}


?>
