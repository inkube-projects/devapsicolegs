<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\UserProvider;
use NaS\DevaPsicolegs\Entity\SeoSection;

/**
 * Seo Section Service
 */
class SeoSectionService extends ServiceAware
{
    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserInfo();
    }

    /**
     * Guarda una seccion
     *
     * @return object
     */
    public function persist()
    {
        $em = $this->em;
        // Validación
        $this->validate('persist');

        $seoSection = new SeoSection;
        $seoSection->setSection(secure_mysql($_POST['name']));
        $em->persist($seoSection);
        $em->flush();

        $this->addLogs($this->user, "Creando sección. ID: {$seoSection->getId()}, Nombre: {$seoSection->getSection()}");
        $this->generateFlash('added', "La <b>ección</b> ha sido creada correctamente");
        return $seoSection;
    }

    /**
     * Edita una sección
     *
     * @param integer $id
     * @return object
     */
    public function update($id)
    {
        $em = $this->em;
        // Validación
        $this->validate('update', $id);

        $seoSection = $em->getRepository('NaS\DevaPsicolegs\Entity\SeoSection')->findOneBy(array('id' => $id));
        $seoSection->setSection(secure_mysql($_POST['name']));
        $em->persist($seoSection);
        $em->flush();

        $this->addLogs($this->user, "Creando sección. ID: {$seoSection->getId()}, Nombre: {$seoSection->getSection()}");
        $this->generateFlash('edited', "La <b>Sección</b> ha sido editada correctamente");
        return $seoSection;
    }

    /**
     * Elimina una sección
     * @return void
     */
    public function remove()
    {
        $id = @number_format($_POST['entry'],0,"","");
        $this->existRecord("NaS\DevaPsicolegs\Entity\SeoSection", "ss", "ss.id='{$id}'", "La sección no es válida");
        $seoSection = $this->em->getRepository('NaS\DevaPsicolegs\Entity\SeoSection')->findOneBy(array('id' => $id));
        $this->em->remove($seoSection);
        $this->em->flush();

        $this->generateFlash('removed', "La <b>Sección</b> ha sido eliminada correctamente");
    }

    /**
     * Valida las entradas del formulario
     *
     * @param string $act persist, update
     * @return void
     */
    public function validate($act, $id = NULL)
    {
        $arr_required = array(
            'name' => 'Debes ingresar el nombre de la sección'
        );
        requiredPost($_POST, $arr_required);

        if ($act == "update") {
            $this->existRecord("NaS\DevaPsicolegs\Entity\SeoSection", "ss", "ss.id='{$id}'", "La sección no es válida");
        }

        isValidString($_POST['name'], "Nombre: No se permiten caracteres especiales", "#$%^*\|");
    }

    /**
     * Obtiene la información del ususairo
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }
}


?>
