<?php

namespace NaS\DevaPsicolegs\Services\Admin;

use NaS\Classes\ServiceAware;
use NaS\DevaPsicolegs\Entity\UserLogs;
use NaS\Classes\JWThandler;

/**
 * Servicio de inicio de sesión
 */
class LoginService extends ServiceAware
{
    public function login()
    {
        $arr_required = array(
            'user_email' => 'Debes agregar el nombre de usuario o E-mail',
            'password' => 'Debes agregar la contraseña'
        );
        requiredPost($_POST, $arr_required);
        $username_email = mb_convert_case(secure_mysql($_POST['user_email']), MB_CASE_UPPER, "UTF-8");
        $pass = addslashes($_POST['password']);
        $user = $this->verifyCredentials($username_email, $pass);
        $this->createSession($user);
    }

    /**
     * Verifica las credenciales y retorna el usuario
     *
     * @param string $username_email
     * @param string $pass
     * @return object|Exception
     */
    public function verifyCredentials($username_email, $pass)
    {
        $em = $this->em;
        $userRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\User');

        // se verifica si es un email o un nombre de usuario
        if (filter_var($username_email, FILTER_VALIDATE_EMAIL) === false) {
            $user = $userRepository->findUserByUsername($username_email);
        } else {
            $user = $userRepository->findUserByEmail($username_email);
        }

        // Si el usuario no existe
        if (!$user) {
            throw new \Exception("Usuario o contraseña incorrecto(s)");
        }

        $user_password = $user->getPassword();
        if (!password_verify($pass, $user_password)) {
            throw new \Exception("Usuario o contraseña incorrecto(s)");
        }

        return $user;
    }

    /**
     * Crea la sesion
     *
     * @param object $user
     * @return void
     */
    public function createSession($user)
    {
        $em = $this->em;
        $JWThandler = new JWThandler;
        $userLogs = new UserLogs;
        $ip_acces = @$_SERVER['REMOTE_ADDR'];
        $ip_proxy = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $ip_compa = @$_SERVER['HTTP_CLIENT_IP'];

        // se genera el token de sesión
        $arr_data = array(
            'user_id' => $user->getId(),
            'username' => $user->getUsername()
        );
        $token = $JWThandler->generateToken($arr_data, time(), (time() + 86400));

        $userLogs->setUsername($user->getUsername());
        $userLogs->setAction("Inicio de sesión");
        $userLogs->setIpAccess($ip_acces);
        $userLogs->setIpProxy($ip_proxy);
        $userLogs->setIpCompany($ip_compa);
        $userLogs->setUser($user);
        $userLogs->setCreateAt(new \DateTime);
        $userLogs->getUser()->setLastLogin(new \DateTime);
        $userLogs->getUser()->setSessionToken($token);
        $em->persist($userLogs);
        $em->flush();

        $_SESSION['SES_ADMIN_DEVAPSICOLEGS']['token'] = $token;
        $_SESSION['SES_ADMIN_DEVAPSICOLEGS']['id'] = $user->getId();
        $_SESSION['SES_ADMIN_DEVAPSICOLEGS']['username'] = $user->getUsername();

        setcookie("session.cookie_lifetime", "session expired", time() + 86400);
    }
}


?>
