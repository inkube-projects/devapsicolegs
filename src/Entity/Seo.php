<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SEO entity
 *
 * @ORM\Table(name="seo")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\SeoRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Seo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", length=65535, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="text", length=255, nullable=true)
     */
    private $author;


    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", length=65535, nullable=false)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * Many to One SeoSection
     * @var \NaS\DevaPsicolegs\Entity\SeoSection
     *
     * @ORM\ManyToOne(targetEntity="SeoSection", inversedBy="seo")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id")
     */
    private $seoSection;

    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Title
     *
     * @param string title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of Author
     *
     * @param string author
     *
     * @return self
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the value of Keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set the value of Keywords
     *
     * @param string keywords
     *
     * @return self
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Many to One SeoSection
     *
     * @return \NaS\DevaPsicolegs\Entity\SeoSection
     */
    public function getSeoSection()
    {
        return $this->seoSection;
    }

    /**
     * Set the value of Many to One SeoSection
     *
     * @param \NaS\DevaPsicolegs\Entity\SeoSection seoSection
     *
     * @return self
     */
    public function setSeoSection(\NaS\DevaPsicolegs\Entity\SeoSection $seoSection)
    {
        $this->seoSection = $seoSection;

        return $this;
    }

}


?>
