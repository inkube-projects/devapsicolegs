<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Role
 *
 * @ORM\Table(name="role")
 * @ORM\Entity()
 */
class Role
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=80, nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="role_name", type="string", length=80, nullable=false)
     */
    private $roleName;

    /**
     * @var \NaS\DevaPsicolegs\Entity\User
     * 
     * @ORM\OneToMany(targetEntity="User", mappedBy="role")
     */
    private $user;

    public function __construct() {
        $this->user = new ArrayCollection();
    }

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of role
     *
     * @return  string
     */ 
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @param  string  $role
     *
     * @return  self
     */ 
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get the value of roleName
     *
     * @return  string
     */ 
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * Set the value of roleName
     *
     * @param  string  $roleName
     *
     * @return  self
     */ 
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;

        return $this;
    }

    /**
     * Get the value of user
     *
     * @return  NaS\DevaPsicolegs\Entity\User
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @param  \NaS\DevaPsicolegs\Entity\User  $user
     *
     * @return  self
     */ 
    public function setUser(\NaS\DevaPsicolegs\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }
}
