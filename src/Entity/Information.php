<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Tags Entoty
 *
 * @ORM\Table(name="information")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\InformationRepository")
 *
 */
class Information
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="history", type="text", length=4294967295, nullable=true)
     */
    private $history;

    /**
     * @var string
     *
     * @ORM\Column(name="team", type="text", length=4294967295, nullable=true)
     */
    private $team;

    /**
     * @var string
     *
     * @ORM\Column(name="services", type="text", length=4294967295, nullable=true)
     */
    private $services;



    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of History
     *
     * @return string
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set the value of History
     *
     * @param string history
     *
     * @return self
     */
    public function setHistory($history)
    {
        $this->history = $history;

        return $this;
    }

    /**
     * Get the value of Team
     *
     * @return string
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set the value of Team
     *
     * @param string team
     *
     * @return self
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get the value of Services
     *
     * @return string
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set the value of Services
     *
     * @param string services
     *
     * @return self
     */
    public function setServices($services = NULL)
    {
        $this->services = $services;

        return $this;
    }

}

?>
