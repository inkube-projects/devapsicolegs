<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=180, nullable=false, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180, nullable=false, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="username_canonical", type="string", length=180, nullable=false)
     */
    private $usernameCanonical;

    /**
     * @var string
     *
     * @ORM\Column(name="email_canonical", type="string", length=180, nullable=false)
     */
    private $emailCanonical;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_image", type="string", length=180, nullable=true)
     */
    private $profileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=180, nullable=true)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    private $passwordRequestedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="session_token", type="text", length=65535, nullable=true)
     */
    private $sessionToken;

    /**
     * @var \NaS\DevaPsicolegs\Entity\Role
     *
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="user")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=false)
     */
    private $role;

    /**
     * @var \NaS\DevaPsicolegs\Entity\UserStatus
     *
     * @ORM\ManyToOne(targetEntity="UserStatus", inversedBy="user")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false)
     */
    private $userStatus;

    /**
     * @var \NaS\DevaPsicolegs\Entity\UserInformation
     *
     * @ORM\OneToOne(targetEntity="UserInformation", mappedBy="user", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $userInformation;

    /**
     * @var \NaS\DevaPsicolegs\Entity\UserLogs
     *
     * @ORM\OneToMany(targetEntity="UserLogs", mappedBy="user")
     */
    private $userLogs;

    /**
     * One to many Post
     * @var \NaS\DevaPsicolegs\Entity\Post
     *
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     */
    private $post;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;


    public function __construct() {
        $this->userLogs = new ArrayCollection();
        $this->post = new ArrayCollection();
    }
    

    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of Username
     *
     * @param string username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param string email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of Password
     *
     * @param string password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of Username Canonical
     *
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    /**
     * Set the value of Username Canonical
     *
     * @param string usernameCanonical
     *
     * @return self
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    /**
     * Get the value of Email Canonical
     *
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Set the value of Email Canonical
     *
     * @param string emailCanonical
     *
     * @return self
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * Get the value of Profile Image
     *
     * @return string
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Set the value of Profile Image
     *
     * @param string profileImage
     *
     * @return self
     */
    public function setProfileImage($profileImage)
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    /**
     * Get the value of Code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of Code
     *
     * @param string code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of Last Login
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set the value of Last Login
     *
     * @param \DateTime lastLogin
     *
     * @return self
     */
    public function setLastLogin(\DateTime $lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get the value of Password Requested At
     *
     * @return \DateTime
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * Set the value of Password Requested At
     *
     * @param \DateTime passwordRequestedAt
     *
     * @return self
     */
    public function setPasswordRequestedAt(\DateTime $passwordRequestedAt)
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    /**
     * Get the value of Role
     *
     * @return \NaS\DevaPsicolegs\Entity\Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of Role
     *
     * @param \NaS\DevaPsicolegs\Entity\Role role
     *
     * @return self
     */
    public function setRole(\NaS\DevaPsicolegs\Entity\Role $role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get the value of User Status
     *
     * @return \NaS\DevaPsicolegs\Entity\UserStatus
     */
    public function getUserStatus()
    {
        return $this->userStatus;
    }

    /**
     * Set the value of User Status
     *
     * @param \NaS\DevaPsicolegs\Entity\UserStatus userStatus
     *
     * @return self
     */
    public function setUserStatus(\NaS\DevaPsicolegs\Entity\UserStatus $userStatus)
    {
        $this->userStatus = $userStatus;

        return $this;
    }

    /**
     * Get the value of User Information
     *
     * @return \NaS\DevaPsicolegs\Entity\UserInformation
     */
    public function getUserInformation()
    {
        return $this->userInformation;
    }

    /**
     * Set the value of User Information
     *
     * @param \NaS\DevaPsicolegs\Entity\UserInformation userInformation
     *
     * @return self
     */
    public function setUserInformation(\NaS\DevaPsicolegs\Entity\UserInformation $userInformation)
    {
        $this->userInformation = $userInformation;
        $userInformation->setUser($this);

        return $this;
    }

    /**
     * Get the value of Create At
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set the value of Create At
     *
     * @param \DateTime createAt
     *
     * @return self
     */
    public function setCreateAt(\DateTime $createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get the value of Update At
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set the value of Update At
     *
     * @param \DateTime updateAt
     *
     * @return self
     */
    public function setUpdateAt(\DateTime $updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get the value of User Logs
     *
     * @return \NaS\DevaPsicolegs\Entity\UserLogs
     */
    public function getUserLogs()
    {
        return $this->userLogs;
    }

    /**
     * Set the value of User Logs
     *
     * @param \NaS\DevaPsicolegs\Entity\UserLogs userLogs
     *
     * @return self
     */
    public function setUserLogs(\NaS\DevaPsicolegs\Entity\UserLogs $userLogs)
    {
        $this->userLogs = $userLogs;

        return $this;
    }


    /**
     * Get the value of sessionToken
     *
     * @return  string
     */
    public function getSessionToken()
    {
        return $this->sessionToken;
    }

    /**
     * Set the value of sessionToken
     *
     * @param  string  $sessionToken
     *
     * @return  self
     */
    public function setSessionToken($sessionToken)
    {
        $this->sessionToken = $sessionToken;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setCreateAtValue()
    {
      $this->createAt = new \DateTime();
      $this->updateAt = new \DateTime();
    }

    /**
    * @ORM\PreUpdate
    */
    public function setUpdateAtValue()
    {
      $this->updateAt = new \DateTime();
    }

}
