<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * UserStatus
 *
 * @ORM\Table(name="user_status")
 * @ORM\Entity()
 */
class UserStatus
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50, nullable=false)
     */
    private $description;

    /**
     * @var \NaS\DevaPsicolegs\Entity\User
     * 
     * @ORM\OneToMany(targetEntity="User", mappedBy="userStatus")
     */
    private $user;

    public function __construct() {
        $this->user = new ArrayCollection();
    }

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of description
     *
     * @return  string
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string  $description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of user
     *
     * @return  \NaS\DevaPsicolegs\Entity\User
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @param  \NaS\DevaPsicolegs\Entity\User  $user
     *
     * @return  self
     */ 
    public function setUser(\NaS\DevaPsicolegs\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }
}
