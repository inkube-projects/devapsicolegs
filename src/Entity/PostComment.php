<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Post entity
 *
 * @ORM\Table(name="post_comments")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PostComment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="names", type="text", length=180, nullable=false)
     */
    private $names;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=180, nullable=false)
     */
    private $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=false)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * Many to One - Post
     * @var \NaS\DevaPsicolegs\Entity\Post
     * 
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="postComment")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $post;

    public function __construct() {
        $this->postGalleryImage = new ArrayCollection();
    }


   

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of names
     *
     * @return  string
     */ 
    public function getNames()
    {
        return $this->names;
    }

    /**
     * Set the value of names
     *
     * @param  string  $names
     *
     * @return  self
     */ 
    public function setNames($names)
    {
        $this->names = $names;

        return $this;
    }

    /**
     * Get the value of email
     *
     * @return  string
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @param  string  $email
     *
     * @return  self
     */ 
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of comment
     *
     * @return  string
     */ 
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set the value of comment
     *
     * @param  string  $comment
     *
     * @return  self
     */ 
    public function setComment(string $comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get the value of active
     *
     * @return  boolean
     */ 
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the value of active
     *
     * @param  boolean  $active
     *
     * @return  self
     */ 
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get the value of createAt
     *
     * @return  \DateTime
     */ 
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set the value of createAt
     *
     * @param  \DateTime  $createAt
     *
     * @return  self
     */ 
    public function setCreateAt(\DateTime $createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get the value of updateAt
     *
     * @return  \DateTime
     */ 
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set the value of updateAt
     *
     * @param  \DateTime  $updateAt
     *
     * @return  self
     */ 
    public function setUpdateAt(\DateTime $updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setCreateAtValue()
    {
      $this->createAt = new \DateTime();
      $this->updateAt = new \DateTime();
    }

    /**
    * @ORM\PreUpdate
    */
    public function setUpdateAtValue()
    {
      $this->updateAt = new \DateTime();
    }

    /**
     * Get many to One - Post
     *
     * @return  \NaS\DevaPsicolegs\Entity\Post
     */ 
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set many to One - Post
     *
     * @param  \NaS\DevaPsicolegs\Entity\Post  $post  Many to One - Post
     *
     * @return  self
     */ 
    public function setPost(\NaS\DevaPsicolegs\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }
}

?>
