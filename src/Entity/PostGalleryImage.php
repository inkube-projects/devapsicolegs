<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PostGalleryImage entity
 *
 * @ORM\Table(name="post_gallery")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\PostGalleryImageRepository")
 *
 */
class PostGalleryImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image_name", type="text", length=180, nullable=false)
     */
    private $imageName;

    /**
     * Many to one Post
     * @var \NaS\DevaPsicolegs\Entity\Post
     *
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="postGalleryImage")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $post;



    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of imageName
     *
     * @return  string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set the value of imageName
     *
     * @param  string  $imageName
     *
     * @return  self
     */
    public function setImageName(string $imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get the value of Many to one Post
     *
     * @return \NaS\DevaPsicolegs\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set the value of Many to one Post
     *
     * @param \NaS\DevaPsicolegs\Entity\Post post
     *
     * @return self
     */
    public function setPost(\NaS\DevaPsicolegs\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

}

?>
