<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team entity
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\TeamRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=180, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="text", length=65535, nullable=true)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_image", type="text", length=180, nullable=true)
     */
    private $profileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="text", length=20, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="text", length=65535, nullable=true)
     */
    private $experience;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;



    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of name
     *
     * @return  string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     *
     * @return  self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description
     *
     * @return  string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string  $description
     *
     * @return  self
     */
    public function setDescription(string $description = NULL)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of profileImage
     *
     * @return  string
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Set the value of profileImage
     *
     * @param  string  $profileImage
     *
     * @return  self
     */
    public function setProfileImage(string $profileImage = NULL)
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    /**
     * Get the value of createAt
     *
     * @return  \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set the value of createAt
     *
     * @param  \DateTime  $createAt
     *
     * @return  self
     */
    public function setCreateAt(\DateTime $createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get the value of updateAt
     *
     * @return  \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set the value of updateAt
     *
     * @param  \DateTime  $updateAt
     *
     * @return  self
     */
    public function setUpdateAt(\DateTime $updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setCreateAtValue()
    {
      $this->createAt = new \DateTime();
      $this->updateAt = new \DateTime();
    }

    /**
    * @ORM\PreUpdate
    */
    public function setUpdateAtValue()
    {
      $this->updateAt = new \DateTime();
    }

    /**
     * Get the value of phone
     *
     * @return  string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @param  string  $phone
     *
     * @return  self
     */
    public function setPhone(string $phone = NULL)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of experience
     *
     * @return  string
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set the value of experience
     *
     * @param  string  $experience
     *
     * @return  self
     */
    public function setExperience(string $experience = NULL)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get the value of Short Description
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set the value of Short Description
     *
     * @param string shortDescription
     *
     * @return self
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

}


?>
