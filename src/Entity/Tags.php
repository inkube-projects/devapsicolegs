<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tags Entoty
 *
 * @ORM\Table(name="tags")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\TagsRepository")
 *
 */
class Tags
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=180, nullable=false)
     */
    private $name;

    /**
     * Many to many Post
     * @var \NaS\DevaPsicolegs\Entity\Post
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="tags")
     */
    private $post;

    public function __construct() {
        $this->post = new ArrayCollection();
    }

    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of name
     *
     * @return  string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     *
     * @return  self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get many to many Post
     *
     * @return  \NaS\DevaPsicolegs\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Get many to many Post
     *
     * @return \NaS\DevaPsicolegs\Entity\Post
     */
    public function addPost(\NaS\DevaPsicolegs\Entity\Post $post)
    {
        $this->post[] = $post;
    }
}


?>
