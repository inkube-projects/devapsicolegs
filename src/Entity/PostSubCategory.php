<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PostSubCategory Entity
 *
 * @ORM\Table(name="posts_subcategory")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\PostSubCategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class PostSubCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=180, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="canonical_name", type="text", length=200, nullable=false)
     */
    private $canonicalName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * Many to one PostCategory
     * @var \NaS\DevaPsicolegs\Entity\PostCategory
     *
     * @ORM\ManyToOne(targetEntity="PostCategory", inversedBy="postSubCategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $postCategory;

    /**
     * Many to many Post
     * @var \NaS\DevaPsicolegs\Entity\Post
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="postSubCategory")
     */
    private $post;

    public function __construct() {
        $this->post = new ArrayCollection();
    }

    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Many to one PostCategory
     *
     * @return \NaS\DevaPsicolegs\Entity\PostCategory
     */
    public function getPostCategory()
    {
        return $this->postCategory;
    }

    /**
     * Set the value of Many to one PostCategory
     *
     * @param \NaS\DevaPsicolegs\Entity\PostCategory postCategory
     *
     * @return self
     */
    public function setPostCategory(\NaS\DevaPsicolegs\Entity\PostCategory $postCategory)
    {
        $this->postCategory = $postCategory;

        return $this;
    }

    /**
     * Get many to many Post
     *
     * @return \NaS\DevaPsicolegs\Entity\Post
     */
    public function addPost(\NaS\DevaPsicolegs\Entity\Post $post)
    {
        $this->post[] = $post;
    }

    /**
     * Get the value of Canonical Name
     *
     * @return string
     */
    public function getCanonicalName()
    {
        return $this->canonicalName;
    }

    /**
     * Set the value of Canonical Name
     *
     * @param string canonicalName
     *
     * @return self
     */
    public function setCanonicalName($canonicalName)
    {
        $this->canonicalName = $canonicalName;

        return $this;
    }
}


?>
