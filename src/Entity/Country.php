<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity()
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=70, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="country_index", type="string", length=10, nullable=true)
     */
    private $countryIndex;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, nullable=true)
     */
    private $code;

    /**
     * @var \NaS\DevaPsicolegs\Entity\UserInformation
     *
     * @ORM\OneToMany(targetEntity="UserInformation", mappedBy="country")
     */
    private $userInformation;


    public function __construct() {
        $this->userInformation = new ArrayCollection();
    }

    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of Country
     *
     * @param string country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of Country Index
     *
     * @return string
     */
    public function getCountryIndex()
    {
        return $this->countryIndex;
    }

    /**
     * Set the value of Country Index
     *
     * @param string countryIndex
     *
     * @return self
     */
    public function setCountryIndex($countryIndex)
    {
        $this->countryIndex = $countryIndex;

        return $this;
    }

    /**
     * Get the value of Code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of Code
     *
     * @param string code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of User Information
     *
     * @return \NaS\DevaPsicolegs\Entity\UserInformation
     */
    public function getUserInformation()
    {
        return $this->userInformation;
    }

    /**
     * Set the value of User Information
     *
     * @param \NaS\DevaPsicolegs\Entity\UserInformation userInformation
     *
     * @return self
     */
    public function setUserInformation(\NaS\DevaPsicolegs\Entity\UserInformation $userInformation)
    {
        $this->userInformation = $userInformation;

        return $this;
    }

}
