<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Team entity
 *
 * @ORM\Table(name="services")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\ServicesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Services
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=180, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=4294967295, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * One to Many - SubServices
     * @var NaS\DevaPsicolegs\Entity\SubServices
     *
     * @ORM\OneToMany(targetEntity="SubServices", mappedBy="services", cascade={"persist", "remove"})
     */
    private $subServices;


    public function __construct() {
        $this->subServices = new ArrayCollection();
    }


    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description = NULL)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Create At
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set the value of Create At
     *
     * @param \DateTime createAt
     *
     * @return self
     */
    public function setCreateAt(\DateTime $createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get the value of Update At
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set the value of Update At
     *
     * @param \DateTime updateAt
     *
     * @return self
     */
    public function setUpdateAt(\DateTime $updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }


    /**
    * @ORM\PrePersist
    */
    public function setCreateAtValue()
    {
      $this->createAt = new \DateTime();
      $this->updateAt = new \DateTime();
    }

    /**
    * @ORM\PreUpdate
    */
    public function setUpdateAtValue()
    {
      $this->updateAt = new \DateTime();
    }

    /**
     * Get the value of One to Many - SubServices
     *
     * @return NaS\DevaPsicolegs\Entity\SubServices
     */
    public function getSubServices()
    {
        return $this->subServices;
    }

    /**
     * Set the value of One to Many - SubServices
     *
     * @param NaS\DevaPsicolegs\Entity\SubServices subServices
     *
     * @return self
     */
    public function setSubServices(\NaS\DevaPsicolegs\Entity\SubServices $subServices)
    {
        $this->subServices = $subServices;

        return $this;
    }
}


?>
