<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Tags Entoty
 *
 * @ORM\Table(name="contact_reason")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\ContactRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class ContactReason
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=200, nullable=false)
     */
    private $name;

    /**
     * One to many - Contact
     * @var \NaS\DevaPsicolegs\Entity\Contact
     *
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="contactReason")
     */
    private $contact;


    public function __construct() {
        $this->contact = new ArrayCollection();
    }


    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of One to many - Contact
     *
     * @return NaS\DevaPsicolegs\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set the value of One to many - Contact
     *
     * @param \NaS\DevaPsicolegs\Entity\Contact contact
     *
     * @return self
     */
    public function setContact(\NaS\DevaPsicolegs\Entity\Contact $contact)
    {
        $this->contact = $contact;

        return $this;
    }

}

?>
