<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SeoSection entity
 *
 * @ORM\Table(name="seo_section")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\SeoRepository")
 */
class SeoSection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="section", type="text", length=255, nullable=false)
     */
    private $section;

    /**
     * One To Many SEO
     * @var \NaS\DevaPsicolegs\Entity\Seo
     *
     * @ORM\OneToMany(targetEntity="Seo", mappedBy="seoSection")
     */
    private $seo;

    public function __construct()
    {
        $this->seo = new ArrayCollection();
    }

    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Section
     *
     * @return string
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set the value of Section
     *
     * @param string section
     *
     * @return self
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get the value of One To Many SEO
     *
     * @return \NaS\DevaPsicolegs\Entity\Seo
     */
    public function getSeo()
    {
        return $this->seo;
    }

    /**
     * Set the value of One To Many SEO
     *
     * @param \NaS\DevaPsicolegs\Entity\Seo seo
     *
     * @return self
     */
    public function setSeo(\NaS\DevaPsicolegs\Entity\Seo $seo)
    {
        $this->seo = $seo;

        return $this;
    }

}


?>
