<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Post entity
 *
 * @ORM\Table(name="posts")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", length=65535, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=4294967295, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="cover_image", type="text", length=255, nullable=true)
     */
    private $coverImage;

    /**
     * @var string
     *
     * @ORM\Column(name="banner_image", type="text", length=255, nullable=true)
     */
    private $bannerImage;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="text", length=65535, nullable=true)
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="text", length=50, nullable=false)
     */
    private $code;

    /**
     * @var boolean
     *
     * @ORM\Column(name="headline", type="boolean", nullable=false)
     */
    private $headline;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="draft", type="boolean", nullable=false)
     */
    private $draft;

    /**
     * @var decimal
     *
     * @ORM\Column(name="rating", type="decimal", scale=1, nullable=true)
     */
    private $rating;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * many to one User
     * @var \NaS\DevaPsicolegs\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="post")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many to Many PostSubCategory
     * @var \NaS\DevaPsicolegs\Entity\PostCategory
     *
     * @ORM\ManyToMany(targetEntity="PostCategory", inversedBy="post", indexBy="id", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="posts_categories",
     *      joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="postcategory_id", referencedColumnName="id")}
     * )
     */
    private $postCategory;

    /**
     * Many to Many PostSubCategory
     * @var \NaS\DevaPsicolegs\Entity\PostSubCategory
     *
     * @ORM\ManyToMany(targetEntity="PostSubCategory", inversedBy="post", cascade={"persist"})
     * @ORM\JoinTable(name="posts_subcategories")
     */
    private $postSubCategory;

    /**
     * Many to Many Tags
     * @var \NaS\DevaPsicolegs\Entity\Tags
     *
     * @ORM\ManyToMany(targetEntity="Tags", inversedBy="post", cascade={"persist"})
     * @ORM\JoinTable(name="posts_tags")
     */
    private $tags;

    /**
     * one to many PostGalleryImage
     * @var \NaS\DevaPsicolegs\Entity\PostGalleryImage
     *
     * @ORM\OneToMany(targetEntity="PostGalleryImage", mappedBy="post", cascade={"persist", "remove"})
     */
    private $postGalleryImage;

    /**
     * One to One PostSeo
     * @var \NaS\DevaPsicolegs\Entity\PostSeo
     *
     * @ORM\OneToOne(targetEntity="PostSeo", mappedBy="post", cascade={"persist","remove"})
     */
    private $postSeo;

    /**
     * One to Many - PostComment
     * @var \NaS\DevaPsicolegs\Entity\PostComment
     * 
     * @ORM\OneToMany(targetEntity="PostComment", mappedBy="post", cascade={"persist", "remove"})
     */
    private $postComment;


    public function __construct() {
        $this->postCategory = new ArrayCollection();
        $this->postSubCategory = new ArrayCollection();
        $this->Tags = new ArrayCollection();
        $this->postGalleryImage = new ArrayCollection();
        $this->postComment = new ArrayCollection();
    }


    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Title
     *
     * @param string title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of Content
     *
     * @param string content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of Cover Image
     *
     * @return string
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * Set the value of Cover Image
     *
     * @param string coverImage
     *
     * @return self
     */
    public function setCoverImage($coverImage)
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    /**
     * Get the value of Banner Image
     *
     * @return string
     */
    public function getBannerImage()
    {
        return $this->bannerImage;
    }

    /**
     * Set the value of Banner Image
     *
     * @param string bannerImage
     *
     * @return self
     */
    public function setBannerImage($bannerImage)
    {
        $this->bannerImage = $bannerImage;

        return $this;
    }

    /**
     * Get the value of Video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set the value of Video
     *
     * @param string video
     *
     * @return self
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get the value of Create At
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set the value of Create At
     *
     * @param \DateTime createAt
     *
     * @return self
     */
    public function setCreateAt(\DateTime $createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get the value of Update At
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set the value of Update At
     *
     * @param \DateTime updateAt
     *
     * @return self
     */
    public function setUpdateAt(\DateTime $updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get the value of many to one User
     *
     * @return \NaS\DevaPsicolegs\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of many to one User
     *
     * @param \NaS\DevaPsicolegs\Entity\User user
     *
     * @return self
     */
    public function setUser(\NaS\DevaPsicolegs\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setCreateAtValue()
    {
      $this->createAt = new \DateTime();
      $this->updateAt = new \DateTime();
    }

    /**
    * @ORM\PreUpdate
    */
    public function setUpdateAtValue()
    {
      $this->updateAt = new \DateTime();
    }

    /**
     * Get many to Many Tags
     *
     * @return  \NaS\DevaPsicolegs\Entity\Tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param \NaS\DevaPsicolegs\Entity\Tags
     * @return void
     */
    public function addTag(\NaS\DevaPsicolegs\Entity\Tags $tag)
    {
        $tag->addPost($this);
        $this->tags[] = $tag;
    }

    /**
     * Get many to Many PostSubCategory
     *
     * @return  \NaS\DevaPsicolegs\Entity\PostSubCategory
     */
    public function getPostSubCategory()
    {
        return $this->postSubCategory;
    }

    /**
     * @param \NaS\DevaPsicolegs\Entity\PostSubCategory
     * @return void
     */
    public function addPostSubCategory(\NaS\DevaPsicolegs\Entity\PostSubCategory $postSubCategory)
    {
        $postSubCategory->addPost($this);
        $this->postSubCategory[] = $postSubCategory;
    }

    /**
     * Get the value of Many to Many PostCategory
     *
     * @return \NaS\DevaPsicolegs\Entity\PostCategory
     */
    public function getPostCategory()
    {
        return $this->postCategory;
    }

    /**
     * @param \NaS\DevaPsicolegs\Entity\PostCategory
     * @return void
     */
    public function addPostCategory(\NaS\DevaPsicolegs\Entity\PostCategory $postCategory)
    {
        $postCategory->addPost($this);
        $this->postCategory[] = $postCategory;
    }

    /**
     * Remove PostCategory
     *
     * @param \NaS\DevaPsicolegs\Entity\PostCategory $postCategory
     */
    public function removePostCategory(\NaS\DevaPsicolegs\Entity\PostCategory $postCategory)
    {
        $this->postCategory->removeElement($postCategory);
    }



    /**
     * Get one to many PostGalleryImage
     *
     * @return  \NaS\DevaPsicolegs\Entity\PostGalleryImage
     */
    public function getPostGalleryImage()
    {
        return $this->postGalleryImage;
    }

    /**
     * Add postGalleryImage
     *
     * @param \NaS\DevaPsicolegs\Entity\PostGalleryImage $postGalleryImage
     *
     * @return Panelistas
     */
    public function addPostGalleryImage(\NaS\DevaPsicolegs\Entity\PostGalleryImage $postGalleryImage)
    {
        $postGalleryImage->setPost($this);
        $this->postGalleryImage[] = $postGalleryImage;

        // return $this;
    }

    /**
     * Get the value of code
     *
     * @return  string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @param  string  $code
     *
     * @return  self
     */
    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get one to One PostSeo
     *
     * @return  \NaS\DevaPsicolegs\Entity\PostSeo
     */
    public function getPostSeo()
    {
        return $this->postSeo;
    }

    /**
     * Set one to One PostSeo
     *
     * @param  \NaS\DevaPsicolegs\Entity\PostSeo  $postSeo  One to One PostSeo
     *
     * @return  self
     */
    public function setPostSeo(\NaS\DevaPsicolegs\Entity\PostSeo $postSeo)
    {
        $this->postSeo = $postSeo;
        $postSeo->setPost($this);

        return $this;
    }

    /**
     * Get the value of headline
     *
     * @return  string
     */ 
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * Set the value of headline
     *
     * @param  string  $headline
     *
     * @return  self
     */ 
    public function setHeadline($headline)
    {
        $this->headline = $headline;

        return $this;
    }

    /**
     * Get the value of active
     *
     * @return  string
     */ 
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the value of active
     *
     * @param  string  $active
     *
     * @return  self
     */ 
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get the value of draft
     *
     * @return  string
     */ 
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * Set the value of draft
     *
     * @param  string  $draft
     *
     * @return  self
     */ 
    public function setDraft(string $draft)
    {
        $this->draft = $draft;

        return $this;
    }

    /**
     * Get the value of rating
     *
     * @return  decimal
     */ 
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set the value of rating
     *
     * @param  decimal  $rating
     *
     * @return  self
     */ 
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get one to Many - PostComment
     *
     * @return  \NaS\DevaPsicolegs\Entity\PostComment
     */ 
    public function getPostComment()
    {
        return $this->postComment;
    }

    /**
     * Set one to Many - PostComment
     *
     * @param  \NaS\DevaPsicolegs\Entity\PostComment  $postComment  One to Many - PostComment
     *
     * @return  self
     */ 
    public function setPostComment(\NaS\DevaPsicolegs\Entity\PostComment $postComment)
    {
        $this->postComment = $postComment;

        return $this;
    }
}

?>
