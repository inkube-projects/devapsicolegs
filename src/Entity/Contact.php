<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Tags Entoty
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\ContactRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Contact
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="names", type="text", length=200, nullable=false)
     */
    private $names;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=180, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="text", length=30, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", length=4294967295, nullable=false)
     */
    private $message;

    /**
     * @var boolean
     *
     * @ORM\Column(name="readed", type="boolean", nullable=false)
     */
    private $readed;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", length=4294967295, nullable=true)
     */
    private $note;

    /**
     * Many to one - ContactReason
     * @var \NaS\DevaPsicolegs\Entity\ContactReason
     *
     * @ORM\ManyToOne(targetEntity="ContactReason", inversedBy="contact")
     * @ORM\JoinColumn(name="reason_id", referencedColumnName="id")
     */
    private $contactReason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;


    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Names
     *
     * @return string
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * Set the value of Names
     *
     * @param string names
     *
     * @return self
     */
    public function setNames($names)
    {
        $this->names = $names;

        return $this;
    }

    /**
     * Get the value of Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param string email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of Phone
     *
     * @param string phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of Message
     *
     * @param string message
     *
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get the value of Readed
     *
     * @return boolean
     */
    public function getReaded()
    {
        return $this->readed;
    }

    /**
     * Set the value of Readed
     *
     * @param boolean readed
     *
     * @return self
     */
    public function setReaded($readed)
    {
        $this->readed = $readed;

        return $this;
    }

    /**
     * Get the value of Create At
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set the value of Create At
     *
     * @param \DateTime createAt
     *
     * @return self
     */
    public function setCreateAt(\DateTime $createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get the value of Update At
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set the value of Update At
     *
     * @param \DateTime updateAt
     *
     * @return self
     */
    public function setUpdateAt(\DateTime $updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setCreateAtValue()
    {
        $this->createAt = new \DateTime();
        $this->updateAt = new \DateTime();
    }

    /**
    * @ORM\PreUpdate
    */
    public function setUpdateAtValue()
    {
        $this->updateAt = new \DateTime();
    }

    /**
     * Get the value of Many to one - ContactReason
     *
     * @return \NaS\DevaPsicolegs\Entity\ContactReason
     */
    public function getContactReason()
    {
        return $this->contactReason;
    }

    /**
     * Set the value of Many to one - ContactReason
     *
     * @param \NaS\DevaPsicolegs\Entity\ContactReason contactReason
     *
     * @return self
     */
    public function setContactReason(\NaS\DevaPsicolegs\Entity\ContactReason $contactReason)
    {
        $this->contactReason = $contactReason;

        return $this;
    }


    /**
     * Get the value of Note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set the value of Note
     *
     * @param string note
     *
     * @return self
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

}

?>
