<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PostCategory entity
 *
 * @ORM\Table(name="posts_category")
 * @ORM\Entity(repositoryClass="NaS\DevaPsicolegs\Repository\PostCategoryRepository")
 *
 */
class PostCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=180, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="canonical_name", type="text", length=200, nullable=false)
     */
    private $canonicalName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="cover_image", type="text", length=255, nullable=true)
     */
    private $coverImage;

    /**
     * Many to many Post
     * @var \NaS\DevaPsicolegs\Entity\Post
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="postCategory")
     */
    private $post;

    /**
     * One to many PostSubCategory
     * @var \NaS\DevaPsicolegs\Entity\PostSubCategory
     *
     * @ORM\OneToMany(targetEntity="PostSubCategory", mappedBy="postCategory", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $postSubCategory;


    public function __construct() {
        $this->post = new ArrayCollection();
        $this->postSubCategory = new ArrayCollection();
    }


    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Cover Image
     *
     * @return string
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * Set the value of Cover Image
     *
     * @param string coverImage
     *
     * @return self
     */
    public function setCoverImage($coverImage)
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    /**
     * Get the value of Many to many Post
     *
     * @return \NaS\DevaPsicolegs\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Get many to many Post
     *
     * @return \NaS\DevaPsicolegs\Entity\Post
     */
    public function addPost(\NaS\DevaPsicolegs\Entity\Post $post)
    {
        $this->post[] = $post;
    }

    /**
     * Get the value of One to many PostSubCategory
     *
     * @return \NaS\DevaPsicolegs\Entity\PostSubCategory
     */
    public function getPostSubCategory()
    {
        return $this->postSubCategory;
    }

    /**
     * Set the value of One to many PostSubCategory
     *
     * @param \NaS\DevaPsicolegs\Entity\PostSubCategory postSubCategory
     *
     * @return self
     */
    public function setPostSubCategory(\NaS\DevaPsicolegs\Entity\PostSubCategory $postSubCategory)
    {
        $this->postSubCategory = $postSubCategory;

        return $this;
    }

    /**
     * Get the value of Canonical Name
     *
     * @return string
     */
    public function getCanonicalName()
    {
        return $this->canonicalName;
    }

    /**
     * Set the value of Canonical Name
     *
     * @param string canonicalName
     *
     * @return self
     */
    public function setCanonicalName($canonicalName)
    {
        $this->canonicalName = $canonicalName;

        return $this;
    }
}


?>
