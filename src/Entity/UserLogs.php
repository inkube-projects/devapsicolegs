<?php

namespace NaS\DevaPsicolegs\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * UserLogs
 *
 * @ORM\Table(name="user_logs")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class UserLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=180, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="text", length=65535, nullable=false)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_access", type="string", length=20, nullable=true)
     */
    private $ipAccess;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_proxy", type="string", length=20, nullable=true)
     */
    private $ipProxy;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_company", type="string", length=20, nullable=true)
     */
    private $ipCompany;

    /**
     * @var \NaS\DevaPsicolegs\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userLogs")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;


    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of username
     *
     * @return  string
     */ 
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @param  string  $username
     *
     * @return  self
     */ 
    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of action
     *
     * @return  string
     */ 
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set the value of action
     *
     * @param  string  $action
     *
     * @return  self
     */ 
    public function setAction(string $action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get the value of ipAccess
     *
     * @return  string
     */ 
    public function getIpAccess()
    {
        return $this->ipAccess;
    }

    /**
     * Set the value of ipAccess
     *
     * @param  string  $ipAccess
     *
     * @return  self
     */ 
    public function setIpAccess($ipAccess)
    {
        $this->ipAccess = $ipAccess;

        return $this;
    }

    /**
     * Get the value of ipProxy
     *
     * @return  string
     */ 
    public function getIpProxy()
    {
        return $this->ipProxy;
    }

    /**
     * Set the value of ipProxy
     *
     * @param  string  $ipProxy
     *
     * @return  self
     */ 
    public function setIpProxy($ipProxy)
    {
        $this->ipProxy = $ipProxy;

        return $this;
    }

    /**
     * Get the value of ipCompany
     *
     * @return  string
     */ 
    public function getIpCompany()
    {
        return $this->ipCompany;
    }

    /**
     * Set the value of ipCompany
     *
     * @param  string  $ipCompany
     *
     * @return  self
     */ 
    public function setIpCompany($ipCompany)
    {
        $this->ipCompany = $ipCompany;

        return $this;
    }

    /**
     * Get the value of user
     *
     * @return  \NaS\DevaPsicolegs\Entity\User
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @param  \NaS\DevaPsicolegs\Entity\User  $user
     *
     * @return  self
     */ 
    public function setUser(\NaS\DevaPsicolegs\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of createAt
     *
     * @return  \DateTime
     */ 
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set the value of createAt
     *
     * @param  \DateTime  $createAt
     *
     * @return  self
     */ 
    public function setCreateAt(\DateTime $createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setCreateAtValue()
    {
      $this->createAt = new \DateTime();
    }
}
