<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Web\PostController;
use NaS\DevaPsicolegs\Services\Web\PostService;

$arr_role_access = ['ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER', 'ROLE_ANONYMOUS'];


// Publicaciones
Route::set('/blog/{page}', ['GET'], false, $arr_role_access, function(){
    $request = new Request;
    $postController = new PostController;
    $postController->listAction($request);
});

Route::set('/ajax/post/comment', ['POST'], false, $arr_role_access, function(){
    $postService = new PostService;
    $postService->em->getConnection()->beginTransaction();

    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'comment');

        $data = $postService->persist();

        $arr_comment = [
            'name' => $data->getNames(),
            'comment' => $data->getComment(),
            'create' => $data->getCreateAt()->format('d-m-Y H:i')
        ];

        $arr_response = array('status' => 'OK', 'message' => "El comentario ha sido cargado", 'data' => $arr_comment);
        $authorization->destroyToken($_POST['t_form']);
        $postService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $postService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

?>
