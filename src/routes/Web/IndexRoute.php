<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\JWThandler;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Web\IndexController;
use NaS\DevaPsicolegs\Controller\Web\PostController;

$arr_role_access = ['ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER', 'ROLE_ANONYMOUS'];

// Home
Route::set('/{entry}/{page}', ['GET'], false, $arr_role_access, function(){
    $postController = new PostController;
    $request = new Request;
    $em = getEntityManager();

    $entry = secure_mysql(sanitize($request->getParameter('entry')));
    $category = $em->getRepository('NaS\DevaPsicolegs\Entity\PostCategory')->findOneBy(['canonicalName' => $entry]);
    $subCategory = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSubCategory')->findOneBy(['canonicalName' => $entry]);
    $postSeo = $em->getRepository('NaS\DevaPsicolegs\Entity\PostSeo')->findOneBy(['url' => $entry]);

    if ($category) { // Listado por categoría
        // breadcrumb
        $arr_breadcrumb = [
            0 => [
                'name' => $category->getName(),
                'url' => $category->getCanonicalName()
            ]
        ];
        $arr_filters_ext = ['src_category' => [$category->getId()]]; // Filtro

        $postController->listAction($request, $category->getCanonicalName(), $arr_filters_ext, $arr_breadcrumb);
    } elseif ($subCategory) { // Por subcategoría
        // breadcrumb
        $arr_breadcrumb = [
            0 => [
                'name' => $subCategory->getPostCategory()->getName(),
                'url' => $subCategory->getPostCategory()->getCanonicalName()
            ],
            1 => [
                'name' => $subCategory->getName(),
                'url' => $subCategory->getCanonicalName()
            ]
        ];
        $arr_filters_ext = ['src_subcategory' => [$subCategory->getId()]]; // Filtro

        $postController->listAction($request, $subCategory->getCanonicalName(), $arr_filters_ext, $arr_breadcrumb);
    } elseif ($postSeo) { // Detalle de la publicación
        $postController->showAction($postSeo->getPost()->getId());
    } else {
        $indexController = new IndexController;
        $indexController->homeAction();
    }
});

Route::set('/cookies', ['GET'], false, $arr_role_access, function(){
    $indexController = new IndexController;
    $indexController->cookiesAction();
});

Route::set('/legal', ['GET'], false, $arr_role_access, function(){
    $indexController = new IndexController;
    $indexController->legalAction();
});

?>
