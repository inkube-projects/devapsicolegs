<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\JWThandler;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Web\SexologyController;

$arr_role_access = ['ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER', 'ROLE_ANONYMOUS'];

Route::set('/sexologia', ['GET'], false, $arr_role_access, function(){
    $sexologyController = new SexologyController;
    $sexologyController->introductionAction();
});

Route::set('/educacion-sexual', ['GET'], false, $arr_role_access, function(){
    $sexologyController = new SexologyController;
    $sexologyController->sexualEducationAction();
});

Route::set('/consueling', ['GET'], false, $arr_role_access, function(){
    $sexologyController = new SexologyController;
    $sexologyController->consuelingAction();
});

Route::set('/sexologia-clinica', ['GET'], false, $arr_role_access, function(){
    $sexologyController = new SexologyController;
    $sexologyController->clinicalSexologyAction();
});

?>
