<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\JWThandler;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Web\PresentationController;


$arr_role_access = ['ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER', 'ROLE_ANONYMOUS'];

// El centro
Route::set('/el-centro', ['GET'], false, $arr_role_access, function(){
    $presentationController = new PresentationController;
    $presentationController->centerAction();
});

?>
