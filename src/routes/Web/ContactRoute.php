<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\JWThandler;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Web\ContactController;
use NaS\DevaPsicolegs\Services\Web\ContactService;

$arr_role_access = ['ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER', 'ROLE_ANONYMOUS'];

// Vista de contacto
Route::set('/contacto', ['GET'], false, $arr_role_access, function(){
    $contactController = new ContactController;
    $contactController->contactAction();
});

// Se guarda el mensaje
Route::set('/ajax/contact', ['POST'], false, $arr_role_access, function(){
    $contactService = new ContactService;
    $contactService->em->getConnection()->beginTransaction();

    try {
        // $authorization = new Authorization;
        // $authorization->verifyFormToken($_POST['t_form'], 'contact');

        $data = $contactService->persist();

        $arr_response = array('status' => 'OK', 'message' => "El mensaje ha sido enviado");
        // $authorization->destroyToken($_POST['t_form']);
        $contactService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $contactService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

// Se guarda el mensaje
Route::set('/test/contact', ['POST'], false, $arr_role_access, function(){
    $contactService = new ContactService;
    $contactService->em->getConnection()->beginTransaction();

    $arr_data = [
        'names' => 'Ali Guevara',
        'email' => 'ali.jose118@gmail.com',
        'phone' => '04125588865',
        'subject' => 'Ampliar información',
        'message' => 'lorem ipsum'
    ];
    $arr_to['ali.jose118@gmail.com'] = "Ali Guevara";
    $contactService->sendEmail("Web/Contact/contact-email-template.twig", $arr_data, "test", $arr_to);
});

?>
