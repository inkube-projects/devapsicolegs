<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\JWThandler;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Web\PsychologyController;

$arr_role_access = ['ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER', 'ROLE_ANONYMOUS'];

Route::set('/psicologia-clinica', ['GET'], false, $arr_role_access, function(){
    $psychologyController = new PsychologyController;
    $psychologyController->psychologyAction();
});

Route::set('/psicoterapia-infanto-juvenil', ['GET'], false, $arr_role_access, function(){
    $psychologyController = new PsychologyController;
    $psychologyController->psychotherapyAction();
});

Route::set('/psicologia-educativa', ['GET'], false, $arr_role_access, function(){
    $psychologyController = new PsychologyController;
    $psychologyController->educationalPsychologyAction();
});

?>
