<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\ContactController;
use NaS\DevaPsicolegs\Services\Admin\ContactService;

$arr_roles_access = ['ROLE_ADMIN', 'ROLE_MODERATOR'];

// Listado de mensjaes
Route::set('/inkcms/contact', ['GET'], true, $arr_roles_access, function(){
    $contactController = new ContactController;
    $contactController->listAction();
});

// Ver mensaje
Route::set('/inkcms/contact/message/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $contactController = new ContactController;
    $request = new Request;
    $id = $request->getParameter('id');
    $contactController->editAction($id);
});

Route::set('/inkcms/ajax/contact/message/update/{id}', ['POST'], true, $arr_roles_access, function(){
    $contactController = new ContactController;
    $contactService = new ContactService;
    $contactService->em->getConnection()->beginTransaction();

    try {
        $request = new Request;
        $id = $request->getParameter('id');

        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'contact');

        $data = $contactService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "El mensaje ha sido editado correctamente", "data" => $id, "token" => $contactController->createFormToken('contact'));
        $authorization->destroyToken($_POST['t_form']);
        $contactService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $contactService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/contact/message/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $contactService = new ContactService;
        $data = $contactService->remove();

        $arr_response = array('status' => 'OK', 'message' => "El mensaje ha sido eliminado");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

?>
