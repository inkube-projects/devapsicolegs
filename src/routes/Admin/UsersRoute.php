<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\UserController;
use NaS\DevaPsicolegs\Services\Admin\UsersService;

$arr_roles_access = ['ROLE_ADMIN'];


Route::set('/inkcms/users', ['GET'], true, $arr_roles_access, function(){
    $userController = new UserController;
    $userController->listAction();
});

Route::set('/inkcms/user/add', ['GET'], true, $arr_roles_access, function(){
    $userController = new UserController;
    $userController->addAction();
});

Route::set('/inkcms/user/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $userController = new UserController;
    $id = $request->getParameter('id');
    $userController->editAction($id);
});

Route::set('/inkcms/ajax/user/new', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'user');

        $usersService = new UsersService;
        $data = $usersService->persist();

        $arr_response = array('status' => 'OK', 'message' => "Usuario creado correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/user/update/{id}', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'user');

        $request = new Request;
        $id = $request->getParameter('id');

        $usersService = new UsersService;
        $data = $usersService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "Usuario editado correctamente", "data" => $id);
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/user/uploadImage', ['POST'], true, $arr_roles_access, function(){ // Imagen temporal
    try {
        $usersService = new UsersService;
        $data = $usersService->uploadProfileImage(1);

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/user/proccessImage', ['POST'], true, $arr_roles_access, function(){ // Se procesa la imagen
    try {
        $usersService = new UsersService;
        $data = $usersService->uploadProfileImage(2);

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/user/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $usersService = new UsersService;
        $data = $usersService->remove();

        $arr_response = array('status' => 'OK', 'message' => "El usuario se ha eliminado correctamente");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

// Perfil ------------------------------------------------------------------------------------------------------

Route::set('/inkcms/profile', ['GET'], true, $arr_roles_access, function(){
    $userController = new UserController;
    $userController->profileAction();
});

Route::set('/inkcms/ajax/profile', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'profile');

        $usersService = new UsersService;
        $data = $usersService->profile();

        $arr_response = array('status' => 'OK', 'message' => "Perfil editado correctamente");
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

?>
