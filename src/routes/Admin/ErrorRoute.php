<?php

use NaS\Classes\Route;

$arr_role_access = ['ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER', 'ROLE_ANONYMOUS'];

Route::set('/inkcms/404', ['GET'], false, $arr_role_access, function(){
    echo '404';
});

Route::set('/inkcms/500', ['GET'], false, $arr_role_access, function(){
    echo '500';
});


?>
