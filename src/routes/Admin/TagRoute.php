<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\TagController;
use NaS\DevaPsicolegs\Services\Admin\TagService;

$arr_roles_access = ['ROLE_ADMIN', 'ROLE_MODERATOR'];

Route::set('/inkcms/tags', ['GET'], true, $arr_roles_access, function(){
    $tagController = new TagController;
    $tagController->listAction();
});

Route::set('/inkcms/tag/add', ['GET'], true, $arr_roles_access, function(){
    $tagController = new TagController;
    $tagController->addAction();
});

Route::set('/inkcms/tag/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");

    $tagController = new TagController;
    $tagController->editAction($id);
});

Route::set('/inkcms/ajax/tag/new', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'tags');

        $tagService = new TagService;
        $data = $tagService->persist();

        $arr_response = array('status' => 'OK', 'message' => "La etiqueta ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/tag/update/{id}', ['POST'], true, $arr_roles_access, function(){
    try {
        $request = new Request;
        $id = $request->getParameter('id');

        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'tags');

        $tagService = new TagService;
        $data = $tagService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "La etiqueta ha sido editada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/tag/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $tagService = new TagService;
        $data = $tagService->remove();

        $arr_response = array('status' => 'OK', 'message' => "La etiqueta ha sido eliminada");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

?>
