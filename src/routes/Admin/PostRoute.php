<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\PostController;
use NaS\DevaPsicolegs\Controller\Admin\PostCategoryController;
use NaS\DevaPsicolegs\Controller\Admin\PostSubCategoryController;
use NaS\DevaPsicolegs\Services\Admin\PostCategoryService;
use NaS\DevaPsicolegs\Services\Admin\PostSubCategoryService;
use NaS\DevaPsicolegs\Services\Admin\PostService;
use NaS\DevaPsicolegs\Controller\Admin\PostCommentController;

$arr_roles_access = ['ROLE_ADMIN', 'ROLE_MODERATOR'];


//////////////////////////////////////////////
/// Publicaciones                          ///
// ///////////////////////////////////////////
Route::set('/inkcms/posts/{draft}/{headline}/{active}/{category}/{subcategory}/{tags}/{user}/{dateFrom}/{dateTo}', ['GET'], true, $arr_roles_access, function(){
    $postController = new PostController;
    $request = new Request;
    $postController->listAction($request);
});

Route::set('/inkcms/post/add', ['GET'], true, $arr_roles_access, function(){
    $postController = new PostController;
    $postController->addAction();
});

Route::set('/inkcms/post/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = $request->getParameter('id');

    $postController = new PostController;
    $postController->editAction($id);
});

Route::set('/inkcms/ajax/post/new', ['POST'], true, $arr_roles_access, function(){
    $postService = new PostService;
    $postService->em->getConnection()->beginTransaction();

    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'post');

        $data = $postService->persist();

        $arr_response = array('status' => 'OK', 'message' => "La publicación ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
        $postService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $postService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/update/{id}', ['POST'], true, $arr_roles_access, function(){
    $postService = new PostService;
    $postService->em->getConnection()->beginTransaction();

    try {
        $request = new Request;
        $id = $request->getParameter('id');

        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'post');

        $data = $postService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "La publicación ha sido editada correctamente", "data" => $id);
        $authorization->destroyToken($_POST['t_form']);
        $postService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $postService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});


Route::set('/inkcms/ajx/post/getSubcategories', ['POST'], true, $arr_roles_access, function(){
    try {
        $postService = new PostService;
        $data = $postService->getSubcateogries();

        $arr_response = array('status' => 'OK', 'message' => "Listando subcategorías", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/uploadImage', ['POST'], true, $arr_roles_access, function(){ // Imagen temporal
    try {
        $postService = new PostService;
        $data = $postService->uploadProfileImage(1);

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/proccessImage', ['POST'], true, $arr_roles_access, function(){ // Se procesa la imagen
    try {
        $postService = new PostService;
        $data = $postService->uploadProfileImage(2);

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/CKeditor/images', ['POST'], true, $arr_roles_access, function(){
    try {
        $postService = new PostService;
        $data = $postService->uploadCKeditorImage();

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "url" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/gallery', ['POST'], true, $arr_roles_access, function(){
    try {
        $postService = new PostService;
        $data = $postService->uploadGallery();

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/gallery/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $postService = new PostService;
        isValidString($_POST['img'], "No se permiten caracteres especiales", "#$%^*\|");
        isValidOption(1, 2, $_POST['act'], "La opción no es válida");
        $image = secure_mysql($_POST['img']);
        $act = @number_format($_POST['act'],0,"","");

        $data = $postService->removeGalleryImage($act, $image);
        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $postService = new postService;
        $postService->remove();

        $arr_response = array('status' => 'OK', 'message' => "La publicación ha sido eliminada correctamente");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

//////////////////////////////////////////////
/// Categoria de publicaciones             ///
// ///////////////////////////////////////////
Route::set('/inkcms/post/categories', ['GET'], true, $arr_roles_access, function(){
    $postCategoryController = new PostCategoryController;
    $postCategoryController->listAction();
});

Route::set('/inkcms/post/category/add', ['GET'], true, $arr_roles_access, function(){
    $postCategoryController = new PostCategoryController;
    $postCategoryController->addAction();
});

Route::set('/inkcms/post/category/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $postCategoryController = new PostCategoryController;
    $id = $request->getParameter('id');
    $postCategoryController->editAction($id);
});

Route::set('/inkcms/ajax/post/category/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $postCategoryService = new PostCategoryService;
        $data = $postCategoryService->remove();

        $arr_response = array('status' => 'OK', 'message' => "La categoría ha sido eliminada");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/category/new', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'postCategory');

        $postCategoryService = new PostCategoryService;
        $data = $postCategoryService->persist();

        $arr_response = array('status' => 'OK', 'message' => "La categoría ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/category/update/{id}', ['POST'], true, $arr_roles_access, function(){
    try {
        $request = new Request;
        $id = $request->getParameter('id');

        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'postCategory');

        $postCategoryService = new PostCategoryService;
        $data = $postCategoryService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "La categoría ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/category/uploadImage', ['POST'], true, $arr_roles_access, function(){ // Imagen temporal
    try {
        $postCategoryService = new PostCategoryService;
        $data = $postCategoryService->uploadProfileImage(1);

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/category/proccessImage', ['POST'], true, $arr_roles_access, function(){ // Se procesa la imagen
    try {
        $postCategoryService = new PostCategoryService;
        $data = $postCategoryService->uploadProfileImage(2);

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "data" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

//////////////////////////////////////////////
/// Sub categorías                         ///
// ///////////////////////////////////////////
Route::set('/inkcms/post/subcategories', ['GET'], true, $arr_roles_access, function(){
    $postSubCategoryController = new PostSubCategoryController;
    $postSubCategoryController->listAction();
});

Route::set('/inkcms/post/subcategory/add', ['GET'], true, $arr_roles_access, function(){
    $postSubCategoryController = new PostSubCategoryController;
    $postSubCategoryController->addAction();
});

Route::set('/inkcms/post/subcategory/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = $request->getParameter('id');
    $postSubCategoryController = new PostSubCategoryController;
    $postSubCategoryController->editAction($id);
});

Route::set('/inkcms/ajax/post/subcategory/new', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'postSubCategory');

        $postSubCategoryService = new PostSubCategoryService;
        $data = $postSubCategoryService->persist();

        $arr_response = array('status' => 'OK', 'message' => "La sub-categoría ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/subcategory/update/{id}', ['POST'], true, $arr_roles_access, function(){
    try {
        $request = new Request;
        $id = $request->getParameter('id');

        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'postSubCategory');

        $postSubCategoryService = new PostSubCategoryService;
        $data = $postSubCategoryService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "La sub-categoría ha sido editada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/post/subcategory/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $postSubCategoryService = new PostSubCategoryService;
        $data = $postSubCategoryService->remove();

        $arr_response = array('status' => 'OK', 'message' => "La categoría ha sido eliminada");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});


//////////////////////////////////////////////
/// Comentarios                             //
//////////////////////////////////////////////
Route::set('/inkcms/post/comments', ['GET'], true, $arr_roles_access, function(){
    $postCommentController = new PostCommentController;
    $postCommentController->listAction();
});

Route::set('/inkcms/post/comment/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = $request->getParameter('id');
    $postCommentController = new PostCommentController;
    $postCommentController->editAction($id);
});

Route::set('/inkcms/ajax/post/comment/update/{id}', ['POST'], true, $arr_roles_access, function(){
    try {
        $request = new Request;
        $id = $request->getParameter('id');

        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'comment');

        $postService = new PostService;
        $data = $postService->updateComment($id);

        $arr_response = array('status' => 'OK', 'message' => "El coemntario ha sido editado", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

?>
