<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\SliderController;
use NaS\DevaPsicolegs\Services\Admin\SliderService;

$arr_roles_access = ['ROLE_ADMIN', 'ROLE_MODERATOR'];

//////////////////////////////////////////////
/// Slider                                 ///
// ///////////////////////////////////////////
Route::set('/inkcms/sliders', ['GET'], true, $arr_roles_access, function(){
    $sliderController = new SliderController;
    $sliderController->listAction();
});

Route::set('/inkcms/slider/add', ['GET'], true, $arr_roles_access, function(){
    $sliderController = new SliderController;
    $sliderController->addAction();
});

Route::set('/inkcms/slider/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = $request->getParameter('id');

    $sliderController = new SliderController;
    $sliderController->editAction($id);
});

Route::set('/inkcms/ajax/slider/new', ['POST'], true, $arr_roles_access, function(){
    $sliderService = new SliderService;

    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'slider');

        $data = $sliderService->persist();

        $arr_response = array('status' => 'OK', 'message' => "La imagen ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/slider/update/{id}', ['POST'], true, $arr_roles_access, function(){
    $sliderService = new SliderService;
    $request = new Request;
    $id = $request->getParameter('id');

    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'slider');

        $data = $sliderService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "La imagen ha sido editada correctamente", "data" => $id);
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});


?>
