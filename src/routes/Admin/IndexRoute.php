<?php

use NaS\Classes\Route;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\IndexController;
use NaS\DevaPsicolegs\Services\Admin\LoginService;

$arr_roles_access_anonymous = ['ROLE_ANONYMOUS'];
$arr_roles_access = ['ROLE_ADMIN', 'ROLE_MODERATOR'];

Route::set('/inkcms/login', ['GET'], false, $arr_roles_access_anonymous, function(){
    $indexController = new IndexController;
    $indexController->loginAction();
});

Route::set('/inkcms/denied', ['GET'], false, $arr_roles_access_anonymous, function(){
    $indexController = new IndexController;
    $s_alert = (isset($_SESSION['ERROR_SESSION_VALIDATEd'])) ? $_SESSION['ERROR_SESSION_VALIDATEd'] : NULL;
    $indexController->loginAction($s_alert);
});

Route::set('/inkcms', ['GET'], true, $arr_roles_access, function(){
    $indexController = new IndexController;
    $indexController->homeAction();
});

Route::set('/inkcms/logout', ['GET'], true, $arr_roles_access, function(){
    $indexController = new IndexController;
    $indexController->logOutAction();
});

Route::set('/inkcms/ajax/login', ['POST'], false, $arr_roles_access_anonymous, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'login');

        $loginService = new LoginService;
        $loginService->login();

        $arr_response = array('status' => 'OK', 'message' => "Inicio de sesión correcto");
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});
?>
