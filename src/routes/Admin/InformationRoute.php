<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\InformationController;
use NaS\DevaPsicolegs\Services\Admin\InformationService;

$arr_roles_access = ['ROLE_ADMIN', 'ROLE_MODERATOR'];

Route::set('/inkcms/information/history', ['GET'], true, $arr_roles_access, function(){
    $informationController = new InformationController;
    $informationController->historyAction();
});

Route::set('/inkcms/information/team', ['GET'], true, $arr_roles_access, function(){
    $informationController = new InformationController;
    $informationController->teamAction();
});

Route::set('/inkcms/information/team/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");

    $informationController = new InformationController;
    $informationController->editMemberAction($id);
});

Route::set('/inkcms/information/services', ['GET'], true, $arr_roles_access, function(){
    $informationController = new InformationController;
    $informationController->servicesAction();
});

Route::set('/inkcms/information/service/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");

    $informationController = new InformationController;
    $informationController->editServiceAction($id);
});

Route::set('/inkcms/information/subservices', ['GET'], true, $arr_roles_access, function(){
    $informationController = new InformationController;
    $informationController->listSubServicesAction();
});

Route::set('/inkcms/information/subservice/add', ['GET'], true, $arr_roles_access, function(){
    $informationController = new InformationController;
    $informationController->addSubServicesAction();
});

Route::set('/inkcms/information/subservice/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = @number_format($request->getParameter('id'),0,"","");

    $informationController = new InformationController;
    $informationController->editSubServicesAction($id);
});

// AJAX ------------------------------------------------------------------------------------------------------------

Route::set('/inkcms/ajx/information/uploadImageCK', ['GET'], true, $arr_roles_access, function(){
    try {
        $informationService = new InformationService;
        $data = $informationService->uploadCKeditorImage();

        $arr_response = array('status' => 'OK', 'message' => "Carga de la imagen", "url" => $data);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/information/{act}', ['POST'], true, $arr_roles_access, function(){
    $request = new Request;
    $act = $request->getParameter('act');
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();

    try {
        $authorization = new Authorization;

        if ($act == 1) {
            $authorization->verifyFormToken($_POST['t_form'], 'history');
            $informationService->persistHistory();
        } elseif ($act == 2) {
            $authorization->verifyFormToken($_POST['t_form'], 'team');
            $informationService->persistTeam();
        } elseif ($act == 3) {
            $authorization->verifyFormToken($_POST['t_form'], 'services');
            $informationService->persistServices();
        } else {
            throw new \Exception("Opción inválida", 400);
        }


        $arr_response = array('status' => 'OK', 'message' => "La infromación ha sido agregada correctamente");
        $authorization->destroyToken($_POST['t_form']);
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/team/edit/{id}', ['POST'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();

    try {
        $authorization = new Authorization;

        $authorization->verifyFormToken($_POST['t_form'], 'team');
        $informationService->updateMember($id);

        $arr_response = array('status' => 'OK', 'message' => "El miembro ha sido editado correctamente");
        $authorization->destroyToken($_POST['t_form']);
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/team/remove', ['POST'], true, $arr_roles_access, function(){
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();
    try {
        $informationService->removeMember();

        $arr_response = array('status' => 'OK', 'message' => "El miembro ha sido eliminada correctamente");
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/service/edit/{id}', ['POST'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();

    try {
        $authorization = new Authorization;

        $authorization->verifyFormToken($_POST['t_form'], 'service');
        $informationService->updateService($id);

        $arr_response = array('status' => 'OK', 'message' => "El servicio ha sido editado correctamente");
        $authorization->destroyToken($_POST['t_form']);
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/service/remove', ['POST'], true, $arr_roles_access, function(){
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();
    try {
        $informationService->removeService();

        $arr_response = array('status' => 'OK', 'message' => "El servicio ha sido eliminado correctamente");
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/subservice/new', ['POST'], true, $arr_roles_access, function(){
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();

    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'subservice');

        $data = $informationService->persistSubservice();

        $arr_response = array('status' => 'OK', 'message' => "El sub-servicio ha sido agregado correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/subservice/update/{id}', ['POST'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();

    try {
        $authorization = new Authorization;

        $authorization->verifyFormToken($_POST['t_form'], 'subservice');
        $informationService->updateSubService($id);

        $arr_response = array('status' => 'OK', 'message' => "El servicio ha sido editado correctamente");
        $authorization->destroyToken($_POST['t_form']);
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/information/subservice/remove', ['POST'], true, $arr_roles_access, function(){
    $informationService = new InformationService;
    $informationService->em->getConnection()->beginTransaction();
    try {
        $informationService->removeSubService();

        $arr_response = array('status' => 'OK', 'message' => "El servicio ha sido eliminado correctamente");
        $informationService->em->getConnection()->commit();
    } catch (\Exception $e) {
        $informationService->em->getConnection()->rollBack();
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

?>
