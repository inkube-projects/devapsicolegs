<?php

use NaS\Classes\Route;
use NaS\Classes\Request;
use NaS\Classes\Authorization;
use NaS\DevaPsicolegs\Controller\Admin\SeoSectionController;
use NaS\DevaPsicolegs\Controller\Admin\SeoController;
use NaS\DevaPsicolegs\Services\Admin\SeoSectionService;
use NaS\DevaPsicolegs\Services\Admin\SeoService;

$arr_roles_access = ['ROLE_ADMIN', 'ROLE_MODERATOR'];

// SEO section
Route::set('/inkcms/seo/sections', ['GET'], true, $arr_roles_access, function(){
    $seoSectionController = new SeoSectionController;
    $seoSectionController->listAction();
});

Route::set('/inkcms/seo/section/add', ['GET'], true, $arr_roles_access, function(){
    $seoSectionController = new SeoSectionController;
    $seoSectionController->addAction();
});

Route::set('/inkcms/seo/section/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");

    $seoSectionController = new SeoSectionController;
    $seoSectionController->editAction($id);
});

Route::set('/inkcms/ajax/seo/section/new', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'seoSection');

        $seoSectionService = new SeoSectionService;
        $data = $seoSectionService->persist();

        $arr_response = array('status' => 'OK', 'message' => "La sección ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/seo/section/update/{id}', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'seoSection');

        $request = new Request;
        $id = number_format($request->getParameter('id'),0,"","");

        $seoSectionService = new SeoSectionService;
        $data = $seoSectionService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "La sección ha sido agregada correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/seo/section/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $seoSectionService = new SeoSectionService;
        $data = $seoSectionService->remove();

        $arr_response = array('status' => 'OK', 'message' => "La sección ha sido eliminada");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

// SEO
Route::set('/inkcms/seo', ['GET'], true, $arr_roles_access, function(){
    $seoController = new SeoController;
    $seoController->listAction();
});

Route::set('/inkcms/seo/add', ['GET'], true, $arr_roles_access, function(){
    $seoController = new SeoController;
    $seoController->addAction();
});

Route::set('/inkcms/seo/edit/{id}', ['GET'], true, $arr_roles_access, function(){
    $request = new Request;
    $id = number_format($request->getParameter('id'),0,"","");

    $seoController = new SeoController;
    $seoController->editAction($id);
});

Route::set('/inkcms/ajax/seo/new', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'seo');

        $seoService = new SeoService;
        $data = $seoService->persist();

        $arr_response = array('status' => 'OK', 'message' => "El SEO ha sido agregado correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/seo/update/{id}', ['POST'], true, $arr_roles_access, function(){
    try {
        $authorization = new Authorization;
        $authorization->verifyFormToken($_POST['t_form'], 'seo');

        $request = new Request;
        $id = number_format($request->getParameter('id'),0,"","");

        $seoService = new SeoService;
        $data = $seoService->update($id);

        $arr_response = array('status' => 'OK', 'message' => "El SEO ha sido editado correctamente", "data" => $data->getId());
        $authorization->destroyToken($_POST['t_form']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

Route::set('/inkcms/ajax/seo/remove', ['POST'], true, $arr_roles_access, function(){
    try {
        $seoService = new SeoService;
        $data = $seoService->remove();

        $arr_response = array('status' => 'OK', 'message' => "El SEO ha sido eliminado");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }

    header('Content-Type: application/json');
    echo json_encode($arr_response);
});

?>
