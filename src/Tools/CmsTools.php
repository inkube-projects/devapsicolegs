<?php

namespace NaS\DevaPsicolegs\Tools;

use NaS\Classes\ServiceAware;

/**
 * Herramientas para el CMS
 */
class CmsTools
{
    function __construct()
    {
        $this->em = getEntityManager();
    }

    /**
     * Iniciador del servicio
     * @return void
     */
    public function execute()
    {
        $this->getContactMessagesTotal();
    }

    /**
     * Obtiene los mensajes de contacto
     * @return void
     */
    private function getContactMessagesTotal()
    {
        $em = $this->em;
        $contactRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\Contact');
        $contact = $contactRepository->getUnreadMessages();

        $_SESSION['GLOBAL_TWIG']['CmsGlobalInfo'] = ['contact' => $contact];
    }
}


?>
