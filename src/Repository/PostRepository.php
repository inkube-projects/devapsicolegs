<?php

namespace NaS\DevaPsicolegs\Repository;

/**
 * Repositorio de la entidad Post
 */
class PostRepository extends \Doctrine\ORM\EntityRepository
{
    public function getComments($post_id, $mode = "all")
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c FROM NaS\DevaPsicolegs\Entity\PostComment c WHERE c.post = :post AND c.active = :active ORDER BY c.id DESC"
            )
            ->setParameters([
                'post'   => $post_id,
                'active' => 1
            ])
            ->getResult()
        ;
    }

    /**
     * Elimina las categorías por el ID del post
     *
     * @param integer $post_id
     * @return void
     */
    public function removeCategoryByPost($post_id)
    {
        $em = $this->getEntityManager();
        $q = "DELETE FROM posts_categories WHERE post_id='{$post_id}'";
        $stmt = $em->getConnection()->prepare($q);
        $stmt->execute();
    }

    /**
     * Elimina las categorías por el ID del post
     *
     * @param integer $post_id
     * @return void
     */
    public function removeSubCategoryByPost($post_id)
    {
        $em = $this->getEntityManager();
        $q = "DELETE FROM posts_subcategories WHERE post_id='{$post_id}'";
        $stmt = $em->getConnection()->prepare($q);
        $stmt->execute();
    }

    /**
     * Elimina los tags por el ID del post
     *
     * @param integer $post_id
     * @return void
     */
    public function removeTagByPost($post_id)
    {
        $em = $this->getEntityManager();
        $q = "DELETE FROM posts_tags WHERE post_id='{$post_id}'";
        $stmt = $em->getConnection()->prepare($q);
        $stmt->execute();
    }

    /**
     * Obtiene los titulares
     *
     * @return object
     */
    public function getHeadlines()
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM NaS\DevaPsicolegs\Entity\Post p WHERE p.headline = :headline AND p.active = 1 AND p.draft = 0 ORDER BY p.id DESC"
            )
            ->setParameter('headline', 1)
            ->getResult()
        ;
    }

    /**
     * Obtiene los posts segun los filtros que se indiquen
     *
     * @param array $arr_filters
     * @return void
     */
    public function getPostByFilter(array $arr_filters, $result = true)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select(['p'])->from('NaS\DevaPsicolegs\Entity\Post', 'p')->where("p.id <> ''")->groupby('p.id');
        $this
            ->srcDraft($qb, $query, $arr_filters)
            ->srcHeadline($qb, $query, $arr_filters)
            ->srcActive($qb, $query, $arr_filters)
            ->srcCategory($qb, $query, $arr_filters)
            ->srcNotCategory($qb, $query, $arr_filters)
            ->srcSubCategory($qb, $query, $arr_filters)
            ->srcTags($qb, $query, $arr_filters)
            ->srcUser($qb, $query, $arr_filters)
            ->srcDate($qb, $query, $arr_filters)
            ->order($qb, $query, $arr_filters)
            ->setLimit($qb, $query, $arr_filters)
        ;

        // echo $query->getQuery()->getSql();
        return ($result) ? $query->getQuery()->getResult() : $query;
    }

    /**
     * Agrega el filtro de borrador
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcDraft($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_draft'])) {
            if ($arr_filters['src_draft'] != "") {
                $query->andWhere('p.draft = :draft')
                    ->setParameter('draft', $arr_filters['src_draft'])
                ;
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro de titular
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcHeadline($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_headline'])) {
            if ($arr_filters['src_headline'] != "") {
                $query->andWhere('p.headline = :headline')
                    ->setParameter('headline', $arr_filters['src_headline'])
                ;
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro de Estado
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcActive($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_active'])) {
            if ($arr_filters['src_active'] != "") {
                $query->andWhere('p.active = :active')
                    ->setParameter('active', $arr_filters['src_active'])
                ;
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro de Categoría
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcCategory($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_category'][0])) {
            if ($arr_filters['src_category'][0] != "") {
                $query->innerJoin('p.postCategory','c')
                    ->andWhere($qb->expr()->in('c.id', $arr_filters['src_category']))
                ;
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro de NOT IN Categoría
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcNotCategory($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_not_category'][0])) {
            if ($arr_filters['src_not_category'][0] != "") {
                if (isset($arr_filters['src_category'][0])) {
                    $query->andWhere($qb->expr()->notIn('c.id', $arr_filters['src_not_category']));
                } else {
                    $query->innerJoin('p.postCategory','c')
                        ->andWhere($qb->expr()->notIn('c.id', $arr_filters['src_not_category']))
                    ;
                }
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro de SubCategoría
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcSubCategory($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_subcategory'][0])) {
            if ($arr_filters['src_subcategory'][0] != "") {
                $query->innerJoin('p.postSubCategory','sc')
                    ->andWhere($qb->expr()->in('sc.id', $arr_filters['src_subcategory']))
                ;
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro Tags
     *
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcTags($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_tags'][0])) {
            if ($arr_filters['src_tags'][0] != "") {
                $query->innerJoin('p.tags','t')
                    ->andWhere($qb->expr()->in('t.id', $arr_filters['src_tags']))
                ;
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro Usuario
     *
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcUser($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_user'])) {
            if ($arr_filters['src_user'] != "") {
                $query->andWhere('p.user = :user')
                    ->setParameter('user', $arr_filters['src_user'])
                ;
            }
        }

        return $this;
    }

    /**
     * Agrega el filtro Fechas
     *
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function srcDate($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['src_start_date']) || isset($arr_filters['src_end_date'])) {
            if ($arr_filters['src_start_date'] != "" || $arr_filters['src_end_date'] != "") {
                $dateFrom = ($arr_filters['src_start_date'] != "") ? to_date($arr_filters['src_start_date']) : "2019-01-01";
                $dateTo = ($arr_filters['src_end_date'] != "") ? to_date($arr_filters['src_end_date']) : date('Y-m-d');
                $exprBetween = $qb->expr()->between(
                    'p.createAt',
                    ':date_init',
                    ':date_end'
                );

                $query
                    ->andWhere($exprBetween)
                    ->setParameter('date_init', $dateFrom)
                    ->setParameter('date_end', $dateTo)
                ;
            }
        }

        return $this;
    }

    /**
     * Agregar el orden por rating
     *
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function order($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['order_rating'])) {
            if ($arr_filters['order_rating']) {
                $query->addOrderBy('p.rating', 'DESC')
                ;
            }
        }

        if (isset($arr_filters['order_id'])) {
            if ($arr_filters['order_id']) {
                $query->addOrderBy('p.id', 'DESC');
            }
        }

        return $this;
    }

    /**
     * Agrega el limite de entrada
     *
     * @param  object $qb          QueryBuilder
     * @param  object $query       Query creado
     * @param  array $arr_filters  Filtros
     * @return object
     */
    public function setLimit($qb, $query, $arr_filters)
    {
        if (isset($arr_filters['offset']) && isset($arr_filters['limit'])) {
            if ($arr_filters['offset'] != "" && $arr_filters['limit'] != "") {
                $query->setFirstResult($arr_filters['offset'])
                    ->setMaxResults($arr_filters['limit']);
                ;
            }
        }

        return $this;
    }
}


?>
