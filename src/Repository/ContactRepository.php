<?php

namespace NaS\DevaPsicolegs\Repository;

/**
 * Repositorio de la entidad Information
 */
class ContactRepository extends \Doctrine\ORM\EntityRepository
{
    public function getUnreadMessages()
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c FROM NaS\DevaPsicolegs\Entity\Contact c WHERE c.readed = 0 ORDER BY c.id DESC"
            )
            ->getResult()
        ;
    }
}


?>
