<?php

namespace NaS\DevaPsicolegs\Repository;

/**
 * Repositorio de la entidad User
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Obtiene un administrador o moderador por nombre de ususario
     * @param  string $username  Nombre de ususario
     * @return object
     */
    public function findUserByUsername($username, $role = "1, 2")
    {
        $result = $this->getEntityManager()
            ->createQuery(
                'SELECT u FROM NaS\DevaPsicolegs\Entity\User u WHERE u.username = :username AND u.role IN ('.$role.')'
            )
            ->setParameters(
                array(
                    'username' => $username
                    // 'roles' => $role
                )
            )
            ->getResult()
        ;

        return (isset($result[0])) ? $result[0] : NULL;
    }

    /**
     * Obtiene un administrador o moderador por el E-mail
     * @param  string $email Email
     * @return object
     */
    public function findUserByEmail($email, $role = "1, 2")
    {
        $result = $this->getEntityManager()
            ->createQuery(
                'SELECT u FROM NaS\DevaPsicolegs\Entity\User u WHERE u.email = :email AND u.role IN ('.$role.')'
            )
            ->setParameters(
                array(
                    'email' => $email
                    // 'roles' => $role
                )
            )
            // ->getSql()
            ->getResult()
        ;
        // echo $result; exit;
        return (isset($result[0])) ? $result[0] : NULL;
    }
    
    /**
     * Retorna usuarios por rol
     *
     * @param string $opt Opción de búsqueda [all: todos, moderators: moderadores, admin: administradores]
     * @return object
     */
    public function getModerators($opt = "all")
    {
        if ($opt == "all") {
            $roles = "1, 2";
        } elseif ($opt == "moderators") {
            $roles = "2";
        } elseif ($opt == "admin") {
            $roles = "1";
        }

        return $this->getEntityManager()
            ->createQuery(
                'SELECT u FROM NaS\DevaPsicolegs\Entity\User u WHERE u.role IN (:role)'
            )
            ->setParameters(
                array(
                    'role' => $roles
                )
            )
            ->getResult()
        ;
    }
}


?>
