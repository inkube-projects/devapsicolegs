<?php

namespace NaS\DevaPsicolegs\Repository;

/**
 * Repositorio de la entidad PostSubCategory
 */
class PostSubCategoryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Retorna el listado de subcategorías de las categorías enviadas en el arreglo
     *
     * @param  array $cat Arreglo con el ID de las categorías
     * @return object
     */
    public function findSubByCat($cat)
    {
        if (is_array($cat) && count($cat) > 0) {
            return $this->getEntityManager()
                ->createQuery(
                    "SELECT psc FROM NaS\DevaPsicolegs\Entity\PostSubCategory psc WHERE psc.postCategory IN (".implode(", ", $cat).")"
                )
                // ->setParameter('cat', implode(", ", $cat))
                ->getResult()
                // ->getSql()
            ;
        }

        return "";
    }
}


?>
