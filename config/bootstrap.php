<?php

require('./vendor/autoload.php');

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

function getEntityManager()
{
    $env = json_decode(file_get_contents("config/env.json"), true);
    $paths = array("./src/Entity/");
    $isDevMode = true;

    // the connection configuration
    $dbParams = array(
        'driver'   => $env['db_driver'],
        'user'     => $env['db_user'],
        'password' => $env['db_password'],
        'dbname'   => $env['db_name'],
        'host'     => $env['db_host'],
        'charset'  => 'utf8mb4'
    );

    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
    return EntityManager::create($dbParams, $config);
}

$entityManager = getEntityManager();

?>
