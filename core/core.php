<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// ini_set('xdebug.max_nesting_level', -1);

// nucleo del sitio web
@session_start();

$env = json_decode(file_get_contents("config/env.json"), true);
define('DB_HOST', $env['db_host']);
define('DB_NAME', $env['db_name']);
define('DB_USER', $env['db_user']);
define('DB_PASS', $env['db_password']);
define('DB_DRIVER', $env['db_driver']);
define('DB_CHARSET', $env['db_charset']);

// Datos de email
define('EMAIL_HOST', 'mail.psicologiaysexologia.es');
define('EMAIL_SENDER', 'enviosweb@psicologiaysexologia.es');
define('EMAIL_PASSWORD', '3D&45mve');

define('APP_TITLE', 'Cosmeceutical Center');
define('APP_COPY', '<strong>Copyright &copy; '.date('Y').' <a href="#">devapsicolegs</a>.</strong> All rights reserved.');
define('HTML_DIR', 'html/');

// URL
define('BASE_DOMAIN', $env['base_domain']);
define('BASE_PATH', $env['base_path']);
define('BASE_URL', BASE_DOMAIN.BASE_PATH);

// No imagen
define('APP_IMG_VIEW_NOIMAGE', BASE_URL.'/public/images/no_images');

// Usuarios
define('APP_IMG_USER', 'public/images/users');
define('APP_IMG_VIEW_USER', BASE_URL.'/public/images/users');

// Categoría de publicaciones
define('APP_IMG_POST_CATEGORY', 'public/images/post_category');
define('APP_IMG_VIEW_POST_CATEGORY', BASE_URL.'/public/images/post_category');

// Publicaciones
define('APP_IMG_POST', 'public/images/posts');
define('APP_IMG_VIEW_POST', BASE_URL.'/public/images/posts');

// Slider
define('APP_IMG_SLIDER', 'public/images/sliders');
define('APP_IMG_VIEW_SLIDER', BASE_URL.'/public/images/sliders');

// information
define('APP_IMG_INFORMATION', 'public/images/information');
define('APP_IMG_VIEW_INFORMATION', BASE_URL.'/public/images/information');

// JWT
define('JWT_SECRET', 'nBHU08CAf7cOmWEHBWT3WQ3J3bjbecX2tDiKAnDy');

date_default_timezone_set('America/Caracas');

require('core/functions/main-lib.php');
require('config/bootstrap.php');
require('vendor/autoload.php');

?>
