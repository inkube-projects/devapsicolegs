<?php

namespace NaS\Classes;

use Firebase\JWT\JWT;

/**
 * Clase con los procesos basicos de JWT
 */
class JWThandler
{
    private $secret = JWT_SECRET;

    /**
     * Genera un nuevo token
     * @param  array $data   Información en el token
     * @param  integer $iat  Tiempo de inicio
     * @param  integer $exp  Tiempo de expiración
     * @return string
     */
    public function generateToken($data, $iat = NULL, $exp = NULL)
    {
        $time = time();
        $iat = ($iat) ? $iat : time();                // Tiempo que inició el token
        $exp = ($exp) ? $exp : $iat + (60 * 60 * 60); // Tiempo que expirará el token (+60 horas)

        $token = array(
            'iat' => $iat,
            'exp' => $exp,
            'aud' => $this->Aud(),
            'data' => $data
        );
        return JWT::encode($token, $this->secret);
    }

    /**
     * Verifica el token
     * @param  string $token JWT token
     * @return boolean
     */
    public function checkToken($token)
    {
        if(empty($token)) {
            throw new \Exception("Invalid token supplied.");
        }

        $decode = JWT::decode(
            $token,
            $this->secret,
            array('HS256')
        );

        if($decode->aud !== $this->Aud()) {
            throw new \Exception("Invalid token supplied.");
        }

        return true;
    }

    /**
     * Decodifica el token
     * @param  string $token JWT token
     * @return object
     */
    public function decodeToken($token)
    {
        if(empty($token)) {
            throw new \Exception("Invalid token supplied.");
        }

        $decode = JWT::decode(
            $token,
            $this->secret,
            array('HS256')
        );

        return $decode;
    }

    /**
     * Obttiene el parametro data del token
     * @param  string $token JWT token
     * @return array
     */
    public function getData($token)
    {
        if(empty($token)) {
            throw new \Exception("Invalid token supplied.");
        }

        return JWT::decode(
            $token,
            $this->secret,
            array('HS256')
        )->data;
    }

    /**
     * Ontiene el Aud de JWT
     * @return string
     */
    private function Aud()
    {
        $aud = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }

        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();

        return sha1($aud);
    }
}


?>
