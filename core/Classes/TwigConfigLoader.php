<?php

namespace NaS\Classes;

/**
 * Carga la configuración del poryecto para twig
 */
class TwigConfigLoader
{
    /**
     * Carga la configuración de Twig
     *
     * @return object
     */
    public function loadConfig()
    {
        $loader = new \Twig\Loader\FilesystemLoader('views');
        $twig = new \Twig\Environment($loader, []);

        //Variables globales
        $twig->addGlobal('baseURL', BASE_URL);
        
        // Imágenes de publicaciones
        $twig->addGlobal('APP_IMG_VIEW_POST', APP_IMG_VIEW_POST);

        // implement whatever logic you need to determine the asset path
        $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) {
            return sprintf(BASE_URL.'/public/assets/%s', ltrim($asset, '/'));
        }));

        // implement whatever logic you need to determine the asset path
        $twig->addFunction(new \Twig_SimpleFunction('base_url', function () {
            return BASE_URL;
        }));

        // implement whatever logic you need to determine the url path
        $twig->addFunction(new \Twig_SimpleFunction('path', function ($path) {
            return sprintf(BASE_URL.'/%s', ltrim($path, '/'));
        }));

        // Funcion para las imagenes
        $twig->addFunction(new \Twig_SimpleFunction('image', function ($image_path) {
            return sprintf(BASE_URL.'/public/images/%s', ltrim($image_path, '/'));
        }));

        // Funcion isSelected para los select
        $twig->addFunction(new \Twig_SimpleFunction('isSelected', function ($value_1, $value_2) {
            return ($value_1 == $value_2) ? 'selected="selected"' : "";
        }));

        // Funcion isSelectedMultiple para Select Multiple
        $twig->addFunction(new \Twig_SimpleFunction('isSelectedMultiple', function ($arr, $val) {
            $r = "";
            if (is_array($arr)) {
                foreach ($arr as $v) {
                    if ($v == $val) { return ' selected="selected"'; }
                }
            }
            return $r;
        }));

        // Funcion isChecked para los checkbox
        $twig->addFunction(new \Twig_SimpleFunction('isChecked', function ($arr, $value) {
            return (in_array($value, $arr)) ? 'checked="checked"' : "";
        }));

        // Funcion sanitize
        $twig->addFunction(new \Twig_SimpleFunction('sanitize', function ($input) {
            return sanitize($input);
        }));

        return $twig;
    }
}


?>
