<?php

namespace NaS\Classes;

use NaS\Classes\Authorization;

/**
 * Enrutamiento del proyecto
 */
class Route
{
    public static $validRoutes = array();
    public static $parameter = array();

    public static function set($route, array $method, $aut = false, array $roles, $function)
    {
        self::$validRoutes[] = array(
            'route'       => $route,
            'method'      => $method,
            'aut'         => $aut,
            'function'    => $function,
            'accessRoles' => $roles
        );
    }

    /**
     * Obtiene e invoca el metodo de la ruta
     * @param string   $route    Ruta
     * @param array    $method   Metodo de consumir la ruta
     * @param boolean  $aut      Variable de autenticación
     * @param function $function Función
     * @return void
     */
    public static function loadRoute()
    {
        $authorization = new Authorization;
        $url = self::getCurrentUri();
        $request_method = $_SERVER['REQUEST_METHOD'];
        $flag = false;


        foreach (self::$validRoutes as $key => $route) {
            $v_param = (strpos($route['route'], "/{") === false) ? false : true;
            if ($v_param) {
                $complete_route = explode("/{", $route['route']);
                $clean_route = $complete_route[0];
            } else {
                $clean_route = $route['route'];
            }
            $clean_route = ($clean_route == "") ? "/" : $clean_route;

            if ($clean_route == $url) {
                if ($route['aut']) { $authorization->validateSession($route['accessRoles'], $route['route']); }
                $route['function']->__invoke(); exit;
            }
        }

        foreach (self::$validRoutes as $key => $route) {
            $v_param = (strpos($route['route'], "/{") === false) ? false : true;

            if ($v_param) {
                $arr_url = explode("/{", $route['route']);
                $current_route = ($arr_url[0] == "") ? "/" : $arr_url[0];
                $ext = "";
                $o_url = explode($current_route, $url);
                $arr_ext = [];
                foreach ($o_url as $key => $val) {
                    if ($key > 0) {
                        $arr_ext[] = $val;
                    }
                }

                if ($current_route != "/") {
                    $ext = implode("/", $arr_ext)."/";
                    if ($current_route.$ext == $url."/") {
                        // Se verifica la sesión
                        if ($route['aut']) { $authorization->validateSession($route['accessRoles'], $route['route']); }
                        $route['function']->__invoke(); exit;
                    }
                }
            }
        }

        foreach (self::$validRoutes as $key => $route) {
            $v_param = (strpos($route['route'], "/{") === false) ? false : true;

            if ($v_param) {
                $arr_url = explode("/{", $route['route']);
                $current_route = ($arr_url[0] == "") ? "/" : $arr_url[0];
                $ext = "";
                $o_url = explode($current_route, $url);
                $arr_ext = [];
                foreach ($o_url as $key => $val) {
                    if ($key > 0) {
                        $arr_ext[] = $val;
                    }
                }

                $ext = implode("/", $arr_ext)."/";
                if ($current_route.$ext == $url."/") {
                    // Se verifica la sesión
                    if ($route['aut']) { $authorization->validateSession($route['accessRoles'], $route['route']); }
                    $route['function']->__invoke(); exit;
                }
            }
        }
    }

    /**
     * Obtiene la ruta actual
     * @return string
     */
    public static function getCurrentUri()
    {
        $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
        if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
        $uri = '/' . trim($uri, '/');
        return $uri;
    }

    /**
     * Valida si la ruta existe
     * @return boolean
     */
    public static function validateRoute()
    {
        $flag = false;
        $arr_routes = self::$validRoutes;
        $request_method = $_SERVER['REQUEST_METHOD'];
        $url = self::getCurrentUri();

        foreach ($arr_routes as $val) {
            $pos = strpos($val['route'], "/{");

            if ($pos !== false) {
                $arr_url = explode("/{", $val['route']);
                $current_route = $arr_url[0];

                $verify_url = ($current_route."/" == $url."/") ? true : false;
                // $verify_url = strpos($url, $current_route);

                if ($verify_url) {
                    if (in_array($request_method, $val['method'])) {
                        $flag = true;
                    }
                }
            } else {
                if ($val['route'] == $url) {
                    if (in_array($request_method, $val['method'])) {
                        $flag = true;
                    }
                }
            }
        }

        return $flag;
    }
}


?>
