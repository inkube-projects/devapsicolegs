<?php

namespace NaS\Classes;

use NaS\Classes\JWThandler;
use NaS\Classes\ServiceAware;

/**
 * Proveedor de usuario
 */
class UserProvider
{
    public function __construct()
    {
        $this->token = @$_SESSION['SES_ADMIN_DEVAPSICOLEGS']['token'];
        $this->user_id = @$_SESSION['SES_ADMIN_DEVAPSICOLEGS']['id'];
        $this->username = @$_SESSION['SES_ADMIN_DEVAPSICOLEGS']['username'];
    }

    /**
     * Obtiene la informacion del usuario con una sesión abierta
     *
     * @return object
     */
    public function validateUserSession()
    {
        if (!isset($_SESSION['SES_ADMIN_DEVAPSICOLEGS']['token']) || !isset($_SESSION['SES_ADMIN_DEVAPSICOLEGS']['id'])) {
            throw new \Exception("Acceso Restringido");
        }
        
        $JWThandler = new JWThandler;
        $serviceAware = new ServiceAware;
        $em = $serviceAware->em;
        $userRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\User');

        $data = $JWThandler->getData($this->token);
        if (($data->user_id != $this->user_id) || ($data->username != $this->username)) {
            throw new \Exception("No hay una sesión valida");
        }

        $user = $userRepository->findOneBy(array(
            'id' => $this->user_id,
            'username' => $this->username,
            'sessionToken' => $this->token
        ));

        if (!$user) {
            throw new \Exception("El usuario no es válido");
        }

        return $user;
    }

    /**
     * Retorna el usuario con la sesión activa
     *
     * @return object
     */
    public function getUserSesison()
    {
        $serviceAware = new ServiceAware;
        $userRepository = $serviceAware->em->getRepository('NaS\DevaPsicolegs\Entity\User');
        return $userRepository->findOneBy(array(
            'id' => $this->user_id,
            'username' => $this->username,
            'sessionToken' => $this->token
        ));
    }
}


?>