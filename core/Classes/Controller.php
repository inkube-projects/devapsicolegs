<?php

namespace NaS\Classes;

use NaS\Classes\UserProvider;

/**
 * Clase principal para controladores
 */
class Controller
{
    function __construct()
    {
        $this->twigConfigLoader = new TwigConfigLoader;
        $this->twig = $this->twigConfigLoader->loadConfig();
        $this->user = @$this->getUserInfo();
        $this->userGlobalTwig = $this->setTwigGlobal();
        $this->em = getEntityManager();
        $this->globals = $this->setAllGlobals();
    }

    /**
     * Se agrega como global todas las variables de sesión con el atributo GLOBAL_TWIG
     *
     * @return void
     */
    public function setAllGlobals()
    {
        if (isset($_SESSION['GLOBAL_TWIG'])) {
            $twig = $this->twig;

            foreach ($_SESSION['GLOBAL_TWIG'] as $key => $global) {
                foreach ($global as $k => $value) {
                    $twig->addGlobal($key, [$k => $value]);
                }
            }

            unset($_SESSION['GLOBAL_TWIG']);
        }
    }

    /**
     * Crea el token para los formularios
     * @param  string $method Metodo
     * @return string
     */
    public function createFormToken($method)
    {
        $JWThandler = new JWThandler;
        $data = array(
            'method' => $method,
            'session_id' => session_id(),
            'r' => uniqid()
        );
        $token = $JWThandler->generateToken($data, time(), (time() + 18000));
        $_SESSION['RR_FORM_TOKEN'] = $token;

        return $token;
    }

    /**
     * Se obtiene la información del usuario
     * @return object
     */
    public function getUserInfo()
    {
        $userProvider = new UserProvider;
        return $userProvider->getUserSesison();
    }

    /**
     * Crea una variable global de twig
     */
    public function setTwigGlobal()
    {
        if (isset($_SESSION['SES_ADMIN_DEVAPSICOLEGS'])) {
            $twig = $this->twig;
            $user = $this->user;
            if ($user) {
                $code = $user->getCode();

                $arr_user = array(
                    'username' => $user->getUsername(),
                    'name' => mb_convert_case($user->getUserInformation()->getName(), MB_CASE_TITLE, "UTF-8"),
                    'lastName' => mb_convert_case($user->getUserInformation()->getLastName(), MB_CASE_TITLE, "UTF-8"),
                    'profileImage' => ($user->getProfileImage()) ? APP_IMG_VIEW_USER."/user_".$code."/profile/".$user->getProfileImage() : APP_IMG_VIEW_NOIMAGE."/profile.jpg",
                    'roleName' => mb_convert_case($user->getRole()->getRoleName(), MB_CASE_TITLE, "UTF-8"),
                    'role' => $user->getRole()->getRole(),
                    'register' => $user->getCreateAt()->format('d-m-Y')
                );

                $twig->addGlobal('userSession', $arr_user);
            }
        }
    }

    /**
     * Retorna un mensaje flash
     * @param  string $class   clase para el alert
     * @param  string $message Mensjae a mostrar
     * @return string
     */
    public function flashMessage($class, $message)
    {
        $flash_message = '<div class="alert '.$class.' alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        '.$message.'
        </div>';

        return $flash_message;
    }

    /**
     * Muestra una forma global de los mensajes flash
     * @param  array $get Variable $_GET
     * @return string
     */
    public function flashMessageGlobal()
    {
        $flash_message = "";
        if (isset($_SESSION['RR_FLASH_MESSAGE'])) {
            $opt = $_SESSION['RR_FLASH_MESSAGE']['opt'];
            $message = $_SESSION['RR_FLASH_MESSAGE']['message'];

            if ($opt == 'added') {
                $flash_message = $this->flashMessage('alert-success', $message);
            } elseif ($opt == 'edited') {
                $flash_message = $this->flashMessage('alert-info', $message);
            } elseif ($opt == 'removed') {
                $flash_message = $this->flashMessage('alert-warning', $message);
            }

            unset($_SESSION['RR_FLASH_MESSAGE']);
        }

        return $flash_message;
    }
}


?>
