<?php

namespace NaS\Classes;

use NaS\Classes\ServiceAware;
use NaS\Classes\UserProvider;

/**
 * Clase que se encarga de los accesos
 */
class Authorization
{
    private $jwt_secret = JWT_SECRET;

    /**
     * Verifica el token de los formularios
     * @param  string $token  JWT token
     * @param  string $method Método a consumir
     * @return void
     */
    public function verifyFormToken($token, $method)
    {
        $JWThandler = new JWThandler;

        if (!isset($_SESSION['RR_FORM_TOKEN'])) {
            throw new \Exception("Invalid token supplied");
        }

        $JWThandler->checkToken($token);
        $data = $JWThandler->getData($token);

        if ($data->method != $method || $_SESSION['RR_FORM_TOKEN'] != $token) {
            throw new \Exception("Invalid token supplied");
        }

        if (isset($_SESSION['RR_FORM_TOKEN_CONSUMED'])) {
            if (in_array($token, $_SESSION['RR_FORM_TOKEN_CONSUMED'])) {
                throw new \Exception("El token ya ha sido consumido");
            }
        }
    }

    /**
     * Destruye el token y lo coloca en una lista de inutilizables
     * @param  string $token JWT token
     * @return void
     */
    public function destroyToken($token)
    {
        unset($_SESSION['RR_FORM_TOKEN']);

        if (isset($_SESSION['RR_FORM_TOKEN_CONSUMED'])) {
            array_push($_SESSION['RR_FORM_TOKEN_CONSUMED'], $token);
        } else {
            $_SESSION['RR_FORM_TOKEN_CONSUMED'] = array($token);
        }
    }

    /**
     * Valida la sesion del usuario
     *
     * @return void
     */
    public function validateSession(array $roles, $route)
    {
        $serviceAware = new ServiceAware;
        $em = $serviceAware->em;
        $auth = false;
        // $userRepository = $em->getRepository('NaS\DevaPsicolegs\Entity\User');

        try {
            $userProvider = new UserProvider;
            $user = $userProvider->validateUserSession();

            foreach ($roles as $key => $role) {
                if ($role == $user->getRole()->getRole()) {
                    $auth = true;
                }
            }

            if (!$auth) {
                throw new \Exception("No tienes acceso", 403);
            }
        } catch (\Exception $e) {
            $_SESSION['ERROR_SESSION_VALIDATEd'] = $e->getMessage();
            $pos = strpos($route, "/inkcms");
            
            if ($pos === false) {
                header('location: '.BASE_URL.'/denied'); exit;
            } else {
                header('location: '.BASE_URL.'/inkcms/denied'); exit;
            }
        }
    }
}


?>
