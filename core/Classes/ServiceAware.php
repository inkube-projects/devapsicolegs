<?php

namespace NaS\Classes;

use NaS\Classes\TwigConfigLoader;
use NaS\DevaPsicolegs\Entity\UserLogs;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Metodos para los servicios
 */
class ServiceAware
{
    function __construct()
    {
        $this->em = getEntityManager();
    }

    /**
     * Prepara la imagen temporal
     * @param  string $temp_path   Ruta de la carpeta temporal
     * @param  string $param_image Nombre del parametro de la imagen
     * @return string
     */
    public function prepareCoverTemp($temp_path, $param_image, $prefix = "profile_")
    {
        // se elimina las imagenes temporales que esten en la carpeta
        clearDirectory($temp_path);

        // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES[$param_image]['type']);
        $image_name = $prefix.rand(00000, 99999).uniqid().$extens;
        list($width, $height) = getimagesize($_FILES[$param_image]['tmp_name']);

        if($width > 1000){
            ImagenTemp($param_image, 1000, $image_name, $temp_path."/");
        }else{
            ImagenTemp($param_image, $width, $image_name, $temp_path."/");
        }

        return $image_name;
    }

    /**
     * Genera el formulario para recortar imagen
     *
     * @param string $img_name        Nombre de la imagen
     * @param string $temp_path       Ruta de las imagenes temporales
     * @param string $path_send_form  Ruta de procesamiento de la imagen
     * @param string $ppal_form_name  Nombre del formulario contenedor
     * @param string $user_code       Código de usuario
     * @return string
     */
    public function showFormCoverIMG($img_name, $temp_path, $path_send_form, $ppal_form_name, $user_code)
    {
        $twigConfigLoader = new TwigConfigLoader;
        $twig = $twigConfigLoader->loadConfig();
        $input_value = $user_code."_".$img_name;

        list($width, $height) = getimagesize($temp_path."/".$img_name);

        if($width > 360) {
            $img_width = 360;
        } else {
            $img_width = $width;
        }

        if($height > 230) {
            $img_height = 230;
        } else {
            $img_height = $height;
        }

        $view = $twig->render('Admin/Ajax/jCrop.twig', array(
            'path_send_form' => $path_send_form,
            'width' => $width,
            'img_width' => $img_width,
            'height' => $height,
            'img_height' => $img_height,
            'img_name' => $img_name,
            'temp_path' => $temp_path,
            'ppal_form_name' => $ppal_form_name,
            'input_value' => $input_value,
            'view_image' => BASE_URL."/".$temp_path."/".$img_name

        ));

        return $view;
    }

    /**
     * Crea la imagen recortada
     *
     * @param string $directory_temp  Directorio de la imagen temporal
     * @param string $user_code       Código del usuario
     * @return void
     */
    public function createIMG($directory_temp, $user_code)
    {
        $dir = $directory_temp;
        $handle = opendir($dir);

        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                $image = $file;
            }
        }

        $ext = explode(".", $image)[1];
        $x = sanitize($_POST['x']);
        $y = sanitize($_POST['y']);
        $w = sanitize($_POST['w']);
        $h = sanitize($_POST['h']);
        $targ_w = $w;
        $targ_h = $h;
        $jpeg_quality = 90;
        $src = $directory_temp.$image;
        $src_min = $dir."min_".$image;

        if(($ext == "jpeg")||($ext == "jpg")){
            $img_r = imagecreatefromjpeg($src);
        }if($ext == "png"){
            $img_r = imagecreatefrompng($src);
        } if($ext == "gif"){
            $img_r = imagecreatefromgif($src);
        }

        $src = $directory_temp.$user_code."_".$image;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);

        if (($ext == "jpeg") || ($ext== "jpg ")){
            imagejpeg($dst_r,$src,90);
        } elseif ($ext == "png") {
            imagepng($dst_r, $src);
        } elseif ($ext=="gif") {
            imagegif($dst_r,$src);
        }

        // se crea la miniatura
        $src_min = $dir."min_".$user_code."_".$image;
        list($width, $height) = getimagesize($src);
        $new_width = 200;
        $new_height = floor($height * ($new_width / $width));
        $targ_w = $new_width;
        $targ_h = $new_height;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);

        if(($ext=="jpeg")||($ext=="jpg")){
            imagejpeg($dst_r,$src_min,90);
        } if($ext=="png"){
            imagepng($dst_r,$src_min);
        } if($ext=="gif"){
            imagegif($dst_r,$src_min);
        }

        imagedestroy($dst_r);
        return $user_code."_".$image;
    }

    /**
     * Verifica si existe un registro
     *
     * @param string $entity   Namespace de la entidad
     * @param string $table    Nombre de la tabla
     * @param string $where    Condición
     * @param string $message  Mensaje de exception
     * @return void
     */
    public function existRecord($entity, $table, $where, $message)
    {
        $cnt_val = $this->em->createQuery("SELECT COUNT({$table}) FROM {$entity} {$table} WHERE {$where}")->getResult();
        if ($cnt_val[0][1] == 0) {
            throw new \Exception($message);
        }
    }

    /**
     * Verifica si un registro esta duplicado
     *
     * @param string $entity   Namespace de la entidad
     * @param string $table    Nombre de la tabla
     * @param string $where    Condición
     * @param string $message  Mensaje de exception
     * @return void
     */
    public function duplicatedRecord($entity, $table, $where, $message)
    {
        $cnt_val = $this->em->createQuery("SELECT COUNT({$table}) FROM {$entity} {$table} WHERE {$where}")->getResult();
        if ($cnt_val[0][1] > 0) {
            throw new \Exception($message);
        }
    }

    /**
     * Obtiene el valor de una entidad
     *
     * @param string $entity     Namespace de la entidad
     * @param array  $condition  Arreglo con la condición
     * @return object
     */
    public function getEntry($entity, array $condition)
    {
        return $this->em->getRepository($entity)->findOneBy($condition);
    }

    /**
     * Pasa una imagen reortada a su ruta final
     *
     * @param string $name        Nombre de la imagen
     * @param string $old         Nombre de la imagen vieja
     * @param string $temp_path   Ruta temporal
     * @param string $final_path  Ruta final
     * @return void
     */
    public function saveImage($name, $old = "", $temp_path, $final_path)
    {
        $filename = $temp_path.$name;

        if (fileExist($filename)) {
            rename($filename, $final_path.$name);
            rename($temp_path."min_".$name, $final_path."min_".$name);

            if ((!is_null($old)) && ($old != "")) {
                @unlink($final_path.$old);
                @unlink($final_path."min_".$old);
            }

            @unlink($filename);
            @unlink($temp_path."min_".$name);
        }
    }

    /**
     * Crea una variable de sesión para mensajes flash
     * @param  string $opt     Opción
     * @param  string $message Mensaje a mostrar
     * @return void
     */
    public function generateFlash($opt, $message)
    {
        $_SESSION['RR_FLASH_MESSAGE'] = [
            'opt' => $opt,
            'message' => $message
        ];
    }

    /**
     * Guarda los logs
     *
     * @param object $user     Objeto del usuario que realiza la acción
     * @param string $message  Mensaje del log
     * @return void
     */
    public function addLogs($user, $message)
    {
        $ip_acces = @$_SERVER['REMOTE_ADDR'];
        $ip_proxy = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $ip_compa = @$_SERVER['HTTP_CLIENT_IP'];
        $o_user = $this->getEntry('\NaS\DevaPsicolegs\Entity\User', array("id" => $user->getId()));

        $userLogs = new UserLogs;
        $userLogs->setUsername($user->getUsername());
        $userLogs->setAction($message);
        $userLogs->setIpAccess($ip_acces);
        $userLogs->setIpProxy($ip_proxy);
        $userLogs->setIpCompany($ip_compa);
        $userLogs->setUser($o_user);

        $this->em->persist($userLogs);
        $this->em->flush();
    }

    /**
     * Guardando imagenes temporales
     * @return void
     */
    public function uploadImages($param, $path, $prefix, $template = "Admin/Ajax/temp-gallery.twig")
    {
        $arr_lenght = count($_FILES[$param]['tmp_name']);
        // $path = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/";

        for ($i = 0; $i < $arr_lenght; $i++) {
            // se sube la nueva imagen temporal
            $extens = str_replace("image/",".",$_FILES[$param]['type'][$i]);
            $img_name = $prefix.rand(00000, 99999).uniqid().$extens;
            list($width, $height) = getimagesize($_FILES[$param]['tmp_name'][$i]);

            if($width > 800){
                UpPictureArray($param, 800, $img_name, $path."/", $i);
            }else{
                UpPictureArray($param, $width, $img_name, $path."/", $i);
            }
        }

        return $this->getTempImages($path, $template);
    }

    /**
     * Retorna las miniaturas de todas las imágenes temporales en tiwg-html
     * @return string
     */
    public function getTempImages($path, $template = "Admin/Ajax/temp-gallery.twig")
    {
        // $path = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/";
        $twigConfigLoader = new TwigConfigLoader;
        $twig = $twigConfigLoader->loadConfig();
        $handle = opendir($path."/");
        $result = "";
        $cnt = 0;
        $arr_temp_images = array();

        while ($file = readdir($handle)){
            if (is_file($path."/".$file)) {
                if (substr($file, 0,4) != "min_") {
                    $arr_temp_images[] = array(
                        'file' => $file,
                        'cnt' => $cnt
                    );
                    $cnt++;
                }
            }
        }

        $view = $twig->render($template, array(
            'images' => $arr_temp_images,
            'base_path' => BASE_URL."/".$path
        ));

        return $view;
    }

    /**
     * Mueve images de una ruta a otra
     *
     * @param  string $from Ruta inicial
     * @param  string $to   Ruta final
     * @return array
     */
    public function moveImages($from, $to)
    {
        $arr = array();
        $handle = opendir($from);
        $cnt = 0;

        while ($file = readdir($handle)){
            if (is_file($from.$file)) {
                if (rename($from.$file, $to.$file)) {
                    if (substr($file, 0,4) != "min_") {
                        $arr[] = $file;
                    }
                }
            }
        }

        return $arr;
    }

    public function sendEmail($email_template, $arr_data, $subject, $arr_to)
    {
        $mail = new PHPMailer(true);

        $twigConfigLoader = new TwigConfigLoader;
        $twig = $twigConfigLoader->loadConfig();
        
        $template = $twig->render($email_template, $arr_data);

        //Server settings
        $mail->isSMTP();
        $mail->Host = EMAIL_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = EMAIL_SENDER;
        $mail->Password = EMAIL_PASSWORD;
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->setFrom(EMAIL_SENDER, 'psicologiaysexologia.es - Contacto');
        foreach ($arr_to as $email => $name) {
            $mail->addAddress($email, $name);
        }
        
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body    = $template;
        $mail->AltBody = $template;

        $mail->send();
    }
}


?>
