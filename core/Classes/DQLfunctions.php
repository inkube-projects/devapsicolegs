<?php

namespace NaS\Classes;

class DQLfunctions
{
    function __construct()
    {
        $this->em = getEntityManager();
    }


    public function getCount($entity, $table, $where)
    {
        $cnt_val = $this->em->createQuery("SELECT COUNT({$table}) FROM {$entity} {$table} WHERE {$where}")->getResult();
        return $cnt_val[0][1];
    }
}


?>