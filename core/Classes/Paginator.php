<?php

namespace NaS\Classes;

class Paginator
{
    public $url;
    public $additional;

    function __construct($url, $additional)
    {
        $this->em = getEntityManager();
        $this->url = $url;
        $this->additional = $additional;
    }

    /**
     * Realiza la paginación
     *
     * @param object  $query           QueryBuilder
     * @param integer $page            Página actual
     * @param integer $limit_per_page  Número de resultados por página
     * @return void
     */
    public function paginate($query, $page, $limit_per_page)
    {
        $page = @number_format($page,0,"","");
        $limit_per_page = @number_format($limit_per_page,0,"","");
        $current_page = $page;

        // Se obtiene el número total de resultados
        $total = count($query->getQuery()->getScalarResult());

        if ($current_page > ceil($total / $limit_per_page)) {
            $current_page = 1;
        } elseif ($current_page == 0){
            $current_page = 1;
        }

        // Se obtiene el total de páginas
        $page_total = ceil($total / $limit_per_page);

        // Resultados por pagina
        $query->setFirstResult(($current_page - 1) * $limit_per_page)
            ->setMaxResults($limit_per_page)
        ;

        $response = [
            'results' => $query->getQuery()->getResult(),
            'paginator' => $this->getPaginator($total, $current_page, $limit_per_page, $page_total)
        ];

        return $response;
    }

    /**
     * Realiza los calculos y retorna el paginador
     * @return object
     */
    public function getPaginator($total, $current_page, $limit_per_page, $page_total)
    {
        if ($current_page > ceil($total / $limit_per_page)) { $current_page = 1; } elseif ($current_page == 0) { $current_page = 1;}

        if ($current_page > 2){
            $pager_it_lo = $current_page - 2;

            if (($current_page + 2) >$page_total) {
                $pager_it_hi = $page_total;
            } else {
                $pager_it_hi = $current_page + 2;
            }
        } else {
            $pager_it_lo = 1;

            if($page_total > 5){
                $pager_it_hi = 5;
            } else {
                $pager_it_hi = $page_total;
            }
        }

        return $this->getHtmlPaginator($current_page, $page_total, $total);
    }

    /**
     * Retorna el HTML del paginador
     * @param  integer $current_page   Página actual
     * @param  integer $page_total     Cantidad de páginas
     * @param  string  $url            URL base
     * @param  integer $total          Total de registros
     * @return string
     */
    public function getHtmlPaginator($current_page, $page_total, $total)
    {
        $url = $this->url;
        $additional = $this->additional;

        if($current_page == ""){ $current_page = 1; }

        if ($current_page > 2){
            $pager_it_lo = $current_page - 2;

            if (($current_page + 2) > $page_total){ $pager_it_hi = $page_total; } else { $pager_it_hi = $current_page + 2; }
        } else {
            $pager_it_lo = 1;
            if ($page_total > 5){ $pager_it_hi = 5; } else { $pager_it_hi = $page_total; }
        }

        $html = '
            <div class="paginationWrapper large">
        ';

        if ($current_page > 1) {
            // $html .= '<li><a href="'.$url.'/1'.$additional.'" title="Inicio">Inicio</a></li>';
        }

        if ($current_page != 1) {
            $html .= '<a href="'.$url.'/'.($current_page - 1).$additional.'" title="Anterior" class="pagiPrev"><i class="fa fa-angle-left"></i></a>';
        }

        $html .= '<div class="nubmerPagination">';
        for($j = $pager_it_lo; $j <= $current_page; $j++){
            $ext = ''; $lnk = ''; $ext_fond = '';
            if ($j == $current_page) { $ext = 'class="activePagi"'; $lnk = "#"; $ext_fond = ''; }
            if ($total !=0 ) {
                $html .='<a href="'.$url.'/'.$j.$additional.'" '.$ext.' class="numberPagi">'.$j.'</a>';
            }
        }
        $html .= '</div>';

        for($j = ($current_page + 1); $j <= $pager_it_hi; $j++){
            $html .= '<a class="css-no-active" href="'.$url.'/'.$j.$additional.'">'.$j.'</a>';
        }

        if (($current_page != $page_total) && ($total != 0)){
            $html .= '<a href="'.$url.'/'.($current_page + 1).$additional.'" title="Siguiente" class="pagiNext"><i class="fa fa-angle-right"></i></a>';
        }

        if (($j-1) <= $page_total) {
            //$html .= '<li><a href="'.$url.'/'.$page_total.$additional.'" title="Final">Final</a></li>';
        }

        $html .= '</div>';

        return $html;
    }
}


?>
