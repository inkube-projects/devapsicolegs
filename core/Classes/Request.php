<?php

namespace NaS\Classes;

use NaS\Classes\Route;

class Request
{
    public $parameters = array();

    /**
     * Retorna el valor del parametro
     * @param  string $paramName Nombre del parametro
     * @return string
     */
    public function getParameter($paramName)
    {
        if (empty($paramName)) { throw new \Exception("Debes agregar el nombre del parametro"); }
        $parameters = $this->getParameters();
        if (!isset($parameters[$paramName])) { throw new \Exception("El parametro no es válido {$paramName}"); }
        return $parameters[$paramName];
    }

    /**
     * Obtiene el nombre y valor de los parametros
     * @return array
     */
    public function getParameters()
    {
        $route = new Route;
        $uri = $route::getCurrentUri();
        $arr_routes = $route::$validRoutes;
        $arr_response = array();

        foreach ($arr_routes as $val) {
            $pos = strpos($val['route'], "/{");
            if ($pos !== false) {
                $arr_url = explode("/{", $val['route']);
                $current_route = ($arr_url[0] == "") ? "/" : $arr_url[0];
                $verify_url = strpos($uri, $current_route);

                if ($verify_url !== false) {
                    // Se obtiene los nombres de los parametros
                    $full_parameters = explode($current_route, $val['route']);

                    if ($current_route == "/") {
                        $full_parameters = array_filter($full_parameters, "strlen");
                        $full_parameters[1] = "/".implode("/", $full_parameters);
                    }
                    
                    $parameters = explode('/', $full_parameters[1]);
                    $arr_url_parameters = array();

                    foreach ($parameters as $parameter) {
                        if ($parameter != "") {
                            preg_match('/{(.+?)}/', $parameter, $matches);
                            $arr_url_parameters[] = $matches[1];
                        }
                    }

                    $full_values = explode($current_route, $uri);
                    if ($current_route == "/") {
                        $full_values = array_filter($full_values, "strlen");
                        $full_values[1] = "/".implode("/", $full_values);
                    }

                    $full_values[1] = (substr($full_values[1],0,1) == "/") ? $full_values[1] : "/".$full_values[1];
                    $values = explode('/', $full_values[1]);
                    $arr_url_values = array();

                    foreach ($values as $key => $value) {
                        if ($key > 0) {
                            $arr_url_values[] = $value;
                        }
                    }
                    
                    // Se unen los arreglo
                    foreach ($arr_url_parameters as $key => $val) {
                        $arr_response[$val] = (isset($arr_url_values[$key])) ? $arr_url_values[$key] : "";
                    }
                }
            }
        }
        
        return $arr_response;
    }
}


?>
