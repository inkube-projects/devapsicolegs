$(function(e){
    $("#frm-comment").validate({
        rules: {
            names: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            email: {
                required: true,
                email: true
            },
            comment: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            }
        },
        messages: {
            names: {
                required: "Debes ingresar al menos un nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            email: {
                required: "Debes ingresar tu E-mail",
                email: "Debes ingresar un E-mail válido"
            },
            comment: {
                required: "Debes ingresar el comentario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "-validate"));
        },
        submitHandler: function() {
            persist('frm-comment', base_url + '/ajax/post/comment');

            return false;
        }
    });
});

/**
 * Persiste el formulario
 * @param  {string} frm          Nombre del formulario
 * @param  {string} processPATH  Ruta del proceso
 * @param  {string} successPATH  Ruta al finalizar
 * @return {void}
 */
var persist = function(frm, processPATH)
{
    var i = $("#" + frm).serialize();

    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $('.js-alert').html('<div class="alert alt-alert alert-success" role="alert">Tu comentario ha sido publicado correctamente<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $('.js-comments').prepend('<div class="media"><div class="media-left"><a href="#"><img class="media-object" src="' + base_url + '/public/images/no_images/profile.jpg" alt="' + r.data.name + '" width="64"></a></div><div class="media-body css-justify" style="line-height: 17px;"><p class="media-heading pull-left">' + r.data.name + '</p><p class="pull-right"><i class="fas fa-clock"></i> ' + r.data.create + '</p><div class="clearfix"></div>' + r.data.comment + '</div></div><hr>');
                $('html,body').animate({
                    scrollTop: $('.js-comments').offset().top
                }, 'slow');
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
