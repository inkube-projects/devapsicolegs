$(function(e){
    $("#frm-contact").validate({
        rules: {
            c_name: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            c_email: {
                required: true,
                email: true
            },
            c_phone: {
                required: true,
                number: true
            },
            c_reason: {
                required: true
            },
            c_message: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            }
        },
        messages: {
            c_name: {
                required: "Debes agregar al menos un nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            c_email: {
                required: "Debes agregar tu E-mail",
                email: "Debes agregar un email válido"
            },
            c_phone: {
                required: "Debes agregar un número telefónico",
                number: "Solo se permiten números"
            },
            c_reason: {
                required: "Debes seleccionar un motivo"
            },
            c_message: {
                required: "Debes agregar un mensaje",
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist('frm-contact', base_url + '/ajax/contact');

            return false;
        }
    });
});

/**
 * Persiste el formulario
 * @param  {string} frm          Nombre del formulario
 * @param  {string} processPATH  Ruta del proceso
 * @param  {string} successPATH  Ruta al finalizar
 * @return {void}
 */
var persist = function(frm, processPATH)
{
    var i = $("#" + frm).serialize();

    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $('.js-alert').html('<div class="alert alt-alert alert-info" role="alert">Tu mensaje ha sido enviado correctamente. Nos comunicaremos contigo en la brevedad posible<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                document.getElementById("frm-contact").reset();

                $('html,body').animate({
                    scrollTop: $('.js-alert').offset().top
                }, 'slow');
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(document).scrollTop(0);
            }

            $(".btn-submit").attr("disabled", false).html('Guardar');
        }
    });
}
