$(function(e){
    var act = $('.p-key').data('act');
    var form_name = $('.p-key').data('form');
    var pp_id = $('.p-key').data('id');

    $("#" + form_name).validate({
        rules: {
            name: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            description: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
        },
        messages: {
            name: {
                required: "Debes agregar el nombre del miembro del servicio",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            description: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(form_name, base_url + '/inkcms/ajax/service/edit/' + pp_id, base_url + '/inkcms/information/service/edit/' + pp_id);
            return false;
        }
    });

});


/**
 * Persiste el formulario
 * @param  {string} frm          Nombre del formulario
 * @param  {string} processPATH  Ruta del proceso
 * @param  {string} successPATH  Ruta al finalizar
 * @return {void}
 */
var persist = function(frm, processPATH, successPATH)
{
    var i = $('#' + frm).serialize();
    
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
