$(function(e){
    var act = $('.p-key').data('act');
    var pp_id = $('.p-key').data('id');
    var form_name = $('.p-key').data('form');

    // Select2
    $('.select2').select2();

    // CKeditor
    const uploadPath = base_url + '/inkcms/ajx/information/uploadImageCK';
    function MyCustomUploadAdapterPlugin(editor) {
        editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
            return new MyUploadAdapter(loader, uploadPath);
        };
    }

    CKeditor5Config.extraPlugins = [MyCustomUploadAdapterPlugin];

    ClassicEditor
        .create(document.querySelector('#description'), CKeditor5Config)
        .then(editor => {
            window.editor = editor;
            console.log(Array.from(editor.ui.componentFactory.names()));
        })
        .catch(err => {
            console.error(err.stack);
        });

    $("#" + form_name).validate({
        rules: {
            name: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            service: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Debes ingresar el nombre del sub-servicio",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            service: {
                required: "Debes seleccionar un servicio",
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            if (act == 1) {
                persist(form_name, base_url + '/inkcms/ajax/subservice/new', base_url + '/inkcms/information/subservice/edit/' + pp_id);
            } else if (act == 2) {
                persist(form_name, base_url + '/inkcms/ajax/subservice/update/' + pp_id, base_url + '/inkcms/information/subservice/edit/' + pp_id);
            }
            return false;
        }
    });
});

var persist = function(frm, processPATH, successPATH)
{
    var i = $("#" + frm).serialize();
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}