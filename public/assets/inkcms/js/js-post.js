$(function(){
    var act = $('.p-key').data('act');
    var pp_id = $('.p-key').data('id');
    var form_name = $('.p-key').data('form');
    var select2Conf = {
        "language": {
            'noResults': function(){
                return "<span class=\"label label-warning\">Aviso</span> No se encontraron resultados";
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    };

    // Select2
    select2Conf.placeholder = "Selecciona una categoría";
    $('#category').select2(select2Conf);
    select2Conf.placeholder = "Debes seleccionar una categoría primero";
    $('#subcategory').select2(select2Conf);
    select2Conf.placeholder = "Selecciona un tag";
    $('#tag').select2(select2Conf);

    // CKeditor
    const uploadPath = base_url + '/inkcms/ajax/post/CKeditor/images';
    function MyCustomUploadAdapterPlugin(editor) {
        editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
            return new MyUploadAdapter(loader, uploadPath);
        };
    }

    CKeditor5Config.extraPlugins = [MyCustomUploadAdapterPlugin];

    ClassicEditor
        .create(document.querySelector('#content'), CKeditor5Config)
        .then(editor => {
            window.editor = editor;
            console.log(Array.from(editor.ui.componentFactory.names()));
        })
        .catch(err => {
            console.error(err.stack);
        });


    // Busca subcategorías
    $('#category').on('change', function(){
        $.ajax({
            type: 'POST',
            url: base_url + '/inkcms/ajx/post/getSubcategories',
            data: {
                category: $(this).val(),
                post_id: $('.p-key').data('id')
            },
            dataType: 'json',
            success: function(r) {
                if (r.status == 'OK') {
                    $('#ajx-subcategory').html(r.data);
                    select2Conf.placeholder = "Selecciona una subcateoría";
                    $('#subcategory').select2(select2Conf);
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(".btn-submit").attr("disabled", false).html('Guardar');
                    $(document).scrollTop(0);
                }
            }
        });
    });

    // Se valida y prepara la imagen para recortar
    $("#cover_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|jpeg|png|gif)$/i).test(img)) {
            $("#cover_image_validate").html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)");
            $('#profile-container').addClass('btn-danger css-text-white').removeClass('btn-success');
        } else {
            $("#cover_image_validate").html('');
            $('#profile-container').addClass('btn-success css-text-white').removeClass('btn-danger');
            uploadImage(form_name);
        }
    });

    // Subir imagenes de galería
    $('#gallery_images').on('change', function(e){
        if ($(this).val() != "") {
            $('.btn-file-gallery').addClass('btn-success').removeClass('btn-default');
            $('.btn-file-gallery span').html('Subiendo imágenes, por favor espere... <i class="fa fa-spinner fa-spin"></i>');
            $('.btn-submit').attr('disabled', true);
            uploadImages(form_name);
        }
    });

    // Mostrar imagenes temporales en modal
    $('#ajx-gallery, #gallery-body').on('click', '.btn-show', function(e){
        var image = $(this).data('image');
        $('#mod-image').modal('show');
        $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
        e.preventDefault();
    });

    $("#banner").on('change', function(){
        var image = $(this).val();
        if (image != "") {
            $('#img-banner').addClass('btn-success');
            $('#img-banner').removeClass('btn-default');
        }
    });

    $("#" + form_name).validate({
        rules: {
            title: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            category: {
                required: true
            },
            seo_title: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            seo_author: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            seo_url: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            seo_keywords: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            seo_description: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            }
        },
        messages: {
            title: {
                required: "Debes agregar el título de la publicación",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            category: {
                required: "Debes seleccionar al menos una categoría"
            },
            seo_title: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_author: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_url: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_keywords: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_description: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            if (act == 1) {
                persist(form_name, base_url + '/inkcms/ajax/post/new', base_url + '/inkcms/post/edit/');
            } else if (act == 2) {
                persist(form_name, base_url + '/inkcms/ajax/post/update/' + pp_id, base_url + '/inkcms/post/edit/');
            }

            return false;
        }
    });
});


/**
 * Persiste el formulario
 * @param  {string} frm          Nombre del formulario
 * @param  {string} processPATH  Ruta del proceso
 * @param  {string} successPATH  Ruta al finalizar
 * @return {void}
 */
var persist = function(frm, processPATH, successPATH)
{
    // var i = $("#" + frm).serialize();
    var i = new FormData(document.getElementById(frm));
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH + r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

/**
 * Envía una imagen
 * @param  {string} form_name Nombre del personaje
 * @return {void}
 */
var uploadImage = function(form_name)
{
    $("#mod-img-cover").modal('show');
    $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
    $("#ajx-img").html("");
    saveImgAjax(form_name, base_url + '/inkcms/ajax/post/uploadImage', 'ajx-img-cover');
    $("#cover_image").val('');
    return false;
}

/**
 * Se encarga de subir las imágenes de galería
 * @param  {string} form_name Nombre del formulario
 * @return {boolean}
 */
function uploadImages(form_name) {
    var formData = new FormData(document.getElementById(form_name));

    $.ajax({
        type: 'POST',
        url: base_url + '/inkcms/ajax/post/gallery',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(r){
            if (r.status == "OK") {
                $('#ajx-gallery').html(r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(document).scrollTop(0);
            }

            $('#gallery_images').val('');
            $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
            $('.btn-file-gallery span').html('Agregar imágenes');
            $('.btn-submit').attr('disabled', false);
        }
    });

    return false;
}

/**
 * Elimina las imagenes de galería
 * @param  {integer} acc   Acción a realizar
 * @param  {string} image Nombre de la imagen
 * @return {void}
 */
function removeImage(act, image) {
    if (confirm("¿Eliminar imagen?")) {
        $.ajax({
            type: 'POST',
            url: base_url + '/inkcms/ajax/post/gallery/remove',
            dataType: 'json',
            data: {
                img: image,
                act: act,
                post_id: $('.p-key').data('id')
            },
            success: function(r) {
                if (r.status == "OK") {
                    if (act == 1) {
                        $('#ajx-gallery').html(r.data);
                    } else {
                        $('#gallery-body').html(r.data);
                    }

                    $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
                    $('.btn-file-gallery span').html('Agregar imágenes');
                    $('.btn-submit').attr('disabled', false);
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(document).scrollTop(0);
                }
            }
        });
    }
}
