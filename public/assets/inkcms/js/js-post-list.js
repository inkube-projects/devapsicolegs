$(function(e){
    // select2
    var select2Conf = {
        "placeholder": "Todos",
        "language": {
            'noResults': function(){
                return "<span class=\"label label-warning\">Aviso</span> No se encontraron resultados";
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    };
    $('.select2').select2(select2Conf);

    // Mascara para fechas
    $("[data-mask]").inputmask();

    // Configuración de la paginación con DataTable
    $('.data-table').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    $('#frm-search').on('submit', function(e){
        e.preventDefault();
        var draft = ($('#draft').prop('checked')) ? "1" : "0";
        var headline = ($('#headline').prop('checked')) ? "1" : "0";
        var active = ($('#active').prop('checked')) ? "1" : "0";
        var cat = ($('#category').val().join() != '') ? $('#category').val().join() : 'all';
        var sub = ($('#subcategory').val().join() != '') ? $('#subcategory').val().join() : 'all';
        var tag = ($('#tags').val().join() != '') ? $('#tags').val().join() : 'all';
        var user = ($('#user').val() != '') ? $('#user').val() : 'all';
        var start_date = ($('#start_date').val() != '') ? $('#start_date').val() : 'all';
        var end_date = ($('#end_date').val() != '') ? $('#end_date').val() : 'all';
        var URL = '/' + draft + '/' + headline + '/' + active + '/' + cat + '/' + sub + '/' + tag + '/' + user + '/' + start_date + '/' + end_date;

        window.location = base_url + '/inkcms/posts' + URL;
    });

    // Limpiar el formulario de búsqueda
    $('.btn-reset').on('click', function(e){
        $('#frm-search select').val('').trigger('change');
        $('#frm-search input[type=text]').val('');
        defaultButton();
    });
});

function defaultButton()
{
    var isChecked = false;
    $('.btn-src').data('state', (isChecked) ? "on" : "off");
    $('.state-icon').removeClass().addClass('state-icon glyphicon glyphicon-unchecked');
    $('#frm-search .btn-src').removeClass('btn-primary active').addClass('btn-default');
    $('#frm-search input[type=checkbox]').attr('checked', false);
}
