$(function(e){
    var act = $('.p-key').data('act');
    var pp_id = $('.p-key').data('id');
    var form_name = $('.p-key').data('form')

    // Se valida y prepara la imagen para recortar
    $("#cover_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|jpeg|png|gif)$/i).test(img)) {
            $("#cover_image_validate").html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)");
            $('#profile-container').addClass('btn-danger css-text-white').removeClass('btn-success');
        } else {
            $("#cover_image_validate").html('');
            $('#profile-container').addClass('btn-success css-text-white').removeClass('btn-danger');
            uploadImage(form_name);
        }
    });

    $("#" + form_name).validate({
        rules: {
            name: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            description: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            }
        },
        messages: {
            name: {
                required: "Debes ingresar el nombre de la categoría",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            description: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            if (act == 1) {
                persist(form_name, base_url + '/inkcms/ajax/post/category/new', base_url + '/inkcms/post/category/edit/');
            } else if (act == 2) {
                persist(form_name, base_url + '/inkcms/ajax/post/category/update/' + pp_id, base_url + '/inkcms/post/category/edit/');
            }
            return false;
        }
    });
});

var persist = function(frm, processPATH, successPATH)
{
    var i = $("#" + frm).serialize();
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH + r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

var uploadImage = function(form_name)
{
    $("#mod-img-cover").modal('show');
    $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
    $("#ajx-img").html("");
    saveImgAjax(form_name, base_url + '/inkcms/ajax/post/category/uploadImage', 'ajx-img-cover');
    $("#cover_image").val('');
    return false;
}
