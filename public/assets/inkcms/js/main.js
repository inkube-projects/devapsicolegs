$(function(e){
    // navbar Active
    var m = $('.p-key').data('m');
    var l = $('.p-key').data('l');
    $('li[data-h-opt="' + m + '"]').addClass('active menu-open');
    $('li[data-h-opt="' + l + '"]').addClass('active');

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip();
})
