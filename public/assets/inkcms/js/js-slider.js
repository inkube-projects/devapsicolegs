$(function(){
    var act = $('.p-key').data('act');
    var pp_id = $('.p-key').data('id');
    var form_name = $('.p-key').data('form');

    $("#banner").on('change', function(){
        var image = $(this).val();
        if (image != "") {
            $('#img-banner').addClass('btn-success');
            $('#img-banner').removeClass('btn-default');
        }
    });

    $("#" + form_name).validate({
        rules: {
            url: {
                url: true
            },
            content: {
                noSpecialCharacters: /([*^${}()|\[\]\\])/g
            },
            banner: {
                required: (act == 1) ? true : false,
                extension: "jpg|jpeg|png|gif"
            }
        },
        messages: {
            url: {
                url: "Debes ingresar un URL válido"
            },
            content: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            banner: {
                required: "Debes agregar una imagen",
                extension: "Debes ingresar una imagen válida (jpg, jpeg, png, gif)"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            if (act == 1) {
                persist(form_name, base_url + '/inkcms/ajax/slider/new', base_url + '/inkcms/slider/edit/');
            } else if (act == 2) {
                persist(form_name, base_url + '/inkcms/ajax/slider/update/' + pp_id, base_url + '/inkcms/slider/edit/');
            }

            return false;
        }
    });
});

/**
 * Persiste el formulario
 * @param  {string} frm          Nombre del formulario
 * @param  {string} processPATH  Ruta del proceso
 * @param  {string} successPATH  Ruta al finalizar
 * @return {void}
 */
var persist = function(frm, processPATH, successPATH)
{
    var i = new FormData(document.getElementById(frm));
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH + r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
