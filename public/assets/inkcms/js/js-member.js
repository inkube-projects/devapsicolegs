$(function(e){
    var act = $('.p-key').data('act');
    var form_name = $('.p-key').data('form');
    var path = $('.p-key').data('path');
    var pp_id = $('.p-key').data('id');

    // CKeditor
    const uploadPath = base_url + '/inkcms/ajx/information/uploadImageCK';
    function MyCustomUploadAdapterPlugin(editor) {
        editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
            return new MyUploadAdapter(loader, uploadPath);
        };
    }

    CKeditor5Config.extraPlugins = [MyCustomUploadAdapterPlugin];

    ClassicEditor
        .create(document.querySelector('#experience'), CKeditor5Config)
        .then(editor => {
            window.editor = editor;
            console.log(Array.from(editor.ui.componentFactory.names()));
        })
        .catch(err => {
            console.error(err.stack);
        });

    $("#profile_image").on('change', function(){
        var image = $(this).val();
        if (image != "") {
            $('#img-banner').addClass('btn-success');
            $('#img-banner').removeClass('btn-default');
        }
    });

    $("#frm-member").validate({
        rules: {
            name: {
                required: true,
                // noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            description: {
                // noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            profile_image: {
                extension: "jpg|jpeg|png|gif"
            }
        },
        messages: {
            name: {
                required: "Debes agregar el nombre del miembro del equipo",
                // noSpecialCharacters: "No se permiten caracteres especiales"
            },
            description: {
                // noSpecialCharacters: "No se permiten caracteres especiales"
            },
            profile_image: {
                extension: "Debes agregar una imagen válida (jpg|jpeg|png|gif)"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(form_name, base_url + '/inkcms/ajax/team/edit/' + pp_id, base_url + '/inkcms/information/team/edit/' + pp_id);
            return false;
        }
    });

});


/**
 * Persiste el formulario
 * @param  {string} frm          Nombre del formulario
 * @param  {string} processPATH  Ruta del proceso
 * @param  {string} successPATH  Ruta al finalizar
 * @return {void}
 */
var persist = function(frm, processPATH, successPATH)
{
    var i = new FormData(document.getElementById(frm));

    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
