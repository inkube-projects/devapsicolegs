$(function(e){
    var act = $('.p-key').data('act');
    var form_name = $('.p-key').data('form');

    //Datemask dd/mm/yyyy
    $('.datemask').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

    // Select2
    $('.select2').select2();

    // Se valida y prepara la imagen para recortar
    $("#profile_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|jpeg|png|gif)$/i).test(img)) {
            $("#profile_image_validate").html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)");
            $('#profile-container').addClass('btn-danger css-text-white').removeClass('btn-success');
        } else {
            $("#profile_image_validate").html('');
            $('#profile-container').addClass('btn-success css-text-white').removeClass('btn-danger');
            uploadImage(form_name);
        }
    });

    $("#" + form_name).validate({
        rules: {
            email: {
                required: true,
                rangelength: [3, 50],
                email: true
            },
            username: {
                required: true,
                rangelength: [3, 20],
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            pass: {
                required: (act == 1) ? true : false,
                rangelength: [3, 20]
            },
            repeat_pass: {
                required: (act == 1) ? true : false,
                equalTo: "#pass"
            },
            name: {
                required: true,
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            last_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            country: {
                required: true,
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            city: {
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            note: {
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            signature: {
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            address_1: {
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            address_2: {
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            postal_code: {
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            status: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Debes ingresar un E-mail",
                rangelength: "EL email debe tener entre 3 y 50 caracteres",
                email: "Debes ingresar un E-mail válido"
            },
            username: {
                required: "Debes ingresar un Nombre de ususario",
                rangelength: "El nombre de usuario debe tener entre 3 y 20 caracteres",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            pass: {
                required: "Debes ingresar una contraseña",
                rangelength: "La contraseña debe tener entre 3 y 20 caracteres"
            },
            repeat_pass: {
                required: "Debes repetir la contraseña",
                equalTo: "Las contraseñas deben ser iguales"
            },
            name: {
                required: "Debes ingresar al menos un Nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name: {
                required: "Debes ingresar al menos un apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            country: {
                required: "Debes seleccionar un país",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            city: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            note: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            signature: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            address_1: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            address_2: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            postal_code: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            status: {
                required: "Debes seleccionar un Estado"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(form_name, base_url + '/inkcms/ajax/profile', base_url + '/inkcms/profile');
            return false;
        }
    });
});

var persist = function(frm, processPATH, successPATH)
{
    var i = $("#" + frm).serialize();
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

var uploadImage = function(form_name)
{
    $("#mod-img-cover").modal('show');
    $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
    $("#ajx-img").html("");
    saveImgAjax(form_name, base_url + '/inkcms/ajax/user/uploadImage', 'ajx-img-cover');
    $("#profile_image").val('');
    return false;
}
