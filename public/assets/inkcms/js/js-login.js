$(function () {
    document.getElementById("user_email").focus();

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
    });

    $("#frm-login").validate({
        rules: {
            user_email: {
                required: true,
                rangelength: [3, 20],
                noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
            },
            password: {
                required: true,
                rangelength: [5, 20],
            },
        },
        messages: {
            user_email: {
                required: "Debes ingresar tu Nombre de Usuario o E-mail",
                rangelength: "El Usuario debe tener entre 3 y 20 caracteres",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            password: {
                required: "Debes ingresar tu contraseña",
                rangelength: "La contraseña debe tener entre 5 y 20 caracteres",
            },
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            login();

            return false;
        }
    });
});

function login()
{
    $.ajax({
        type: 'POST',
        url: base_url + '/inkcms/ajax/login',
        data: $('#frm-login').serialize(),
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                window.location.replace(base_url + '/inkcms');
                // href.location = base_url + '/admin';
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                $(".btn-submit").attr("disabled", false).html('Iniciar sesión');
                $(document).scrollTop(0);
            }
        }
    });
}
