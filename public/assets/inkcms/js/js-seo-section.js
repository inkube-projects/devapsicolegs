$(function(){
    var act = $('.p-key').data('act');
    var pp_id = $('.p-key').data('id');
    var form_name = $('.p-key').data('form')

    // Validate
    $("#" + form_name).validate({
        rules: {
            name: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            }
        },
        messages: {
            name: {
                required: "Debes agregar el nombre de la sección",
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            if (act == 1) {
                persist(form_name, base_url + '/inkcms/ajax/seo/section/new', base_url + '/inkcms/seo/section/edit/');
            } else if (act == 2) {
                persist(form_name, base_url + '/inkcms/ajax/seo/section/update/' + pp_id, base_url + '/inkcms/seo/section/edit/');
            }
            return false;
        }
    });
});

var persist = function(frm, processPATH, successPATH)
{
    var i = $("#" + frm).serialize();
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH + r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
