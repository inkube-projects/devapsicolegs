$(function(){
    var act = $('.p-key').data('act');
    var pp_id = $('.p-key').data('id');
    var form_name = $('.p-key').data('form')
    // Selet2
    $('.select2').select2();

    $("#" + form_name).validate({
        rules: {
            title: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            author: {
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            section: {
                required: true,
            },
            description: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            },
            keywords: {
                required: true,
                noSpecialCharacters: /([*^${}()|\[\]\/\\])/g
            }
        },
        messages: {
            title: {
                required: "Debes ingresar un título",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            author: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            section: {
                required: "Debes seleccionar una sección",
            },
            description: {
                required: "Debes ingresar una descripción",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            keywords: {
                required: "Debes ingresar al menos una palabra clave",
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("id");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            if (act == 1) {
                persist(form_name, base_url + '/inkcms/ajax/seo/new', base_url + '/inkcms/seo/edit/');
            } else if (act == 2) {
                persist(form_name, base_url + '/inkcms/ajax/seo/update/' + pp_id, base_url + '/inkcms/seo/edit/');
            }
            return false;
        }
    });
});

var persist = function(frm, processPATH, successPATH)
{
    var i = $("#" + frm).serialize();
    $.ajax({
        type: 'POST',
        url: processPATH,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", successPATH + r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
