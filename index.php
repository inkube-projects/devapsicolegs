<?php

$maintenance = false;

if (!$maintenance) {
    require('core/core.php');

    // Se cargan las clases a utilizar
    spl_autoload_register(function ($class_name) {
        // Se requieren las clases personalizadas
        if(file_exists("core/Classes/{$class_name}.php")) {
            require_once("core/Classes/{$class_name}.php");
        }
    });

    //Se cargan las herramientas del gestor
    $cmsTools = new NaS\DevaPsicolegs\Tools\CmsTools;
    $cmsTools->execute();

    // Se requieren las rutas
    $list_routes_container = listFolders("src/routes");
    foreach ($list_routes_container as $c_route) {
        foreach (scandir("src/routes/".$c_route) as $filename) {
            $path = "src/routes/".$c_route."/".$filename;
            if (is_file($path)) {
                require_once($path);
            }
        }
    }

    \NaS\Classes\Route::loadRoute();

    // Se verifican las rutas
    if (!\NaS\Classes\Route::validateRoute()) {
        $errorController = new NaS\DevaPsicolegs\Controller\Admin\ErrorController;
        $errorController->notFoundAction();
    }
} else {
    echo "<h1>Sitio web en mantenimiento</h1>"; exit;
}

?>
